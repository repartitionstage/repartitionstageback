-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 08, 2021 at 12:55 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `tirajosaure_dev`
--
CREATE DATABASE IF NOT EXISTS `tirajosaure_dev` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `tirajosaure_dev`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins`
(
    `id`     int(10) UNSIGNED NOT NULL,
    `userId` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories`
(
    `id`                     int(10) UNSIGNED NOT NULL,
    `name`                   varchar(126)              DEFAULT NULL,
    `maxChoices`             int(11)          NOT NULL DEFAULT '-1',
    `maxAttributions`        int(11)          NOT NULL DEFAULT '-1',
    `maxAttributionsPerYear` int(11)          NOT NULL DEFAULT '-1'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `category_attributions`
--

CREATE TABLE `category_attributions`
(
    `id`            int(10) UNSIGNED NOT NULL,
    `categoryId`    int(10) UNSIGNED NOT NULL,
    `terrainTypeId` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts`
(
    `id`          int(11)      NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text,
    `contact`     text         NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents`
(
    `id`          int(11)      NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text         NOT NULL,
    `promotionId` int(11)      NOT NULL DEFAULT '0',
    `fileId`      int(11)      NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE `files`
(
    `id`   int(11)      NOT NULL,
    `uuid` varchar(36)  NOT NULL,
    `name` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `gardes`
--

CREATE TABLE `gardes`
(
    `id`               int(10) UNSIGNED NOT NULL,
    `garde_terrain_id` int(10) UNSIGNED NOT NULL,
    `date`             varchar(255)     NOT NULL,
    `user_id`          int(10) UNSIGNED NOT NULL DEFAULT '0',
    `year_id`          int(10) UNSIGNED NOT NULL,
    `traded`           tinyint(1)       NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `garde_terrains`
--

CREATE TABLE `garde_terrains`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `name`        varchar(255)     NOT NULL,
    `description` text             NOT NULL,
    `night`       tinyint(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `garde_year_configs`
--

CREATE TABLE `garde_year_configs`
(
    `id`      int(11)          NOT NULL,
    `year_id` int(10) UNSIGNED NOT NULL,
    `config`  text             NOT NULL,
    `file_id` int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE `group`
(
    `id`   int(10) UNSIGNED NOT NULL,
    `name` varchar(45) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations`
(
    `id`       int(10) UNSIGNED NOT NULL,
    `name`     varchar(255) DEFAULT NULL,
    `distance` int(11)      DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs`
(
    `id`               int(11) UNSIGNED NOT NULL,
    `date`             bigint(11)       NOT NULL,
    `executor_user_id` int(11) UNSIGNED NOT NULL,
    `target_user_id`   int(11) UNSIGNED NOT NULL,
    `executor_user_ip` varchar(255)     NOT NULL,
    `type`             varchar(255)     NOT NULL,
    `data`             text             NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `date`, `executor_user_id`, `target_user_id`, `executor_user_ip`, `type`, `data`)
VALUES (1, 1628261847178, 27, 27, '::1', 'LOGIN', '{}'),
       (2, 1628265405052, 27, 27, '::1', 'LOGIN', '{}'),
       (3, 1628271040608, 27, 27, '::1', 'LOGIN', '{}'),
       (4, 1628271789914, 27, 27, '::1', 'LOGIN', '{}'),
       (5, 1628271818801, 1, 1, '::1', 'LOGIN', '{}'),
       (6, 1628276263452, 1, 1, '::1', 'LOGIN', '{}'),
       (7, 1628277091489, 1, 1, '::1', 'LOGIN', '{}'),
       (8, 1628280278724, 1, 1, '::1', 'LOGIN', '{}'),
       (9, 1628280311760, 1, 1, '::1', 'LOGIN', '{}'),
       (10, 1628280316371, 27, 27, '::1', 'LOGIN', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `page_content`
--

CREATE TABLE `page_content`
(
    `id`             int(10) UNSIGNED NOT NULL,
    `name`           varchar(126)     DEFAULT NULL,
    `htmlContent`    text,
    `lastModifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `periods`
--

CREATE TABLE `periods`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `beginDate`   varchar(40)               DEFAULT NULL,
    `endDate`     varchar(40)               DEFAULT NULL,
    `yearId`      int(10) UNSIGNED NOT NULL,
    `promotionId` int(10) UNSIGNED NOT NULL,
    `order`       int(11)          NOT NULL,
    `group`       int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `phases`
--

CREATE TABLE `phases`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `name`        varchar(255)              DEFAULT NULL,
    `order`       int(10) UNSIGNED NOT NULL,
    `promotionId` int(10) UNSIGNED NOT NULL,
    `beginDate`   varchar(40)               DEFAULT NULL,
    `endDate`     varchar(40)               DEFAULT NULL,
    `active`      tinyint(4)       NOT NULL DEFAULT '0',
    `finish`      tinyint(1)       NOT NULL DEFAULT '0',
    `config`      text             NOT NULL,
    `yearId`      int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `promotions`
--

CREATE TABLE `promotions`
(
    `id`                       int(10) UNSIGNED NOT NULL,
    `name`                     varchar(45) DEFAULT NULL,
    `selectableOnRegistration` tinyint(4)  DEFAULT '1'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

CREATE TABLE `ranks`
(
    `id`   int(11)      NOT NULL,
    `name` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `ranks`
--

INSERT INTO `ranks` (`id`, `name`)
VALUES (1, 'STUDENT'),
       (2, 'ADMIN'),
       (3, 'PROFESSOR'),
       (4, 'SECRETARY');

-- --------------------------------------------------------

--
-- Table structure for table `rank_requests`
--

CREATE TABLE `rank_requests`
(
    `id`      int(11)          NOT NULL,
    `user_id` int(11) UNSIGNED NOT NULL,
    `rank_id` int(11)          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reset_password_tokens`
--

CREATE TABLE `reset_password_tokens`
(
    `id`      int(10) UNSIGNED NOT NULL,
    `token`   text             NOT NULL,
    `user_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_informations`
--

CREATE TABLE `student_informations`
(
    `user_id`           int(10) UNSIGNED NOT NULL,
    `student_number`    int(11)          NOT NULL,
    `promotion_id`      int(11) UNSIGNED NOT NULL,
    `group_id`          int(11) UNSIGNED NOT NULL,
    `redoubling`        tinyint(1)       NOT NULL,
    `trusted_person_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `terrains`
--

CREATE TABLE `terrains`
(
    `id`              int(10) UNSIGNED NOT NULL,
    `maxAmountPlaces` int(10) UNSIGNED NOT NULL,
    `periodId`        int(10) UNSIGNED NOT NULL,
    `promotionId`     int(10) UNSIGNED DEFAULT NULL,
    `terrainTypeId`   int(10) UNSIGNED NOT NULL,
    `yearId`          int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `terrain_attributions`
--

CREATE TABLE `terrain_attributions`
(
    `id`        int(10) UNSIGNED NOT NULL,
    `userId`    int(10) UNSIGNED NOT NULL,
    `terrainId` int(10) UNSIGNED NOT NULL,
    `state`     int(11)          NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `terrain_types`
--

CREATE TABLE `terrain_types`
(
    `id`                     int(10) UNSIGNED NOT NULL,
    `name`                   varchar(45)               DEFAULT NULL,
    `locationId`             int(10) UNSIGNED NOT NULL,
    `desc`                   text,
    `respFirstName`          varchar(255)     NOT NULL DEFAULT '',
    `respLastName`           varchar(255)     NOT NULL DEFAULT '',
    `respMail`               varchar(255)     NOT NULL DEFAULT '',
    `maxChoices`             int(11)          NOT NULL DEFAULT '-1',
    `maxAttributions`        int(11)          NOT NULL DEFAULT '-1',
    `maxAttributionsPerYear` int(11)          NOT NULL DEFAULT '-1'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users`
(
    `id`         int(10) UNSIGNED NOT NULL,
    `first_name` varchar(255)     NOT NULL,
    `last_name`  varchar(255)     NOT NULL,
    `email`      varchar(126)     NOT NULL,
    `phone`      varchar(10)      NOT NULL,
    `hash`       binary(60)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone`, `hash`)
VALUES (1, 'Théophile', 'Vieux', 'contact@qilat.fr', '0622989150',
        0x243262243130245869686d6a47395157577a5631374d5249366c31692e324f787a7253496c61754848484532574e696a734e5679716a4d505633794b),
       (8, 'Théophilee', 'Vieux', 'contact@qilat.fre', '0622989150',
        0x243262243130244572422f6b3936626d5a6d52585a4470496d324b456566415235676279394953474d4a69343541724d3456534a4861464b39684653),
       (12, 'Théophilee', 'Vieux', 'contact@qilat.f', '0622989150',
        0x2432622431302470644d6445793455306730396c314c51724b76586a4f4c546b4f79593631784e376864594f6d453644435150563171442f64385057),
       (13, 'Théophilee', 'Vieux', 'contact@qilt.fr', '0622989150',
        0x243262243130246a783374765a4f656f547555644777686442773351754f4448732f66706677304e79784442796c696a61725948382e563144575a4f),
       (15, 'Théophilee', 'Vieux', 'contact@qilit.fr', '0622989150',
        0x24326224313024316e61764350356e526e5053655a6f30345935796565756f2e516f677945322e765952552e494166546f636c42474f694e5850336d),
       (16, 'Théophilee', 'Vieux', 'contact@qilu.fr', '0622989150',
        0x243262243130246f52307346783868436a5154424639723041724d6f4f5344366154356862664a34754231616f5376724a46474561447053446b4e43),
       (17, 'Théophilee', 'Vieux', 'conact@qilu.fr', '0622989150',
        0x24326224313024732f794a6e77484a41322e50366d6d3351324a437165717736324e6c2f4b484c684a46364e575544774f63747554376c4b51374e36),
       (18, 'Théophilee', 'Vieux', 'cojnact@qilu.fr', '0622989150',
        0x243262243130246c2e44424d73772e6b6538733535762e33327062682e4d4f344731563352696e5156486a42437949543051796a706b50714a525671),
       (19, 'Théophilee', 'Vieux', 'cojndact@qilu.fr', '0622989150',
        0x2432622431302453447342536830624d4e4a437358396b4f57584c6e4f57307245434e75454f38574537644d6c3374735544416c3278473072505079),
       (20, 'Théophilee', 'Vieux', 'cojnfdact@qilu.fr', '0622989150',
        0x24326224313024694c6575346d3031626261636669492f55694554782e68464254737a787367647a68527332704c455a6149523235562e7966324236),
       (21, 'Théophilee', 'Vieux', 'cojdnfdact@qilu.fr', '0622989150',
        0x243262243130242f6a33786c63577a43644433466f3477322e7445744f6a72573538765133583837316f454631527a7259496367366a3850566c5761),
       (22, 'Théophilee', 'Vieux', 'cojffdnfdact@qilu.fr', '0622989150',
        0x243262243130246e2e4277326b3259725a334877434d4c716548347a4f7a5535326e4d364158704f67494752465a454d6c412e3631516f616e4e3461),
       (23, 'Théophilee', 'Vieux', 'cojffdfnfdact@qilu.fr', '0622989150',
        0x2432622431302443716336314e3230713579487a2f636b5947682f7865634672386568754f56424b442e6e57786e4f3933787a417a2e462e39336b4b),
       (25, 'Théophilee', 'Vieux', 'cojffdfnjfdact@qilu.fr', '0622989150',
        0x243262243130244230724a2e46584b57507837454d564833677979684f6b4f4f6f6d642e382e53447a396f674c5571734264356175677864694f654f),
       (26, 'Toto', 'Vieux', 'contact@qilat.free', '0622989150',
        0x24326224313024416274454d596f566b524969324f6e34646f502e5965726145355166584e642f765936346f5930382f6a3230732f7647656e613275),
       (27, 'Toto', 'Vieux', 'contact@qilat.com', '0622989150',
        0x24326224313024676d51646c794c3577334862644f78382f45784d726555614b4b63744c796f6262576d5771686e4e644f3934586c2f696d416f7653);

-- --------------------------------------------------------

--
-- Table structure for table `user_ranks`
--

CREATE TABLE `user_ranks`
(
    `user_id` int(10) UNSIGNED NOT NULL,
    `rank_id` int(10)          NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4;

--
-- Dumping data for table `user_ranks`
--

INSERT INTO `user_ranks` (`user_id`, `rank_id`)
VALUES (1, 1),
       (27, 1),
       (27, 2);

-- --------------------------------------------------------

--
-- Table structure for table `years`
--

CREATE TABLE `years`
(
    `id`     int(10) UNSIGNED NOT NULL,
    `name`   varchar(45)               DEFAULT NULL,
    `active` tinyint(1)       NOT NULL DEFAULT '0'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `userId_UNIQUE` (`userId`),
    ADD KEY `user_idx` (`userId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `category_attributions`
--
ALTER TABLE `category_attributions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `unique_idx` (`categoryId`, `terrainTypeId`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `terrainType_idx` (`terrainTypeId`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `gardes`
--
ALTER TABLE `gardes`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_garde_terrain_id` (`garde_terrain_id`),
    ADD KEY `fk_user_id` (`user_id`);

--
-- Indexes for table `garde_terrains`
--
ALTER TABLE `garde_terrains`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `garde_year_configs`
--
ALTER TABLE `garde_year_configs`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `fk_unique_yearId` (`year_id`),
    ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `group`
--
ALTER TABLE `group`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_id` (`executor_user_id`),
    ADD KEY `ip` (`executor_user_ip`),
    ADD KEY `target_user_id` (`target_user_id`);

--
-- Indexes for table `page_content`
--
ALTER TABLE `page_content`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `name_UNIQUE` (`name`),
    ADD KEY `fk_home_content_users_idx` (`lastModifiedBy`);

--
-- Indexes for table `periods`
--
ALTER TABLE `periods`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `year_idx` (`yearId`),
    ADD KEY `promotion_idx` (`promotionId`);

--
-- Indexes for table `phases`
--
ALTER TABLE `phases`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `promotion_idx` (`promotionId`),
    ADD KEY `fk_phases_years_idx` (`yearId`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `ranks`
--
ALTER TABLE `ranks`
    ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rank_requests`
--
ALTER TABLE `rank_requests`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `fk_unique_user_id_rank` (`user_id`, `rank_id`) USING BTREE,
    ADD KEY `fk_rank_request_rank_id` (`rank_id`);

--
-- Indexes for table `reset_password_tokens`
--
ALTER TABLE `reset_password_tokens`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `userId_UNIQUE` (`user_id`),
    ADD KEY `userid_pk_idx` (`user_id`);

--
-- Indexes for table `student_informations`
--
ALTER TABLE `student_informations`
    ADD PRIMARY KEY (`user_id`),
    ADD UNIQUE KEY `student_number` (`student_number`),
    ADD KEY `fk_promotion_id` (`promotion_id`),
    ADD KEY `fk_group_id` (`group_id`),
    ADD KEY `fk_trusted_person_id` (`trusted_person_id`);

--
-- Indexes for table `terrains`
--
ALTER TABLE `terrains`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `period_promotion_terraintype_UNIQUE` (`periodId`, `promotionId`, `terrainTypeId`),
    ADD KEY `terrainType_idx` (`terrainTypeId`),
    ADD KEY `promotion_idx` (`promotionId`),
    ADD KEY `period_idx` (`periodId`),
    ADD KEY `fk_terrains_years_idx` (`yearId`);

--
-- Indexes for table `terrain_attributions`
--
ALTER TABLE `terrain_attributions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `user_idx` (`userId`),
    ADD KEY `terrain_idx` (`terrainId`);

--
-- Indexes for table `terrain_types`
--
ALTER TABLE `terrain_types`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `location_idx` (`locationId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `email_UNIQUE` (`email`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- Indexes for table `user_ranks`
--
ALTER TABLE `user_ranks`
    ADD UNIQUE KEY `user_id` (`user_id`, `rank_id`),
    ADD UNIQUE KEY `user_id_2` (`user_id`, `rank_id`),
    ADD KEY `fk_rank_id` (`rank_id`);

--
-- Indexes for table `years`
--
ALTER TABLE `years`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category_attributions`
--
ALTER TABLE `category_attributions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gardes`
--
ALTER TABLE `gardes`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `garde_terrains`
--
ALTER TABLE `garde_terrains`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `garde_year_configs`
--
ALTER TABLE `garde_year_configs`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `group`
--
ALTER TABLE `group`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
    MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 11;

--
-- AUTO_INCREMENT for table `page_content`
--
ALTER TABLE `page_content`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `periods`
--
ALTER TABLE `periods`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phases`
--
ALTER TABLE `phases`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ranks`
--
ALTER TABLE `ranks`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 5;

--
-- AUTO_INCREMENT for table `rank_requests`
--
ALTER TABLE `rank_requests`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reset_password_tokens`
--
ALTER TABLE `reset_password_tokens`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terrains`
--
ALTER TABLE `terrains`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terrain_attributions`
--
ALTER TABLE `terrain_attributions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `terrain_types`
--
ALTER TABLE `terrain_types`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    AUTO_INCREMENT = 28;

--
-- AUTO_INCREMENT for table `years`
--
ALTER TABLE `years`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
    ADD CONSTRAINT `fk_admins_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `category_attributions`
--
ALTER TABLE `category_attributions`
    ADD CONSTRAINT `fk_ca _terrainTypes` FOREIGN KEY (`terrainTypeId`) REFERENCES `terrain_types` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_ca_categories` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`);

--
-- Constraints for table `gardes`
--
ALTER TABLE `gardes`
    ADD CONSTRAINT `fk_garde_terrain_id` FOREIGN KEY (`garde_terrain_id`) REFERENCES `garde_terrains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `garde_year_configs`
--
ALTER TABLE `garde_year_configs`
    ADD CONSTRAINT `fk_year_id` FOREIGN KEY (`year_id`) REFERENCES `years` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `logs`
--
ALTER TABLE `logs`
    ADD CONSTRAINT `fk_executor_user_id` FOREIGN KEY (`executor_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_target_user_id` FOREIGN KEY (`target_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `page_content`
--
ALTER TABLE `page_content`
    ADD CONSTRAINT `fk_home_content_users` FOREIGN KEY (`lastModifiedBy`) REFERENCES `users` (`id`);

--
-- Constraints for table `periods`
--
ALTER TABLE `periods`
    ADD CONSTRAINT `fk_period_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`),
    ADD CONSTRAINT `fk_periods_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`);

--
-- Constraints for table `phases`
--
ALTER TABLE `phases`
    ADD CONSTRAINT `fb_phases_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`),
    ADD CONSTRAINT `fk_phases_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rank_requests`
--
ALTER TABLE `rank_requests`
    ADD CONSTRAINT `fk_rank_request_rank_id` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`),
    ADD CONSTRAINT `fk_rank_request_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `reset_password_tokens`
--
ALTER TABLE `reset_password_tokens`
    ADD CONSTRAINT `fk_rpt_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `student_informations`
--
ALTER TABLE `student_informations`
    ADD CONSTRAINT `fk_group_id` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_promotion_id` FOREIGN KEY (`promotion_id`) REFERENCES `promotions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_trusted_person_id` FOREIGN KEY (`trusted_person_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `terrains`
--
ALTER TABLE `terrains`
    ADD CONSTRAINT `fk_terrains_periods` FOREIGN KEY (`periodId`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_terrains_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
    ADD CONSTRAINT `fk_terrains_terrain_types` FOREIGN KEY (`terrainTypeId`) REFERENCES `terrain_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_terrains_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `terrain_attributions`
--
ALTER TABLE `terrain_attributions`
    ADD CONSTRAINT `fk_distribution_terrains` FOREIGN KEY (`terrainId`) REFERENCES `terrains` (`id`),
    ADD CONSTRAINT `fk_distribution_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Constraints for table `terrain_types`
--
ALTER TABLE `terrain_types`
    ADD CONSTRAINT `fk_terrain_types_locations` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`);

--
-- Constraints for table `user_ranks`
--
ALTER TABLE `user_ranks`
    ADD CONSTRAINT `fk_player_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
    ADD CONSTRAINT `fk_rank_id` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
