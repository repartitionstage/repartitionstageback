echo "Arrêt de l'api de Tirajosaure"
echo "------------------------------------"
echo "1. Arrêt des processus"
pm2 stop dev.api.tirajosaure.ars-amiens.fr
echo "------------------------------------"
echo "2. Suppression des processus"
pm2 del dev.api.tirajosaure.ars-amiens.fr
echo "------------------------------------"
