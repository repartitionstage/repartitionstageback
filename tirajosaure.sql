SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE `admins`
(
    `id`     int(10) UNSIGNED NOT NULL,
    `userId` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `categories`
(
    `id`                     int(10) UNSIGNED NOT NULL,
    `name`                   varchar(126)              DEFAULT NULL,
    `maxChoices`             int(11)          NOT NULL DEFAULT -1,
    `maxAttributions`        int(11)          NOT NULL DEFAULT -1,
    `maxAttributionsPerYear` int(11)          NOT NULL DEFAULT -1
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `category_attributions`
(
    `id`            int(10) UNSIGNED NOT NULL,
    `categoryId`    int(10) UNSIGNED NOT NULL,
    `terrainTypeId` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `contacts`
(
    `id`          int(11)      NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text DEFAULT NULL,
    `contact`     text         NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `documents`
(
    `id`          int(11)      NOT NULL,
    `name`        varchar(255) NOT NULL,
    `description` text         NOT NULL,
    `promotionId` int(11)      NOT NULL DEFAULT 0,
    `fileId`      int(11)      NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `files`
(
    `id`   int(11)      NOT NULL,
    `uuid` varchar(36)  NOT NULL,
    `name` varchar(255) NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `gardes`
(
    `id`               int(10) UNSIGNED NOT NULL,
    `garde_terrain_id` int(10) UNSIGNED NOT NULL,
    `date`             varchar(255)     NOT NULL,
    `user_id`          int(10) UNSIGNED NOT NULL DEFAULT 0,
    `year_id`          int(10) UNSIGNED NOT NULL,
    `traded`           tinyint(1)       NOT NULL DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `garde_terrains`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `name`        varchar(255)     NOT NULL,
    `description` text             NOT NULL DEFAULT '',
    `night`       tinyint(1)       NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `garde_year_configs`
(
    `id`      int(11)          NOT NULL,
    `year_id` int(10) UNSIGNED NOT NULL,
    `config`  text             NOT NULL,
    `file_id` int(11) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `group`
(
    `id`   int(10) UNSIGNED NOT NULL,
    `name` varchar(45) DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `locations`
(
    `id`       int(10) UNSIGNED NOT NULL,
    `name`     varchar(255) DEFAULT NULL,
    `distance` int(11)      DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `logs`
(
    `id`               int(11) UNSIGNED NOT NULL,
    `date`             bigint(11)       NOT NULL,
    `executor_user_id` int(11) UNSIGNED NOT NULL,
    `target_user_id`   int(11) UNSIGNED NOT NULL,
    `executor_user_ip` varchar(255)     NOT NULL,
    `type`             varchar(255)     NOT NULL,
    `data`             text             NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `page_content`
(
    `id`             int(10) UNSIGNED NOT NULL,
    `name`           varchar(126)     DEFAULT NULL,
    `htmlContent`    text             DEFAULT NULL,
    `lastModifiedBy` int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `periods`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `beginDate`   varchar(40)               DEFAULT NULL,
    `endDate`     varchar(40)               DEFAULT NULL,
    `yearId`      int(10) UNSIGNED NOT NULL,
    `promotionId` int(10) UNSIGNED NOT NULL,
    `order`       int(11)          NOT NULL,
    `group`       int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `phases`
(
    `id`          int(10) UNSIGNED NOT NULL,
    `name`        varchar(255)              DEFAULT NULL,
    `order`       int(10) UNSIGNED NOT NULL,
    `promotionId` int(10) UNSIGNED NOT NULL,
    `beginDate`   varchar(40)               DEFAULT NULL,
    `endDate`     varchar(40)               DEFAULT NULL,
    `active`      tinyint(4)       NOT NULL DEFAULT 0,
    `finish`      tinyint(1)       NOT NULL DEFAULT 0,
    `config`      text             NOT NULL,
    `yearId`      int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `promotions`
(
    `id`                       int(10) UNSIGNED NOT NULL,
    `name`                     varchar(45) DEFAULT NULL,
    `selectableOnRegistration` tinyint(4)  DEFAULT 1
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `reset_password_tokens`
(
    `id`     int(10) UNSIGNED NOT NULL,
    `token`  text             NOT NULL,
    `userId` int(10) UNSIGNED DEFAULT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `terrains`
(
    `id`              int(10) UNSIGNED NOT NULL,
    `maxAmountPlaces` int(10) UNSIGNED NOT NULL,
    `periodId`        int(10) UNSIGNED NOT NULL,
    `promotionId`     int(10) UNSIGNED DEFAULT NULL,
    `terrainTypeId`   int(10) UNSIGNED NOT NULL,
    `yearId`          int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `terrain_attributions`
(
    `id`        int(10) UNSIGNED NOT NULL,
    `userId`    int(10) UNSIGNED NOT NULL,
    `terrainId` int(10) UNSIGNED NOT NULL,
    `state`     int(11)          NOT NULL DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `terrain_types`
(
    `id`                     int(10) UNSIGNED NOT NULL,
    `name`                   varchar(45)               DEFAULT NULL,
    `locationId`             int(10) UNSIGNED NOT NULL,
    `desc`                   text                      DEFAULT NULL,
    `respFirstName`          varchar(255)     NOT NULL DEFAULT '',
    `respLastName`           varchar(255)     NOT NULL DEFAULT '',
    `respMail`               varchar(255)     NOT NULL DEFAULT '',
    `maxChoices`             int(11)          NOT NULL DEFAULT -1,
    `maxAttributions`        int(11)          NOT NULL DEFAULT -1,
    `maxAttributionsPerYear` int(11)          NOT NULL DEFAULT -1
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

CREATE TABLE `users`
(
    `id`            int(10) UNSIGNED NOT NULL,
    `firstName`     varchar(255)     NOT NULL,
    `lastName`      varchar(255)     NOT NULL,
    `email`         varchar(126)     NOT NULL,
    `studentNumber` varchar(44)      NOT NULL,
    `phone`         varchar(10)      NOT NULL,
    `promotionId`   int(10) UNSIGNED NOT NULL,
    `password`      varchar(255)     NOT NULL,
    `salt`          varchar(255)     NOT NULL DEFAULT '',
    `trustedPerson` int(10) UNSIGNED          DEFAULT NULL,
    `group`         int(10) UNSIGNED NOT NULL DEFAULT 1,
    `groupBlocked`  tinyint(1)       NOT NULL DEFAULT 0,
    `redoubling`    tinyint(4)       NOT NULL DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  ROW_FORMAT = COMPACT;

CREATE TABLE `years`
(
    `id`     int(10) UNSIGNED NOT NULL,
    `name`   varchar(45)               DEFAULT NULL,
    `active` tinyint(1)       NOT NULL DEFAULT 0
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;


ALTER TABLE `admins`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `userId_UNIQUE` (`userId`),
    ADD KEY `user_idx` (`userId`);

ALTER TABLE `categories`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

ALTER TABLE `category_attributions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `unique_idx` (`categoryId`, `terrainTypeId`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `terrainType_idx` (`terrainTypeId`);

ALTER TABLE `contacts`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `documents`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `files`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `uuid` (`uuid`);

ALTER TABLE `gardes`
    ADD PRIMARY KEY (`id`),
    ADD KEY `fk_garde_terrain_id` (`garde_terrain_id`),
    ADD KEY `fk_user_id` (`user_id`);

ALTER TABLE `garde_terrains`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `garde_year_configs`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `fk_unique_yearId` (`year_id`),
    ADD UNIQUE KEY `id` (`id`);

ALTER TABLE `group`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `locations`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

ALTER TABLE `logs`
    ADD PRIMARY KEY (`id`),
    ADD KEY `user_id` (`executor_user_id`),
    ADD KEY `ip` (`executor_user_ip`),
    ADD KEY `target_user_id` (`target_user_id`);

ALTER TABLE `page_content`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `name_UNIQUE` (`name`),
    ADD KEY `fk_home_content_users_idx` (`lastModifiedBy`);

ALTER TABLE `periods`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `year_idx` (`yearId`),
    ADD KEY `promotion_idx` (`promotionId`);

ALTER TABLE `phases`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `promotion_idx` (`promotionId`),
    ADD KEY `fk_phases_years_idx` (`yearId`);

ALTER TABLE `promotions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);

ALTER TABLE `reset_password_tokens`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `userId_UNIQUE` (`userId`),
    ADD KEY `userid_pk_idx` (`userId`);

ALTER TABLE `terrains`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD UNIQUE KEY `period_promotion_terraintype_UNIQUE` (`periodId`, `promotionId`, `terrainTypeId`),
    ADD KEY `terrainType_idx` (`terrainTypeId`),
    ADD KEY `promotion_idx` (`promotionId`),
    ADD KEY `period_idx` (`periodId`),
    ADD KEY `fk_terrains_years_idx` (`yearId`);

ALTER TABLE `terrain_attributions`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `user_idx` (`userId`),
    ADD KEY `terrain_idx` (`terrainId`);

ALTER TABLE `terrain_types`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `location_idx` (`locationId`);

ALTER TABLE `users`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `email_UNIQUE` (`email`),
    ADD UNIQUE KEY `studentNumber_UNIQUE` (`studentNumber`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`),
    ADD KEY `trustedPerson_idx` (`trustedPerson`),
    ADD KEY `promotion_idx` (`promotionId`),
    ADD KEY `fk_users_group` (`group`);

ALTER TABLE `years`
    ADD PRIMARY KEY (`id`),
    ADD UNIQUE KEY `id_UNIQUE` (`id`);


ALTER TABLE `admins`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `categories`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `category_attributions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `contacts`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `documents`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `files`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `gardes`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `garde_terrains`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `garde_year_configs`
    MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `group`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `locations`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `logs`
    MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `page_content`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `periods`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `phases`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `promotions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `reset_password_tokens`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `terrains`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `terrain_attributions`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `terrain_types`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `users`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
ALTER TABLE `years`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

ALTER TABLE `admins`
    ADD CONSTRAINT `fk_admins_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

ALTER TABLE `category_attributions`
    ADD CONSTRAINT `fk_ca _terrainTypes` FOREIGN KEY (`terrainTypeId`) REFERENCES `terrain_types` (`id`) ON DELETE CASCADE,
    ADD CONSTRAINT `fk_ca_categories` FOREIGN KEY (`categoryId`) REFERENCES `categories` (`id`);

ALTER TABLE `gardes`
    ADD CONSTRAINT `fk_garde_terrain_id` FOREIGN KEY (`garde_terrain_id`) REFERENCES `garde_terrains` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `garde_year_configs`
    ADD CONSTRAINT `fk_year_id` FOREIGN KEY (`year_id`) REFERENCES `years` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `logs`
    ADD CONSTRAINT `fk_executor_user_id` FOREIGN KEY (`executor_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
    ADD CONSTRAINT `fk_target_user_id` FOREIGN KEY (`target_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `page_content`
    ADD CONSTRAINT `fk_home_content_users` FOREIGN KEY (`lastModifiedBy`) REFERENCES `users` (`id`);

ALTER TABLE `periods`
    ADD CONSTRAINT `fk_period_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`),
    ADD CONSTRAINT `fk_periods_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`);

ALTER TABLE `phases`
    ADD CONSTRAINT `fb_phases_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`),
    ADD CONSTRAINT `fk_phases_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE `reset_password_tokens`
    ADD CONSTRAINT `fk_rpt_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

ALTER TABLE `terrains`
    ADD CONSTRAINT `fk_terrains_periods` FOREIGN KEY (`periodId`) REFERENCES `periods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_terrains_promotions` FOREIGN KEY (`promotionId`) REFERENCES `promotions` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
    ADD CONSTRAINT `fk_terrains_terrain_types` FOREIGN KEY (`terrainTypeId`) REFERENCES `terrain_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    ADD CONSTRAINT `fk_terrains_years` FOREIGN KEY (`yearId`) REFERENCES `years` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `terrain_attributions`
    ADD CONSTRAINT `fk_distribution_terrains` FOREIGN KEY (`terrainId`) REFERENCES `terrains` (`id`),
    ADD CONSTRAINT `fk_distribution_users` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

ALTER TABLE `terrain_types`
    ADD CONSTRAINT `fk_terrain_types_locations` FOREIGN KEY (`locationId`) REFERENCES `locations` (`id`);

ALTER TABLE `users`
    ADD CONSTRAINT `fk_users_users_trusted_person` FOREIGN KEY (`trustedPerson`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
