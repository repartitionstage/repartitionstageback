const express = require("express");
const {TokenParser, IpLoader} = require('./middlewares');

module.exports = (router) => {

    router.use(IpLoader);

    const authentication = require('./features/authentication')(express.Router());
    router.use('/authentication', authentication);

    router.use(TokenParser);

    const user = require('./features/users-management')(express.Router());
    router.use('/users-management', user);

    const garde = require('./features/gardes-management')(express.Router());
    router.use('/gardes-management', garde);

    const repartition = require('./features/repartition')(express.Router());
    router.use('/repartition', repartition)

    const page = require('./features/page')(express.Router());
    router.use('/page', page);

    const file = require('./features/file')(express.Router());
    router.use('/file', file);

    const contact = require('./features/contact')(express.Router());
    router.use('/contact', contact);

    const logs = require('./features/logs')(express.Router());
    router.use('/logs', logs);




    return router;
};
