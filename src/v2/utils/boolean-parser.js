module.exports = (input) => {
    if (input === true || input === 'true' || input === '1')
        return true;
    if (input === false || input === 'false' || input === '0')
        return false;

    return undefined;
}
