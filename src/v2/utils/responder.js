const StandardErrors = require('./standard-errors.js');

const sendError = (connection, error) => {
    if (error === undefined || error['code'] === undefined || error['reason'] === undefined) {
        console.error(error);
        error = StandardErrors.ERROR_OCCURS;
    }

    connection.res
        .status(error.code)
        .json({reason: error.reason});

    if (connection.req.sql) connection.req.sql.release();
};

const sendDefaultError = (connection) => {
    return (error) => {
        sendError(connection, error);
    };
};

const sendSuccessWithData = (connection, data) => {
    connection.res.status(200).json(data);
    if (connection.req.sql) connection.req.sql.release();
};

const sendDefaultData = (connection) => (data) => {
    sendSuccessWithData(connection, data);
};

const sendSuccessWithNoResponse = (connection) => {
    connection.res.status(204).json();
    if (connection.req.sql) connection.req.sql.release();
};

const sendDefaultSuccess = (connection) => () => {
    sendSuccessWithNoResponse(connection)
};

module.exports = {
    sendError,
    sendDefaultError,
    sendSuccessWithData,
    sendDefaultData,
    sendSuccessWithNoResponse,
    sendDefaultSuccess,
};
