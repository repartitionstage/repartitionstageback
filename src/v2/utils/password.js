const bcrypt = require('bcrypt');
const saltRounds = 10;

const encrypt = (password) => {
    return new Promise(
        (resolve, reject) => {
            bcrypt.genSalt(saltRounds, (err, salt) => {
                if (err) {
                    reject();
                } else {
                    bcrypt.hash(password, salt, (err2, hash) => {
                        if (err2) {
                            reject();
                        } else {
                            resolve(hash);
                        }
                    })
                }
            })
        }
    )
}

/**
 *
 * @param password
 * @param hash
 * @returns {Promise<boolean>}
 */
const compare = (password, hash) => {
    if (typeof hash !== 'string')
        hash = String(hash);
    return new Promise(
        (resolve, reject) => {
            bcrypt.compare(password, hash, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            })
        }
    )
}

module.exports = {
    encrypt,
    compare
}
