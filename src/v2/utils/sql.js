const mysql = require('mysql');

const getFormattedFields = (fields, tableName) => {
    if (fields) {
        let result = '';
        for (let i = 0; i < fields.length; i++) {
            if (i !== (fields.length - 1)) {
                result += (tableName ? (tableName + '.' + fields[i] + ', ') : ('`' + fields[i] + '`, '));
            } else {
                result += (tableName ? (tableName + '.' + fields[i] + ' ') : ('`' + fields[i] + '` '));
            }
        }
        return result;
    } else {
        return tableName + '.*'
    }
};

const getInsertQuery = (values) => {
    const isArray = Array.isArray(values);
    values = (isArray === true ? values : [values]);
    let result = "";


    if (values.length > 0) {
        const fields = [];
        const model = values[0];
        const size = Object.values(model).length;

        result += "(";

        let k = 0;
        for (let field in model) {
            if (model.hasOwnProperty(field)) {
                k++;
                fields.push(field);
                if (k !== size) {
                    result += (mysql.escapeId(field) + ", ")
                } else {
                    result += mysql.escapeId(field);
                }
            }
        }
        result += ")";

        result += " VALUES ";

        let i = 0;
        for (let line of values) {
            i++;
            let j = 0;
            let lineSize = Object.values(line).length;
            if (values.length >= 1)
                result += "(";

            for (const field of fields) {
                if (!line[field] && isNaN(Number(line[field])))
                    throw new Error(field + ' not found !');

                const value = mysql.escape(line[field]);
                j++;
                if (j !== lineSize) {
                    result += (value + ", ");
                } else {
                    result += value;
                }
            }

            if (values.length >= 1)
                result += ")";

            if (i !== values.length) {
                result += ",";
            }
        }

        return result;
    } else {
        return null;
    }
};

module.exports = {
    getFormattedFields,
    getInsertQuery
};
