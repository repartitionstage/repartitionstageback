const StandardErrors = require('./standard-errors');

class Table {
    constructor(tableName, jsToSqlLUT, sqlToJsLUT) {
        this.tableName = tableName;
        this.jsToSqlLUT = jsToSqlLUT;
        this.sqlToJsLUT = sqlToJsLUT;

        this.insert = this.insert.bind(this);
        this.updateById = this.updateById.bind(this);
        this.deleteById = this.deleteById.bind(this);
        this.selectById = this.selectById.bind(this);
        this.selectAllByIds = this.selectAllByIds.bind(this);
        this.selectAll = this.selectAll.bind(this);

        this.transformObjects = this.transformObjects.bind(this);
        this.transformObjectJsToSql = this.transformObjectJsToSql.bind(this);
        this.transformObjectSqlToJs = this.transformObjectSqlToJs.bind(this);
        this.transformFieldFromJsToSql = this.transformFieldFromJsToSql.bind(this);
    }

    insert(sql, values) {
        return new Promise(
            (resolve, reject) => {
                values = this.transformObjectJsToSql(values);
                sql.query('INSERT INTO ' + this.tableName + ' SET ?', [values],
                    (error) => {
                        if (error) {
                            if (error.errno === 1062) { //duplicate entry
                                reject(StandardErrors.PARAMS_TAKEN);
                            } else if (error.errno === 1452) { //foreign key not respected
                                reject(StandardErrors.OBJECT_NOT_FOUND);
                            } else {
                                console.error(error);
                                reject(StandardErrors.SQL_ERROR);
                            }
                        } else {
                            resolve();
                        }
                    });
            });
    }

    updateById(sql, id, values) {
        return new Promise(
            (resolve, reject) => {
                delete values.id;
                values = this.transformObjectJsToSql(values);
                sql.query('UPDATE ' + this.tableName + ' SET ? WHERE `id` = ?', [values, id],
                    (error, info) => {
                        if (error) {
                            console.error(error);
                            reject(StandardErrors.SQL_ERROR);
                        } else if (info.affectedRows === 0) {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        } else {
                            resolve();
                        }
                    });
            }
        );
    }

    deleteById(sql, id) {
        return new Promise(
            (resolve, reject) => {
                sql.query('DELETE FROM ' + this.tableName + ' WHERE `id` = ?', [id],
                    (error, info) => {
                        if (error) {
                            console.error(error);
                            reject(StandardErrors.SQL_ERROR);
                        } else if (info.affectedRows === 0) {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        } else {
                            resolve();
                        }
                    });
            }
        );
    }

    selectById(sql, id, fields) {
        return new Promise(
            (resolve, reject) => {
                fields = this.transformFieldFromJsToSql(fields);
                sql.query('SELECT ?? FROM ' + this.tableName + ' WHERE `id` = ?', [fields, id],
                    (error, result) => {
                        if (error) {
                            console.error(error);
                            reject(StandardErrors.SQL_ERROR);
                        } else {
                            if (result && result.length > 0) {
                                resolve(this.transformObjectSqlToJs(result[0]));
                            } else {
                                reject(StandardErrors.OBJECT_NOT_FOUND);
                            }
                        }
                    }
                );
            }
        );
    }

    selectAllByIds(sql, ids, fields) {
        return new Promise(
            (resolve, reject) => {
                fields = this.transformFieldFromJsToSql(fields);
                sql.query('SELECT ?? FROM ' + this.tableName + ' WHERE `id` IN (?)', [fields, ids],
                    (error, result) => {
                        if (error) {
                            console.error(error);
                            reject(StandardErrors.SQL_ERROR);
                        } else {
                            if (result && result.length > 0) {
                                resolve(this.transformObjectSqlToJs(result));
                            } else {
                                reject(StandardErrors.OBJECT_NOT_FOUND);
                            }
                        }
                    }
                );
            }
        );
    }

    selectAll(sql, fields, limit = 0, offset = 0, orderBy = undefined, asc = true) {
        return new Promise(
            (resolve, reject) => {
                fields = this.transformFieldFromJsToSql(fields);

                let limitOffsetOrder = '';

                if (orderBy !== undefined) {
                    limitOffsetOrder += ' ORDER BY ' + this.jsToSqlLUT[orderBy];
                    limitOffsetOrder += (asc ? ' ASC ' : ' DESC ');
                }

                if (limit !== 0) {
                    limitOffsetOrder += ' LIMIT ' + limit;
                    if (offset !== 0) {
                        limitOffsetOrder += ' OFFSET ' + offset;
                    }
                }

                sql.query('SELECT ?? FROM ' + this.tableName + ' ' + limitOffsetOrder, [fields],
                    (error, result) => {
                        if (error) {
                            console.error(error);
                            reject(StandardErrors.SQL_ERROR);
                        } else {
                            resolve(this.transformObjectSqlToJs(result));
                        }
                    });
            }
        );
    }

    transformObjects(table, objects) {
        const isArray = Array.isArray(objects);

        if (!isArray)
            objects = [objects];

        const newObjects = [];

        for (let object of objects) {
            const newObj = {};
            for (let prop in object) {
                if (Object.prototype.hasOwnProperty.call(object, prop)) {
                    if (table[prop] !== undefined) {
                        newObj[table[prop]] = object[prop];
                    } else {
                       newObj[prop] = object[prop];
                    }
                }
            }
            newObjects.push(newObj);
        }

        return isArray ? newObjects : newObjects[0];
    }

    /**
     *
     * @param objects can be an object or an array of object
     * @returns {{}|[]}
     */
    transformObjectJsToSql(objects) {
        return this.transformObjects(this.jsToSqlLUT, objects);
    }

    /**
     *
     * @param objects can be an object or an array of object
     * @returns {{}|[]}
     */
    transformObjectSqlToJs(objects) {
        return this.transformObjects(this.sqlToJsLUT, objects);
    }

    transformFieldFromJsToSql(fields) {
        let newFields = [];
        for(const field of fields) {
            if(this.jsToSqlLUT[field] !== undefined) {
                newFields.push(this.jsToSqlLUT[field]);
            } else {
                newFields.push(field);
            }
        }
        return newFields;
    }
}

module.exports = Table;
