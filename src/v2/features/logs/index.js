const Logs = require('./logs');
const {Authorization, SQL, Validator} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils');

module.exports = (router) => {
    router.route('/')
        .get(Authorization(AuthorizationLevel.ADMIN),
            Validator({
                executorId: {
                    type: 'number',
                    min: 0,
                    optional: true
                },
                targetId: {
                    type: 'number',
                    min: 0,
                    optional: true
                },
                type: {
                    type: 'string'
                }
            }),
            SQL.assign,
            Logs.readAll);
    return router;
};
