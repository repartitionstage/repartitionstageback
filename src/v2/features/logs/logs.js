const {LogsDB} = require('../../database');
const {StandardErrors, Responder} = require('../../utils');
const {LogsService} = require('../../services');

module.exports.readAll = (req, res, next) => {
    const connection = {req, res, next};

    const executorId = req.query['executorId'];
    const targetId = req.query['targetId'];
    const actionType = req.query['type'];

    if (LogsService.TYPES[actionType.toUpperCase()] !== undefined) {
        LogsDB.selectSpecificLog(req.sql, executorId, targetId, actionType)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        Responder.sendError(connection, StandardErrors.INVALID_FORMAT);
    }

};
