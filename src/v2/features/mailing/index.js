const {Authorization, SQL, Validator} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils');
const routes = require('./mailing');

module.exports = (router) => {

    router.route('/:id')
        .all(
            Validator(
                {
                    id: {type: 'number', min: 0}
                },
                {paramsInURL: true}
            )
        )
        .get(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            routes.downloadCSV);

    return router;
};
