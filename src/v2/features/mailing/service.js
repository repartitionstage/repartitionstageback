const fs = require('fs');
const {GoogleService} = require('../../services');
const {PromotionsDB, UsersDB} = require('../../database');
const {StandardErrors} = require('../../utils');

const getPromotionEmail = (name) => {
    return name.toLowerCase() + '@ars-amiens.fr';
}

const insertMails = (groupEmail, emails) => {
    return new Promise(
        (resolve, reject) => {
            let i = 0;
            let error = false;

            for (const email of emails) {
                GoogleService.insertMember(groupEmail, email)
                    .then(() => {
                        i++;
                    })
                    .catch(() => {
                        error = true;
                        i++;
                        reject();
                    });
            }

            const checkRequestsFinished = () => {
                if (i === emails.length) {
                    if (!error)
                        resolve();
                } else {
                    setTimeout(checkRequestsFinished, 1);
                }
            }

            checkRequestsFinished();
        }
    )
}

const updateByPromotion = (sql, id) => {
    return new Promise(
        (resolve, reject) => {
            PromotionsDB.selectById(sql, id, ['id', 'name'])
                .then(
                    (fields) => {
                        if (fields) {
                            const id = fields.id;
                            const name = fields.name;
                            const groupEmail = getPromotionEmail(name);

                            UsersDB.selectByPromotion(sql, id, ['email'])
                                .then(
                                    (users) => {
                                        const emails = users.map(user => user.email);

                                        insertMails(groupEmail, emails)
                                            .then(() => resolve())
                                            .catch((err) => reject(err));

                                    }
                                )
                                .catch((err) => reject(err));
                        } else {
                            reject(StandardErrors.ERROR_OCCURS);
                        }
                    }
                )
                .catch((err) => reject(err));
        }
    );
}

const exportToCSV = async (sql, promotionId) => {
    return new Promise(
        async (resolve, reject) => {
            try {
                const promotion = await PromotionsDB.selectById(sql, promotionId, ["name"]);
                const selectedUsers = await UsersDB.selectByPromotion(sql, promotionId, ["email"]);

                const groupEmail = getPromotionEmail(promotion.name);

                let text = 'Group Email [Required],' +
                    'Member Email,' +
                    'Member Type,' +
                    'Member Role' +
                    '\n';

                for (let user of selectedUsers) {
                    text += groupEmail + ','
                    text += user.email + ',';
                    text += 'USER' + ','
                    text += 'MEMBER'
                    text += '\n';
                }

                const fileName = 'mailing-' + promotion.name + '.csv';
                fs.writeFileSync(fileName, text);
                resolve(fileName);
            } catch (e) {
                reject(e);
            }
        }
    );
};

module.exports = {
    updateByPromotion,
    exportToCSV
}
