const service = require('./service');

let downloadCSV = (req, res, next) => {
    const connection = {req, res, next};
    let id = req.params.id;
    service.exportToCSV(req.sql, id)
        .then(
            (fileName) => {
                res.download(fileName, fileName);
                if (connection.req.sql) connection.req.sql.release();
            }
        );
}

module.exports = {
    downloadCSV
};
