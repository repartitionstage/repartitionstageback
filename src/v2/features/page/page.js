const {PageContentDB} = require('../../database');
const {Responder} = require('../../utils');

exports.read = (req, res, next) => {
    const connection = {req, res, next};
    const name = req.params.name;
    PageContentDB.selectByName(req.sql, name)
        .then(Responder.sendDefaultData(connection))
        .catch(Responder.sendDefaultError(connection));
};


exports.createOrUpdate = (req, res, next) => {
    const connection = {req, res, next};
    const object = req.body;
    object.lastModifiedBy = req.payload.userId;
    PageContentDB.insert(req.sql, object)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));

};

exports.readNames = (req, res, next) => {
    const connection = {req, res, next};
    PageContentDB.selectNames(req.sql)
        .then(Responder.sendDefaultData(connection))
        .catch(Responder.sendDefaultError(connection));
};
