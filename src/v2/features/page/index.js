const {Authorization, Validator, SQL} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils')

module.exports = (router) => {
    const page = require('./page');

    router.route('/')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator({
                name: {
                    type: 'string'
                },
                htmlContent: {
                    type: 'string'
                }
            }),
            SQL.assign,
            page.createOrUpdate)
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            page.readNames);

    router.route('/:name')
        .all(
            Validator(
                {
                    name: {type: 'string'}
                },
                {paramsInURL: true}
            )
        )
        .get(SQL.assign,
            page.read);

    return router;
};
