const {Authorization, SQL} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils');
const exchanges = require('./exchanges');

module.exports = (router) => {
    router.route('/exchanges')
        .post(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            exchanges.create);
    router.route('./exchanges/:id')
        .put(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            exchanges.answer);

    return router;
}

