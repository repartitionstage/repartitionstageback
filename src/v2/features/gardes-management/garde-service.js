const {GardesDB, GardeTerrainsDB, GardeYearConfigsDB} = require('../../database');
const xl = require('excel4node');

const formatTo2Digits = (number) => {
    return (number < 10 && number > 0) ? '0' + number : number;
};
const formatDate = (date) => {
    return formatTo2Digits(date.getDate()) + '/' + formatTo2Digits(date.getMonth() + 1) + '/' + date.getFullYear().toString().slice(-2);
};

class Table {
    constructor() {
        this.days = [];
        this.getDay = (date) => {
            for (let day of this.days) {
                if (formatDate(day.date) === date)
                    return day;
            }
        }
    }
}

class Day {
    constructor(date, config) {
        this.date = date;
        this.weekend = date.getDay() === 0 || date.getDay() === 6; // check if day is Sunday or Saturday
        this.attributions = new Map(); // <terrainId, user>

        config.terrains.forEach(
            (terrain) => {
                this.attributions.set(terrain.id, null);
            }
        );
    }
}

class Exception {
    constructor(promotionId) {
        this.dates = [];
        this.promotionId = promotionId;

        this.containsDate = (dateInput) => {
            for (const date of this.dates) {
                if (date.getDate() === dateInput.getDate()
                    && date.getMonth() === dateInput.getMonth()
                    && date.getFullYear() === dateInput.getFullYear())
                    return true;
            }
            return false;
        }
    }
}

const parseExceptions = (config) => {
    const exceptions = new Map();

    for (const exception of config.exceptions) {
        const e = new Exception(exception.promotion);
        for (const date of exception.dates) {
            e.dates.push(new Date(date));
        }
        exceptions.set(exception.promotion, e);
    }

    const e = new Exception('holydays');
    for (const date of config.holydays) {
        e.dates.push(new Date(date));
    }
    exceptions.set('holydays', e);

    return exceptions;
};

const getUsers = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT id, firstname AS firstName, lastname AS lastName, promotionId as promotion , `group`, email, phone FROM `users` WHERE promotionId IN (2, 3, 4)',
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject();
                    } else {
                        resolve(result);
                    }
                });
        }
    );
};

const createTable = (config) => {
    const table = new Table();

    const beginDate = new Date(config.beginDate);
    const endDate = new Date(config.endDate);

    table.days.push(new Day(beginDate, config));
    let currentDate = new Date(beginDate.getTime());
    while (true) {
        currentDate.setDate(currentDate.getDate() + 1);

        table.days.push(new Day(new Date(currentDate.getTime()), config));

        if (currentDate.getDate() === endDate.getDate()
            && currentDate.getMonth() === endDate.getMonth()
            && currentDate.getFullYear() === endDate.getFullYear())
            break;
    }

    return table;
};

const getGroupName = (id) => {
    switch (id) {
        case 1:
            return 'Aucun';
        case 2:
            return 'A';
        case 3:
            return 'B';
        case 4:
            return 'Les deux';
    }
};

const getPromotionName = (id) => {
    switch (id) {
        case 2:
            return 'DFASM 1';
        case 3:
            return 'DFASM 2';
        case 4:
            return 'DFASM 3';
    }
};

const tableToSQL = (sql, table, yearId) => {
    const gardesArray = [];
    table.days.forEach(
        (day) => {
            day.attributions.forEach(
                (user, terrainId) => {
                    gardesArray.push({
                        id: null,
                        garde_terrain_id: terrainId,
                        date: formatDate(day.date),
                        user_id: user.id,
                        year_id: yearId,
                        traded: false
                    });
                }
            )
        }
    );
    return GardesDB.insert(sql, gardesArray);
};

const tableToXL = (table, users, config) => {
    const wb = new xl.Workbook({
        defaultFont: {
            size: 10,
            name: 'Arial',
            color: '#000000',
            bold: true,
        },
    });

    // TABLEAU
    (() => {
        let ws = wb.addWorksheet('Tableau', {});
        ws.column(1).setWidth(21);
        for (let i = 1; i < 8; i++) {
            ws.column(i + 1).setWidth(23);
        }

        const days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

        const addHeader = (rowId, weekId) => {
            ws.row(rowId).setHeight(21);
            ws.cell(rowId, 3, rowId, 6, true)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        right: {
                            style: 'medium'
                        },
                        left: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#ffff99'
                    },
                    font: {
                        size: 12
                    }
                })
                .string('SEMAINE DU ' + formatDate(table.days[weekId * 7].date) + ' au ' + formatDate(table.days[weekId * 7 + 6].date));

            ws.row(rowId + 2).setHeight(18);
            ws.row(rowId + 3).setHeight(18);

            const horaireStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                border: {
                    top: {
                        style: 'thin'
                    },
                    right: {
                        style: 'thin'
                    },
                    left: {
                        style: 'thin'
                    },
                    bottom: {
                        style: 'thin'
                    }
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#666699'
                },
                font: {
                    size: 8
                }
            });
            ws.cell(rowId + 2, 1, rowId + 2, 8, true)
                .style(horaireStyle)
                .string('HORAIRES');
            ws.cell(rowId + 3, 1, rowId + 3, 3, true)
                .style(horaireStyle)
                .string('SEMAINE : 18h30 -00h ou 18h30 - 08h30 (nuit complete)');
            ws.cell(rowId + 3, 4, rowId + 3, 5, true)
                .style(horaireStyle)
                .string('SAMEDI : 12h30 - 00h ou 12h30 - 8h30 (nuit complete)');
            ws.cell(rowId + 3, 6, rowId + 3, 8, true)
                .style(horaireStyle)
                .string('DIMANCHE : 8h30 - 00h ou 8h30 - 8h30 (nuit complete)');

            ws.row(rowId + 4).setHeight(18);
            ws.row(rowId + 5).setHeight(18);

            const topStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffffff'
                },
                border: {
                    top: {
                        style: 'thin',
                        color: '#000000'
                    },

                    right: {
                        style: 'thin',
                        color: '#000000'
                    },
                    bottom: {
                        style: 'none',
                        color: '#000000'
                    },
                    left: {
                        style: 'thin',
                        color: '#000000'
                    },
                }
            });
            const bottomStyle = wb.createStyle({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffffff'
                },
                border: {
                    top: {
                        style: 'none',
                        color: '#000000'
                    },

                    right: {
                        style: 'thin',
                        color: '#000000'
                    },
                    bottom: {
                        style: 'thin',
                        color: '#000000'
                    },
                    left: {
                        style: 'thin',
                        color: '#000000'
                    },
                }
            });

            ws.cell(rowId + 4, 1)
                .style(topStyle)
                .string('DATE');

            ws.cell(rowId + 5, 1)
                .style(bottomStyle)
                .string('SERVICE');


            for (let dayId = 0; dayId < days.length; dayId++) {
                ws.cell(rowId + 4, dayId + 2)
                    .style(topStyle)
                    .string(days[dayId].toUpperCase());

                const day = table.days[(weekId * days.length) + dayId];
                ws.cell(rowId + 5, dayId + 2)
                    .style(bottomStyle)
                    .string(formatDate(day.date));
            }

        };

        const terrainAmountPerPage = 6;
        const headerRowAmount = 6;
        const terrainRowAmount = 3;

        let currentRowId = 1;
        const weekNb = table.days.length / 7;

        for (let weekId = 0; weekId < weekNb; weekId++) {
            let i = 0;
            while (i < config.terrains.length || (i % terrainAmountPerPage) !== 0) {
                if ((i % terrainAmountPerPage) === 0) {
                    if (weekId !== 0 || i !== 0)
                        currentRowId++; // ADD ROW BELOW PAGE
                    addHeader(currentRowId, weekId);
                    currentRowId += headerRowAmount;
                }

                ws.row(currentRowId).setHeight(14);
                ws.row(currentRowId + 1).setHeight(24);
                ws.row(currentRowId + 2).setHeight(49);


                const terrain = config.terrains[i];

                if (terrain) {
                    ws.cell(currentRowId, 1, currentRowId + 2, 1, true)
                        .style({
                            alignment: {
                                horizontal: 'center',
                                vertical: 'center',
                                wrapText: true
                            },
                            fill: {
                                type: 'pattern',
                                patternType: 'solid',
                                fgColor: '#efeff0'
                            },
                            border: {
                                left: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                right: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                top: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                bottom: {
                                    style: 'thin',
                                    color: '#000000'
                                }
                            },
                            font: {
                                size: 9
                            }
                        })
                        .string(config.terrains[i].name.toUpperCase() + (config.terrains[i].night ? '\n(Nuit complète)' : ''));
                } else {
                    ws.cell(currentRowId, 1, currentRowId + 2, 1, true)
                        .style({
                            alignment: {
                                horizontal: 'center',
                                vertical: 'center',
                                wrapText: true
                            },
                            fill: {
                                type: 'pattern',
                                patternType: 'solid',
                                fgColor: '#a5a5a5'
                            },
                            border: {
                                left: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                right: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                top: {
                                    style: 'thin',
                                    color: '#000000'
                                },
                                bottom: {
                                    style: 'thin',
                                    color: '#000000'
                                }
                            },
                            font: {
                                size: 9
                            }
                        })
                        .string('');
                }

                for (let dayId = (weekId * days.length); dayId < (weekId * days.length + days.length); dayId++) {
                    const day = table.days[dayId];

                    const weekDayId = dayId % 7;

                    if (day && terrain) {
                        const user = day.attributions.get(terrain.id);
                        ws.cell(currentRowId, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                border: {
                                    top: {
                                        style: 'thin'
                                    },
                                    right: {
                                        style: 'thin'
                                    },
                                    left: {
                                        style: 'thin'
                                    },
                                    bottom: {
                                        style: 'none'
                                    }
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: '#ffff99'
                                },
                                font: {
                                    bold: true,
                                    color: '#dd0806'
                                }
                            })
                            .number(user.tableId);
                        ws.cell(currentRowId + 1, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: (weekDayId === 5 ? '#cbfcfd' : (weekDayId === 6 ? '#90b67d' : '#ffffff'))
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'none',
                                    },
                                    bottom: {
                                        style: 'hair',
                                        color: '#000000'
                                    }
                                },
                            })
                            .string(user.firstName + " " + user.lastName);
                        ws.cell(currentRowId + 2, weekDayId + 2)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: (weekDayId === 5 ? '#cbfcfd' : (weekDayId === 6 ? '#90b67d' : '#ffffff'))
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'none',
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: '#000000'
                                    }
                                },
                            })
                            .string('');
                    } else {
                        ws.cell(currentRowId, weekDayId + 2, currentRowId + 2, weekDayId + 2, true)
                            .style({
                                alignment: {
                                    horizontal: 'center',
                                    vertical: 'center',
                                    wrapText: true
                                },
                                fill: {
                                    type: 'pattern',
                                    patternType: 'solid',
                                    fgColor: '#a5a5a5'
                                },
                                border: {
                                    left: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    right: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    top: {
                                        style: 'thin',
                                        color: '#000000'
                                    },
                                    bottom: {
                                        style: 'thin',
                                        color: '#000000'
                                    }
                                },
                                font: {
                                    size: 9
                                }
                            })
                            .string('');
                    }
                }

                currentRowId += terrainRowAmount;
                i++;
            }
        }
    })();

    // COMPTAGE
    (() => {
        ws = wb.addWorksheet('COMPTAGE (Réservé DAM)', {});
        ws.column(1).setWidth(7);
        ws.column(2).setWidth(7);
        ws.column(3).setWidth(9);
        ws.column(4).setWidth(6);
        ws.column(5).setWidth(40);
        ws.column(6).setWidth(20);
        ws.column(7).setWidth(15);

        ws.cell(1, 1, 1, 7, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                }
            })
            .string('DFASM 1 - DFASM 2 - DFASM 3');
        ws.cell(2, 1, 2, 7, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#fcf333'
                },
                font: {
                    color: '#de412c',
                    bold: true,
                },
                border: {
                    left: {
                        style: 'medium',
                    },
                    right: {
                        style: 'medium',
                    },
                    top: {
                        style: 'medium',
                    },
                    bottom: {
                        style: 'medium',
                    }
                },
            })
            .string('ZONE RESERVEE A LA DAM (Ne pas modifier)');
        ws.cell(3, 1)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                }
            })
            .string('SAM');
        ws.cell(3, 2)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                }
            })
            .string('DIM');
        ws.cell(3, 3)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('TOTAL');
        ws.cell(3, 4)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    bottom: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium',
                    }
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#ffff99'
                },
            })
            .string('N°');
        ws.cell(3, 5)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('NOM ET PRENOM');
        ws.cell(3, 6)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('ANNEE');
        ws.cell(3, 7)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                font: {
                    bold: true,
                },
                border: {
                    top: {
                        style: 'medium'
                    },
                    left: {
                        style: 'medium'
                    },
                    right: {
                        style: 'medium'
                    }
                }
            })
            .string('GROUPE');

        const delta = 4;
        for (let userId = 0; userId < users.length; userId++) {
            const user = users[userId];

            ws.cell(userId + delta, 1)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'dotted'
                        },
                        bottom: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.saturdayAmount);
            ws.cell(userId + delta, 2)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        },
                        left: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.sundayAmount);
            ws.cell(userId + delta, 3)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'medium'
                        },
                        bottom: {
                            style: 'medium'
                        },
                        right: {
                            style: 'medium'
                        },
                        left: {
                            style: 'dotted'
                        }
                    }
                })
                .number(user.attribAmount);
            ws.cell(userId + delta, 4)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'none'
                        },
                        bottom: {
                            style: 'dotted'
                        },
                        right: {
                            style: 'thin'
                        },
                        left: {
                            style: 'medium',
                        }
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#ffff99'
                    },
                })
                .number(user.tableId);
            ws.cell(userId + delta, 5)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(user.lastName.toUpperCase() + ' ' + user.firstName);
            ws.cell(userId + delta, 6)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(getPromotionName(user.promotion));
            ws.cell(userId + delta, 7)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    font: {
                        bold: true,
                    },
                    border: {
                        top: {
                            style: 'thin'
                        },
                        right: {
                            style: 'thin'
                        },
                        bottom: {
                            style: 'thin'
                        },
                        left: {
                            style: 'thin',
                        }
                    }
                })
                .string(user.group ? getGroupName(user.group) : 'Non défini');
        }
    })();

    // LISTE ALPHA
    (() => {
        ws = wb.addWorksheet('LISTE ALPHA (Réservé DAM)', {});
        const defaultCellStyle = wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            fill: {
                type: 'pattern',
                patternType: 'solid',
                fgColor: '#ffffff'
            },
            border: {
                left: {
                    style: 'thin',
                    color: '#000000'
                },
                right: {
                    style: 'thin',
                    color: '#000000'
                },
                top: {
                    style: 'thin',
                    color: '#000000'
                },
                bottom: {
                    style: 'thin',
                    color: '#000000'
                }
            },
        });
        const defaultCellStyleBolder = wb.createStyle({
            alignment: {
                horizontal: 'center',
                vertical: 'center',
                wrapText: true
            },
            fill: {
                type: 'pattern',
                patternType: 'solid',
                fgColor: '#ffffff'
            },
            border: {
                left: {
                    style: 'thin',
                    color: '#000000'
                },
                right: {
                    style: 'thin',
                    color: '#000000'
                },
                top: {
                    style: 'thin',
                    color: '#000000'
                },
                bottom: {
                    style: 'thin',
                    color: '#000000'
                }
            },
            font: {
                bold: true
            }
        });

        ws.column(1).setWidth(5);
        ws.column(2).setWidth(20);
        ws.column(3).setWidth(20);
        ws.column(4).setWidth(20);
        ws.column(5).setWidth(20);
        ws.column(6).setWidth(20);
        ws.column(7).setWidth(35);

        ws.cell(1, 1, 1, 7, true)
            .style({
                alignment: {
                    horizontal: 'center',
                    vertical: 'center',
                    wrapText: true
                },
                fill: {
                    type: 'pattern',
                    patternType: 'solid',
                    fgColor: '#fcf305'
                },
                font: {
                    bold: true
                }
            })
            .string('DFASM 1 - DFASM 2 - DFASM 3');
        ws.cell(3, 1)
            .style(defaultCellStyleBolder)
            .string('ID');
        ws.cell(3, 2)
            .style(defaultCellStyleBolder)
            .string('Nom');
        ws.cell(3, 3)
            .style(defaultCellStyleBolder)
            .string('Prénom');
        ws.cell(3, 4)
            .style(defaultCellStyleBolder)
            .string('Promotion');
        ws.cell(3, 5)
            .style(defaultCellStyleBolder)
            .string('Groupe');
        ws.cell(3, 6)
            .style(defaultCellStyleBolder)
            .string('Téléphone');
        ws.cell(3, 7)
            .style(defaultCellStyleBolder)
            .string('Email');

        for (let userId = 0; userId < users.length; userId++) {
            ws.cell(userId + 4, 1)
                .style({
                    alignment: {
                        horizontal: 'center',
                        vertical: 'center',
                        wrapText: true
                    },
                    fill: {
                        type: 'pattern',
                        patternType: 'solid',
                        fgColor: '#fcf305'
                    },
                    border: {
                        left: {
                            style: 'thin',
                            color: '#000000'
                        },
                        right: {
                            style: 'thin',
                            color: '#000000'
                        },
                        top: {
                            style: 'thin',
                            color: '#000000'
                        },
                        bottom: {
                            style: 'thin',
                            color: '#000000'
                        }
                    },
                    font: {
                        bold: true
                    }
                })
                .number(users[userId].tableId);
            ws.cell(userId + 4, 2)
                .style(defaultCellStyle)
                .string(users[userId].lastName);
            ws.cell(userId + 4, 3)
                .style(defaultCellStyle)
                .string(users[userId].firstName);
            ws.cell(userId + 4, 4)
                .style(defaultCellStyle)
                .string(getPromotionName(users[userId].promotion));
            ws.cell(userId + 4, 5)
                .style(defaultCellStyleBolder)
                .string(users[userId].group ? getGroupName(users[userId].group) : 'Non défini');
            ws.cell(userId + 4, 6)
                .style(defaultCellStyleBolder)
                .string(users[userId].phone);
            ws.cell(userId + 4, 7)
                .style(defaultCellStyleBolder)
                .string(users[userId].email);
        }
    })();

    return wb;
};

const getRandomInt = (max) => Math.floor(Math.random() * Math.floor(max));

const genPlanning = (sql, yearId) => {
    return new Promise(
        async (resolve, reject) => {
            const config = (await GardeYearConfigsDB.selectByYearId(sql, yearId)).config;
            const exceptions = parseExceptions(config);
            const users = await getUsers(sql);
            config.terrains = await GardeTerrainsDB.selectAll(sql, ['id', 'name', 'night']);

            const dfasm1 = users
                .filter((user) => user.promotion === 2)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
            const dfasm2 = users
                .filter((user) => user.promotion === 3)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
            const dfasm3 = users
                .filter((user) => user.promotion === 4)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));

            const others = dfasm2.concat(dfasm1);
            const all = dfasm3.concat(dfasm2).concat(dfasm1);
            let tableId = 1;
            all.forEach((user) => {
                user['tableId'] = tableId++;
                user.attribAmount = 0;
                user.saturdayAmount = 0;
                user.sundayAmount = 0;
            });

            const table = createTable(config);

            //STEP 1 : REPART DFASM 3 (No weekend, no night, only 5)
            let lastUserIndex = 0;
            for (let day of table.days) {
                if (!day.weekend && !exceptions.get(4).containsDate(day.date) && !exceptions.get('holydays').containsDate(day.date)) {
                    for (let terrain of config.terrains) {
                        if (!terrain.night) {
                            if (dfasm3[lastUserIndex].attribAmount < 5) {
                                day.attributions.set(terrain.id, dfasm3[lastUserIndex]); // replace
                                dfasm3[lastUserIndex].attribAmount++;
                                lastUserIndex++;
                                lastUserIndex %= dfasm3.length;
                            } else {
                                break;
                            }
                        }
                    }

                    if (dfasm3[dfasm3.length - 1].attribAmount === 5) {
                        break;
                    }
                }
            }

            //STEP 2 : REPART DFASM 1 & DFASM 2 (All terrains, amount not limited, min amount 10)
            let selectionableUsers = others.slice();
            let preAttribOthersIds = [];
            const isAssignable = (user, day) => {
                return !exceptions.get(user.promotion).containsDate(day.date);
            };

            for (let day of table.days) {
                for (let terrain of config.terrains) {
                    if (!day.attributions.get(terrain.id)) {
                        let user;

                        const rejectedIds = [];
                        let randomId;
                        while (true) {
                            if (rejectedIds.length < selectionableUsers.length) {
                                randomId = getRandomInt(selectionableUsers.length);
                                if (!rejectedIds.includes(randomId)) {
                                    user = selectionableUsers[randomId];
                                    if (isAssignable(user, day)) {
                                        selectionableUsers.splice(randomId, 1);
                                        break;
                                    } else {
                                        rejectedIds.push(randomId);
                                    }
                                }
                            } else {
                                randomId = getRandomInt(others.length);
                                user = others[randomId];
                                if (!preAttribOthersIds.includes(randomId)
                                    && isAssignable(user, day)) {
                                    preAttribOthersIds.push(randomId);
                                    break;
                                }
                            }
                        }
                        console.log(user.firstName + ' ' + user.lastName);
                        day.attributions.set(terrain.id, user);
                        user.attribAmount++;
                        if (day.date.getDay() === 6) {
                            user.saturdayAmount++;
                        } else if (day.date.getDay() === 0) {
                            user.sundayAmount++
                        }

                        if (selectionableUsers.length === 0) {
                            selectionableUsers = others.slice();
                            preAttribOthersIds.sort((a, b) => (b - a));

                            for (let id of preAttribOthersIds) {
                                selectionableUsers.splice(id, 1);
                            }

                            preAttribOthersIds = [];
                        }

                    }
                }
            }

            tableToSQL(sql, table, yearId)
                .then(() => resolve())
                .catch((error) => reject(error));
        }
    );
};

const genXL = (sql, yearId) => {
    return new Promise(
        async (resolve, reject) => {
            const config = (await GardeYearConfigsDB.selectByYearId(sql, yearId)).config;
            const users = await getUsers(sql);
            config.terrains = await GardeTerrainsDB.selectAll(sql, ['id', 'name', 'night']);

            const dfasm1 = users
                .filter((user) => user.promotion === 2)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
            const dfasm2 = users
                .filter((user) => user.promotion === 3)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
            const dfasm3 = users
                .filter((user) => user.promotion === 4)
                .sort((u1, u2) => u1.lastName.localeCompare(u2.lastName));
            const all = dfasm3.concat(dfasm2).concat(dfasm1);
            let tableId = 1;
            all.forEach((user) => {
                user['tableId'] = tableId++;
                user.attribAmount = 0;
                user.saturdayAmount = 0;
                user.sundayAmount = 0;
            });

            const getUser = (userId) => {
                for (const user of all) {
                    if (user.id === userId)
                        return user;
                }
            };

            const table = createTable(config);

            GardesDB.selectByYearId(sql, yearId)
                .then(
                    (gardes) => {
                        gardes.forEach(
                            (garde) => {
                                const user = getUser(garde.user_id);
                                const day = table.getDay(garde.date);

                                user.attribAmount++;
                                if (day.date.getDay() === 6) {
                                    user.saturdayAmount++;
                                } else if (day.date.getDay() === 0) {
                                    user.sundayAmount++
                                }

                                day.attributions.set(garde.garde_terrain_id, getUser(garde.user_id));
                            }
                        );
                        resolve(tableToXL(table, all, config));
                    }
                )
                .catch(err => reject(err));
        }
    );
};

module.exports = {
    genPlanning,
    genXL
};


