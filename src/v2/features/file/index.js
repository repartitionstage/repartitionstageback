const {Authorization, SQL, Validator} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils')

module.exports = (router) => {

    const file = require('./file');
    router.route('/document')
        .post(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            file.uploadDocument)
        .get(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            file.getDocuments);

    router.route('/document/:id')
        .all(Validator(
            {
                id: {
                    type: 'number',
                    min: 0
                }
            }, {paramsInURL: true}))
        .get(Authorization(AuthorizationLevel.STUDENT),
            SQL.assign,
            file.getDocument)
        .delete(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            file.deleteDocument);


    return router;
};
