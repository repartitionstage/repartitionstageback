const fs = require('fs');
const Uuid = require('uuid/v4');
const {FilesDB} = require('../../database');
const config = require('../../../config');
const {StandardErrors} = require('../../utils');

const getPath = (uuid) => {
    const addSlash = (config.file.rootPath.charAt(config.file.rootPath.length - 1) === '/');
    return config.file.rootPath + (addSlash ? '/' : '') + uuid;
};

const getNewUUID = async (sql) => {
    return new Promise(
        async (resolve) => {
            let fileUuid;
            do {
                fileUuid = Uuid();
            } while (await FilesDB.isUUIDTaken(sql, fileUuid));
            resolve(fileUuid)
        }
    )
};

const attribNewUUID = async (sql, uuid, fileName) => {
    return new Promise(
        (resolve, reject) => {
            FilesDB.insert(sql, {uuid: uuid, name: fileName})
                .then(() => resolve())
                .catch((err) => reject(err));
        }
    );
};

const deleteFile = async (sql, id) => {
    return new Promise(
        (resolve, reject) => {
            FilesDB.selectById(sql, id, ['uuid'])
                .then(
                    (file) => {
                        FilesDB.deleteById(sql, id)
                            .then(
                                () => {
                                    fs.unlink(getPath(file.uuid), (err) => {
                                        if (err) {
                                            reject(StandardErrors.DELETING_FILE_ERROR);
                                        } else {
                                            resolve()
                                        }
                                    });
                                }
                            ).catch((error) => reject(error));
                    }
                )
                .catch((error) => reject(error));
        }
    );
};

const createNewFile = async (sql, fileName, file) => {
    return new Promise(
        (resolve, reject) => {
            getNewUUID(sql)
                .then(
                    (uuid) => {
                        attribNewUUID(sql, uuid, fileName)
                            .then(
                                () => {
                                    fs.writeFile(getPath(uuid), file,
                                        (error) => {
                                            if (error) {
                                                console.error(error);
                                                reject(StandardErrors.ERROR_OCCURS);
                                            } else {
                                                resolve(uuid);
                                            }
                                        }
                                    );
                                }
                            )
                    }
                )
                .catch((error) => reject(error));
        }
    );
};

module.exports = {
    getNewUUID,
    attribNewUUID,
    deleteFile,
    getPath,
    createNewFile
};
