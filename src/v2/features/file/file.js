const config = require('../../../config');
const {Authorization, AuthorizationLevel, StandardErrors, Responder, Validator} = require('../../utils');
const fileService = require('./service');
const {DocumentsDB, FilesDB, UsersDB} = require('../../database');

const uploadDocument = (req, res, next) => {
    const connection = {req, res, next};
    const multer = require('multer');

    const format = {
        name: {
            type: 'string',
        },
        description: {
            type: 'string',
        },
        promotionId: {
            type: 'string',
        }
    };

    fileService.getNewUUID(req.sql)
        .then(
            (uuid) => {
                const storage = multer.diskStorage({
                    destination: (req2, file, callback) => {
                        callback(null, config.file.rootPath)
                    },
                    filename: (req2, file, callback) => {
                        callback(null, uuid);
                    }
                });
                const upload = multer({storage: storage}).single('document');

                upload(req, res,
                    (err) => {
                        if (err instanceof multer.MulterError) {
                            Responder.sendError(connection, StandardErrors.UPLOAD_ERROR);
                        } else if (err) {
                            Responder.sendError(connection, err);
                        } else {
                            FilesDB.insert(req.sql, {uuid, name: req.file.originalname})
                                .then(
                                    () => {
                                        FilesDB.selectByUUID(req.sql, uuid, ['id'])
                                            .then(
                                                (file) => {
                                                    const name = req.body.name;
                                                    const description = req.body.description;
                                                    const promotionId = req.body.promotionId;

                                                    if (Validator.validateData(format, {
                                                        name,
                                                        description,
                                                        promotionId
                                                    })) {
                                                        const values = {
                                                            name: name,
                                                            description: description,
                                                            promotionId: promotionId,
                                                            fileId: file.id
                                                        };
                                                        DocumentsDB.insert(req.sql, values)
                                                            .then(Responder.sendDefaultSuccess(connection))
                                                            .catch(Responder.sendDefaultError(connection));
                                                    } else {
                                                        fileService.deleteFile(req.sql, file.id)
                                                            .then(Responder.sendDefaultSuccess(connection))
                                                            .catch(Responder.sendDefaultError(connection));
                                                    }
                                                }
                                            )
                                            .catch(Responder.sendDefaultError(connection));
                                    }
                                )
                                .catch(Responder.sendDefaultError(connection));
                        }
                    }
                );
            }
        )
        .catch(Responder.sendDefaultError(connection));
};

const getDocument = (req, res, next) => {
    const connection = {req, res, next};
    DocumentsDB.getAttachedFile(req.sql, req.params.id)
        .then((file) => {
            res.download(fileService.getPath(file.uuid), file.name);
            if (req.sql) req.sql.release();
        })
        .catch(Responder.sendDefaultError(connection));
};

const deleteDocument = async (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    const fileId = (await DocumentsDB.selectById(req.sql, id, ['fileId'])).fileId;
    DocumentsDB.deleteById(req.sql, id)
        .then(
            () => {
                fileService.deleteFile(req.sql, fileId)
                    .then(Responder.sendDefaultSuccess(connection))
                    .catch(Responder.sendDefaultError(connection));
            }
        )
        .catch(Responder.sendDefaultError(connection));
};

const getDocuments = (req, res, next) => {
    const connection = {req, res, next};
    const userId = req.payload.userId;

    if (Authorization.authorize([AuthorizationLevel.ADMIN], req.params.authorization)) {
        DocumentsDB.selectAll(req.sql)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        UsersDB.selectById(req.sql, userId, ['promotionId'])
            .then(
                (user) => {
                    DocumentsDB.getListByPromotion(req.sql, user.promotionId)
                        .then(Responder.sendDefaultData(connection))
                        .catch(Responder.sendDefaultError(connection));
                }
            )
            .catch(Responder.sendDefaultError(connection))
    }
};

module.exports = {
    uploadDocument,
    getDocument,
    deleteDocument,
    getDocuments
};
