const {Authorization, SQL, Validator} = require('../../middlewares');
const {AuthorizationLevel} = require('../../utils')

module.exports = (router) => {
    const contact = require('./contact');
    router.route('/')
        .get(Authorization(AuthorizationLevel.STUDENT),
            Validator(
                {
                    offset: {
                        type: 'number',
                        min: 0
                    },
                    limit: {
                        type: 'number',
                        min: 0
                    }
                }
            ),
            SQL.assign,
            contact.readAll)
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(contact.createFormat),
            SQL.assign,
            contact.create);
    router.route('/:id')
        .all(Validator(
            {
                id: {
                    type: 'number',
                    min: 0
                }
            }, {paramsInURL: true}))
        .get(Authorization(AuthorizationLevel.STUDENT),
            SQL.assign,
            contact.read)
        .put(Authorization(AuthorizationLevel.ADMIN),
            Validator(contact.updateFormat),
            SQL.assign,
            contact.update)
        .delete(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            contact.delete);
    return router;
};
