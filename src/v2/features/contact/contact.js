const {ContactsDB} = require('../../database');
const {RESTRoute} = require('../../utils');

const routes = new RESTRoute(ContactsDB);
routes.createFormat = {
    name: {
        type: 'string'
    },
    description: {
        type: 'string'
    },
    contact: {
        type: 'string'
    }
};

routes.updateFormat = {
    name: {
        type: 'string'
    },
    description: {
        type: 'string'
    },
    contact: {
        type: 'string'
    }
};

routes.defaultSelectFields = ['id', 'name', 'description', 'contact'];

module.exports = routes;
