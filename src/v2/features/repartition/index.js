const {Authorization, Validator, SQL} = require('../../middlewares');
const {
    AuthorizationLevel,
    StandardValidatorFormats
} = require('../../utils')

module.exports = (router) => {

    const promotion = require('./promotion');
    router.route('/promotion')
        .get(
            Validator(
                {
                    onlySelectableOnRegistration: {
                        type: 'boolean',
                        optional: true
                    }
                }, {allowEmptyObject: true}
            ),
            SQL.assign,
            promotion.readAll
        )
        .post(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(promotion.createFormat),
            SQL.assign,
            promotion.create);

    router.route('/promotion/:id')
        .all(Validator({id: {type: 'number', min: 0}}, {paramsInURL: true}))
        .get(
            Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            promotion.read)
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(promotion.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            promotion.update)
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            promotion.delete);


    const category = require('./category');

    router.route('/category')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(category.createFormat),
            SQL.assign,
            category.create)
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(StandardValidatorFormats.READ_ALL(category.createFormat)),
            SQL.assign,
            category.readAll);

    router.route('/category/:id')
        .all(
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                },
                {paramsInURL: true}
            )
        )
        .get(
            Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            category.read)
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(category.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            category.update
        )
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            category.delete
        )

    ;

    const location = require('./location');

    router.route('/location')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(location.createFormat),
            SQL.assign,
            location.create)
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(StandardValidatorFormats.READ_ALL(location.createFormat)),
            SQL.assign,
            location.readAll);

    router.route('/location/:id')
        .all(
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                },
                {paramsInURL: true}
            )
        )
        .get(
            Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            location.read)
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(location.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            location.update
        )
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            location.delete
        );

    const year = require('./year');

    router.route('/year')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(year.createFormat),
            SQL.assign,
            year.create)
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(StandardValidatorFormats.READ_ALL(year.createFormat)),
            SQL.assign,
            year.readAll);

    router.route('/year/:id')
        .get(
            Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(
                {
                    id: [
                        {
                            type: 'number',
                            min: 0
                        },
                        {
                            type: 'string',
                            values: ['active']
                        }
                    ]
                }
            ),
            SQL.assign,
            year.read)
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                },
                {paramsInURL: true}
            ),
            Validator(year.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            year.update)
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                },
                {paramsInURL: true}
            ),
            SQL.assign,
            year.delete);


    const period = require('./period');

    router.route('/period')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(period.createFormat),
            SQL.assign,
            period.create)
        .get(
            Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(
                {
                    promotion: {
                        type: 'number',
                        min: 0
                    },
                    year: [
                        {
                            type: 'number',
                            min: 0
                        },
                        {
                            type: 'string',
                            values: ['active']
                        }
                    ]
                }
            ),
            SQL.assign,
            period.readAll
        );

    router.route('/period/:id')
        .all(
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                }, {paramsInURL: true}
            )
        )
        .get(
            Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            period.read
        )
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(period.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            period.update
        )
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            period.delete
        );

    const terrainType = require('./terraintype');

    const getValidatorFormat = StandardValidatorFormats.READ_ALL(terrainType.createFormat);
    getValidatorFormat.year = {
        type: 'number',
        min: 0,
        optional: true
    };
    getValidatorFormat.promotion = {
        type : 'number',
        min: 0,
        optional: true
    }

    router.route('/terraintype')
        .post(Authorization(AuthorizationLevel.ADMIN),
            Validator(terrainType.createFormat),
            SQL.assign,
            terrainType.create)
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            Validator(StandardValidatorFormats.READ_ALL(terrainType.createFormat)),
            SQL.assign,
            terrainType.readAll)

    router.route('/terraintype/:id')
        .all(
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                }, {paramsInURL: true}
            )
        )
        .get(Authorization(AuthorizationLevel.STUDENT, AuthorizationLevel.ADMIN),
            SQL.assign,
            terrainType.read)
        .put(Authorization(AuthorizationLevel.ADMIN),
            SQL.assign,
            terrainType.update)
        .delete(Authorization(AuthorizationLevel.ADMIN),
            terrainType.delete)

    router.route('/terraintypes/open/year/:yearId/promotion/:promotionId')
        .get(tokenChecker, adminChecker, terraintype.readOpenTerrainTypesByYearAndPromotion);


    return router;
};
