const {PeriodsDB} = require('../../database');
const {Responder, RESTRoute} = require('../../utils');

const routes = new RESTRoute(PeriodsDB,
    {
        order: {
            type: 'number',
            min: 0
        },
        promotionId: {
            type: 'number',
            min: 0
        },
        yearId: {
            type: 'number',
            min: 0
        },
        beginDate: {
            type: 'date'
        },
        endDate: {
            type: 'date'
        },
        group: {
            type: 'number'
        }
    },
    {
        order: {
            type: 'number',
            min: 0,
            optional: true
        },
        promotionId: {
            type: 'number',
            min: 0,
            optional: true
        },
        yearId: {
            type: 'number',
            min: 0,
            optional: true
        },
        beginDate: {
            type: 'date',
            optional: true
        },
        endDate: {
            type: 'date',
            optional: true
        },
        group: {
            type: 'number',
            optional: true
        }
    }
);

routes.defaultSelectFields = ['id', 'order', 'promotion_id', 'year_id', 'begin_date', 'end_date', 'group'];

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};
    const promotion = req.query.promotion;
    const year = req.query.year;

    PeriodsDB.selectAllByPromotionAndYear(req.sql, routes.defaultSelectFields, promotion, year)
        .then(Responder.sendDefaultData(connection))
        .catch(Responder.sendDefaultError(connection));
};

module.exports = routes;

