const {LocationsDB} = require('../../database');
const {RESTRoute} = require('../../utils');

const routes = new RESTRoute(LocationsDB,
    {
        name: {
            type: 'string',
            minLength: 1
        },
        distance: {
            type: 'number',
            min: 0
        }
    },
    {
        name: {
            type: 'string',
            minLength: 1,
            optional: true
        },
        distance: {
            type: 'number',
            min: 0,
            optional: true
        }
    });

routes.defaultSelectFields = ['id', 'name', 'distance'];


module.exports = routes;
