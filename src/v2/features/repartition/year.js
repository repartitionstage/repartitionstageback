const {YearsDB} = require('../../database');
const {Responder, RESTRoute, BooleanParser} = require('../../utils');

const routes = new RESTRoute(YearsDB,
    {
        name: {
            type: 'string'
        }
    },
    {
        name: {
            type: 'string'
        },
        active: {
            type: 'boolean'
        }
    });

routes.defaultSelectFields = ['id', 'name', 'active'];

routes.read = (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    if (id === 'active') {
        YearsDB.selectByActive(req.sql, routes.defaultSelectFields.slice())
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        YearsDB.selectById(req.sql, id, routes.defaultSelectFields.slice())
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    }
};

routes.update = (req, res, next) => {
    const connection = {req, res, next};
    const id = Number(req.params.id);
    const object = req.body;
    delete object.id;

    if (object.active !== undefined)
        object.active = BooleanParser(object.active);

    if (object.active === true) {
        YearsDB.inactiveCurrentYear(req.sql)
            .then(
                () => {
                    YearsDB.updateById(req.sql, id, object)
                        .then(Responder.sendDefaultSuccess(connection))
                        .catch(Responder.sendDefaultError(connection));
                }
            )
            .catch(Responder.sendDefaultError(connection));
    } else {
        YearsDB.updateById(req.sql, id, object)
            .then(Responder.sendDefaultSuccess(connection))
            .catch(Responder.sendDefaultError(connection));
    }
}

module.exports = routes;
