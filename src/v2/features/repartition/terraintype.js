const {TerrainTypesDB} = require('../../../v2/database');
const {Authorization, AuthorizationLevel, BooleanParser, RESTRoute, Responder, StandardErrors, Validator} = require('../../utils');

const routes = new RESTRoute(TerrainTypesDB, {
    name: {
        type: 'string',
        minLength: 0
    },
    locationId: {
        type: 'number',
        min: 0
    },
    desc: {
        type: 'string'
    },
    respFirstName: {
        type: 'string',
        minLength: 0
    },
    respLastName: {
        type: 'string',
        minLength: 0
    },
    respMail: {
        type: 'string',
        minLength: 0
    },
    categoriesIds: {
        type: 'array'
    }
},
    {
        id: {
            type: 'number'
        },
        name: {
            type: 'string',
            minLength: 0
        },
        locationId: {
            type: 'number',
            min: 0
        },
        desc: {
            type: 'string'
        },
        respFirstName: {
            type: 'string',
            minLength: 0
        },
        respLastName: {
            type: 'string',
            minLength: 0
        },
        respMail: {
            type: 'string',
            minLength: 0
        },
        maxChoices: {
            type: 'number',
            min: -1
        },
        maxAttributions: {
            type: 'number',
            min: -1
        },
        maxAttributionsPerYear: {
            type: 'number',
            min: -1
        }
    });

routes.defaultSelectFields = ['id', 'name', 'locationId', 'desc', 'respFirstName', 'respLastName', 'respMail', 'maxChoices', 'maxAttributions', 'maxAttributionsPerYear', 'categoriesIds'];

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};

    const promotionId = req.params.promotionId;
    const yearId = req.params.yearId;

    if(!promotionId && !yearId) {
        TerrainTypesDB.selectAll(req.sql, this.defaultSelectFields.slice(), Number(req.query.limit), Number(req.query.offset), req.query.orderBy, BooleanParser(req.query.asc))
            .then(Responder.sendDefaultData({req, res, next}))
            .catch(Responder.sendDefaultError({req, res, next}));
    } else if ((promotionId && !yearId) || (yearId && !promotionId)) {
        Responder.sendError(res, StandardErrors.INVALID_FORMAT);

    } else if (promotionId && yearId) {
        if (Authorization.authorize([AuthorizationLevel.ADMIN],  req.params.authorization)) {
            TerrainTypesDB.selectOpenTerrainTypesByYearAndPromotion(req.sql, ['tt.id', 'tt.name', yearId, promotionId])
                .then(Responder.sendDefaultData(connection))
                .catch(Responder.sendDefaultError(connection));
        } else {
            Responder.sendError(res, StandardErrors.FORBIDDEN);
        }
    }
};

module.exports = routes;
