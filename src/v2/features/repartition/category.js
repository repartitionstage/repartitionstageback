const {CategoriesDB} = require('../../database');
const {RESTRoute} = require('../../utils');

const routes = new RESTRoute(CategoriesDB,
    {
        name: {
            type: 'string'
        },
        maxAttributions: {
            type: 'number',
            min: -1,
            optional: true
        },
        maxAttributionsPerYear: {
            type: 'number',
            min: -1,
            optional: true
        },
        maxChoices: {
            type: 'number',
            min: -1,
            optional: true
        }
    },
    {
        name: {
            type: 'string',
            optional: true
        },
        maxAttributions: {
            type: 'number',
            min: -1,
            optional: true
        },
        maxAttributionsPerYear: {
            type: 'number',
            min: -1,
            optional: true
        },
        maxChoices: {
            type: 'number',
            min: -1,
            optional: true
        }
    }
);

routes.defaultSelectFields = ['id', 'name', 'maxAttributions', 'maxAttributionsPerYear', 'maxChoices'];

module.exports = routes;
