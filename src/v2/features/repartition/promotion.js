const {PromotionsDB} = require('../../database');
const {BooleanParser, Responder, RESTRoute} = require('../../utils');

const routes = new RESTRoute(PromotionsDB,
    {
        name: {
            type: 'string'
        }
    },
    {
        name: {
            type: 'string'
        },
        openedRegistration : {
            type: 'boolean'
        }
    }
    );

routes.defaultSelectFields = ['id', 'name', 'opened_registration'];

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};

    const onlySelectable = BooleanParser(req.query['onlySelectableOnRegistration']);

    if (onlySelectable) {
        PromotionsDB.selectSelectableOnRegistration(req.sql, routes.defaultSelectFields)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        PromotionsDB.selectAll(req.sql, routes.defaultSelectFields)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    }
};

module.exports = routes;
