const {UsersDB, UserRanksDB} = require('../../database');
const {LogsService, TokenService} = require('../../services');
const {Responder, StandardErrors, Password} = require('../../utils');

/**
 * Check if token provide in body is still valid/legit
 * @param req
 * @param res
 */
const isTokenValid = (req, res) => {
    TokenService.isTokenLegit(req.body.token)
        .then(() => Responder.sendSuccessWithData({req, res}, {valid: true}))
        .catch(() => Responder.sendSuccessWithData({req, res}, {valid: false}));
};

/**
 * Give a new valid access token if refresh token, provided in headers as bearer token, is still valid
 * @param req
 * @param res
 * @param next
 */
const refreshToken = (req, res, next) => {
    const connection = {req, res, next};
    let token = req.headers['authorization'].replace('Bearer ', '');

    TokenService.isTokenLegit(token, TokenService.Types.REFRESH)
        .then((decoded) => {
            const payload = decoded.payload;

            delete payload.iat;
            delete payload.exp;
            delete payload.sub;

            TokenService.getNewAccessToken(decoded.payload)
                .then(Responder.sendDefaultData(connection))
                .catch(Responder.sendDefaultError(connection));
        })
        .catch(() => Responder.sendError(connection, StandardErrors.INVALID_TOKEN));
};

/**
 * Give two tokens : access and refresh, only if email-password match with existing ones in database.
 * Token's payload contains user id and authorization level.
 * Logged request if succeed
 * @param req
 * @param res
 * @param next
 */
const login = (req, res, next) => {
    const connection = {req, res, next};
    const object = req.body;

    UsersDB.selectByEmail(req.sql, object.email, ['id', 'hash'])
        .then(
            (result) => {
                Password.compare(object.password, result.hash)
                    .then(
                        (similar) => {
                            if (similar) {
                                UserRanksDB.selectByUserId(req.sql, result.id)
                                    .then(
                                        async (ranks) => {
                                            const payload = {
                                                userId: result.id,
                                                authorization: ranks
                                            };
                                            try {
                                                const tokens = {
                                                    access: await TokenService.getNewAccessToken(payload),
                                                    refresh: await TokenService.getNewRefreshToken(payload),
                                                };
                                                LogsService.append(req.sql, result.id, result.id, req.ip, LogsService.TYPES.LOGIN, {})
                                                    .then(() => Responder.sendSuccessWithData(connection, {
                                                        userId: result.id,
                                                        tokens
                                                    }))
                                                    .catch(Responder.sendDefaultError(connection));
                                            } catch (e) {
                                                Responder.sendError(connection, e);
                                            }
                                        }
                                    )
                                    .catch(Responder.sendDefaultError(connection));
                            } else {
                                Responder.sendError(connection, StandardErrors.USER_PASSWORD_MISMATCH);
                            }
                        }
                    )
                    .catch(Responder.sendDefaultError(connection));
            }
        )
        .catch(Responder.sendDefaultError(connection));
};

module.exports = {
    isTokenValid,
    refreshToken,
    login
}
