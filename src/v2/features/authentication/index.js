const {Validator, SQL} = require('../../middlewares');
const authentication = require('./authentication');

module.exports = (router) => {

    router.route('/login')
        .post(
            Validator({
                email: {
                    type: 'email'
                },
                password: {
                    type: 'string'
                }
            }),
            SQL.assign,
            authentication.login);

    router.route('/token')
        .get(authentication.refreshToken)
        .post(
            Validator({
                token: {
                    type: 'string',
                    pattern: '^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*$'
                }
            }),
            authentication.isTokenValid
        );

    return router;
};
