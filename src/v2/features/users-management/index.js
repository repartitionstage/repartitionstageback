const {Authorization, RepartitionPhaseChecker, SQL, Validator} = require('../../middlewares');
const {AuthorizationLevel, StandardValidatorFormats} = require('../../utils');
const {RankRequestsDB, UsersDB} = require('../../database');

module.exports = (router) => {

    const user = require('./user');

    router.route('/user')
        .post(
            Validator(user.createFormat),
            SQL.assign,
            user.create)
        .get(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(StandardValidatorFormats.READ_ALL(UsersDB.jsToSqlLUT)),
            SQL.assign,
            user.readAll);

    router.route('/user/:id')
        .all(
            Validator({
                id: {
                    type: 'number',
                    min: 0
                }
            }, {paramsInURL: true})
        )
        .get(
            Authorization(AuthorizationLevel.ADMIN, AuthorizationLevel.STUDENT),
            Validator({
                type: {
                    type: 'string',
                    values: ['header', 'profile'],
                    optional: true
                }
            }),
            SQL.assign,
            user.read)
        .put(
            Authorization(AuthorizationLevel.ADMIN, AuthorizationLevel.STUDENT),
            Validator(user.updateFormat, {allowEmptyObject: false}),
            SQL.assign,
            user.update);

    router.route('/reset-password')
        .post(
            Validator({
                email: {
                    type: 'email'
                }
            }),
            SQL.assign,
            user.requestPassword)
        .get(
            Validator({
                token: {
                    type: 'string',
                    pattern: /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/,
                }
            }),
            SQL.assign,
            user.isResetPasswordTokenValid)
        .put(
            Validator({
                token: {
                    type: 'string',
                    pattern: /^[A-Za-z0-9-_=]+\.[A-Za-z0-9-_=]+\.?[A-Za-z0-9-_.+/=]*$/
                },
                email: {
                    type: 'email'
                },
                password: {
                    type: 'string',
                    minLength: 6
                }
            }),
            SQL.assign,
            user.resetPassword); //post new password to be save as user password

    const rankRequest = require('./rank/rank-request');

    const readAllValidatorFormat = StandardValidatorFormats.READ_ALL(RankRequestsDB.jsToSqlLUT);
    readAllValidatorFormat.userId = {
        type: 'number',
        min: 0,
        optional: true
    }

    router.route('/rank-request')
        .post(
            Authorization(),
            Validator(rankRequest.createFormat),
            SQL.assign,
            rankRequest.create)
        .get(
            Authorization(),
            Validator(readAllValidatorFormat),
            SQL.assign,
            rankRequest.readAll)
        .put(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(
                {
                    id: {
                        type: 'number',
                        min: 0
                    }
                }
            ),
            SQL.assign,
            rankRequest.accept
        )
        .delete(
            Authorization(),
            Validator({
                id: {
                    type: 'number',
                    min: 0
                }
            }),
            SQL.assign,
            rankRequest.refuseOrCancel
        )


    const userRank = require('./rank/user-rank');

    router.route('/rank')
        .post(
            Authorization(AuthorizationLevel.ADMIN),
            Validator(userRank.createFormat),
            SQL.assign,
            userRank.create
        )
        .get(
            Authorization(),
            Validator({
                user: {
                    type: 'number',
                    min: 0
                }
            }),
            SQL.assign,
            userRank.readAll
        )
        .delete(
            Authorization(AuthorizationLevel.ADMIN),
            Validator({
                user: {
                    type: 'number',
                    min: 0
                },
                rank: {
                    type: 'number',
                    min: 0
                }
            }),
            SQL.assign,
            userRank.delete
        )


    return router;
};
