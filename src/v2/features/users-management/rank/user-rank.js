const {UserRanksDB} = require('../../../database');
const RankService = require('./service');
const {Authorization, AuthorizationLevel, BooleanParser, Responder, RESTRoute, StandardErrors} = require('../../../utils');

const routes = new RESTRoute(UserRanksDB,
    {
        user: {
            type: 'number',
            min: 0
        },
        rank: {
            type: 'number',
            min: 0
        }
    });

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};
    const userId = Number(req.query.user)

    if (userId === req.payload.userId || Authorization.authorize([AuthorizationLevel.ADMIN], req.payload.authorization)){
        UserRanksDB.selectByUserId(req.sql, userId)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        Responder.sendError(connection, StandardErrors.FORBIDDEN);
    }
};

routes.delete = (req, res, next) => {
    const connection = {req, res, next};
    const userId = Number(req.query.user);
    const rankId = Number(req.query.rank);
    UserRanksDB.delete(req.sql, userId, rankId)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));
}

module.exports = routes;
