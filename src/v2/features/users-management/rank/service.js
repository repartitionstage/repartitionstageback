const {UserRanksDB} = require('../../../database');

const hasRank = (sql, userId, rankId) => {
    return new Promise(
        (resolve, reject) => {
            UserRanksDB.selectByUserId(sql, userId)
            .then(
                (rankArray) => {
                    if (rankArray.indexOf(rankId) !== -1) {
                        resolve(true);
                    } else {
                        resolve(false);
                    }

                }
            )
            .catch((err) => reject(err))
        }
    );
}

const giveRank = (sql, userId, rankId) => {
    return new Promise(
        (resolve, reject) => {
            UserRanksDB.insert(sql, {userId, rankId})
                .then(() => resolve())
                .catch(
                    (err) => {
                        reject(err)
                    });
        }
    )
}

module.exports = {
    hasRank,
    giveRank
};
