const {RankRequestsDB} = require('../../../database');
const RankService = require('./service');
const {
    Authorization,
    AuthorizationLevel,
    BooleanParser,
    Responder,
    RESTRoute,
    StandardErrors
} = require('../../../utils');
const {LogsService} = require('../../../services');

const routes = new RESTRoute(RankRequestsDB, {
    rankId: {
        type: 'number',
        min: 0
    }
});

routes.create = (req, res, next) => {
    const connection = {req, res, next};

    const {userId, authorization} = req.payload;
    const rankId = Number(req.body.rankId);

    if (Array.isArray(authorization) && authorization.indexOf(rankId) !== -1) {
        Responder.sendError(connection, StandardErrors.ALREADY_EXIST)
        return;
    }

    RankService.hasRank(req.sql, userId, rankId)
        .then(
            (hasRank) => {
                if (hasRank) {
                    Responder.sendError(connection, StandardErrors.ALREADY_EXIST);
                } else {
                    RankRequestsDB.insert(req.sql, {userId, rankId})
                        .then(Responder.sendDefaultSuccess(connection))
                        .catch(Responder.sendDefaultError(connection));
                }
            }
        ).catch(Responder.sendDefaultError(connection))
};

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};
    const {userId, authorization} = req.payload;

    const optionalUserId = Number(req.query.user);

    const limit = Number(req.query.limit);
    const offset = Number(req.query.offset);
    const orderBy = req.query.orderby;
    const asc = BooleanParser(req.query.asc);

    if (!optionalUserId && Authorization.authorize([AuthorizationLevel.ADMIN], authorization)) {
        RankRequestsDB.selectAll(req.sql, ['id', 'userId', 'rankId'], limit, offset, orderBy, asc)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else if (optionalUserId === userId || Authorization.authorize([AuthorizationLevel.ADMIN], authorization)) {
        RankRequestsDB.selectByUserId(req.sql, optionalUserId)
            .then(Responder.sendDefaultData(connection))
            .catch(Responder.sendDefaultError(connection));
    } else {
        Responder.sendError(connection, StandardErrors.FORBIDDEN);
    }
};

routes.accept = (req, res, next) => {
    const connection = {req, res, next};
    const rankRequestId = Number(req.body.id)

    RankRequestsDB.selectById(req.sql, rankRequestId, ['rankId', 'userId'])
        .then(
            ({rankId, userId}) => {
                RankService.giveRank(req.sql, userId, rankId)
                    .then(
                        () => {
                            RankRequestsDB.deleteById(req.sql, rankRequestId)
                                .then(
                                    () => {
                                        LogsService.append(req.sql, req.payload.userId, userId, req.ip, LogsService.TYPES.RANK_REQ_ACCEPT, {rankId})
                                            .then(Responder.sendDefaultSuccess(connection))
                                            .catch(Responder.sendDefaultError(connection));
                                    }
                                )
                                .catch(Responder.sendDefaultError(connection));
                        }
                    )
                    .catch(Responder.sendDefaultError(connection));
            }
        )
        .catch(Responder.sendDefaultError(connection));
};

routes.refuseOrCancel = (req, res, next) => {
    const connection = {req, res, next};
    const rankRequestId = Number(req.query.id);

    RankRequestsDB.selectById(req.sql, rankRequestId, ['rankId', 'userId'])
        .then(
            ({rankId, userId}) => {
                if (userId === req.payload.userId || Authorization.authorize([AuthorizationLevel.ADMIN], req.payload.authorization)) {
                    RankRequestsDB.deleteById(req.sql, rankRequestId)
                        .then(
                            () => {
                                const type = userId === req.payload.userId ? LogsService.TYPES.RANK_REQ_CANCEL : LogsService.TYPES.RANK_REQ_REFUSE;
                                LogsService.append(req.sql, req.payload.userId, userId, req.ip, type, {rankId})
                                    .then(Responder.sendDefaultSuccess(connection))
                                    .catch(Responder.sendDefaultError(connection))
                            }
                        )
                } else {
                    Responder.sendError(connection, StandardErrors.FORBIDDEN);
                }
            })
        .catch(Responder.sendDefaultError(connection));
}

module.exports = routes;
