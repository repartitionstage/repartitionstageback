const {UsersDB} = require('../../database');
const UsersService = require('./service');
const {Authorization, AuthorizationLevel, BooleanParser, Responder, RESTRoute, StandardErrors} = require('../../utils');

const routes = new RESTRoute(UsersDB,
    {
        firstName: {
            type: 'string',
        },
        lastName: {
            type: 'string',
        },
        email: {
            type: 'email'
        },
        phone: {
            type: 'number',
            pattern: '^(0[6,7])([0-9]{8})$'
        },
        password: {
            type: 'string',
            minLength: 6
        }
    },
    {
        firstName: {
            type: 'string',
            optional: true
        },
        lastName: {
            type: 'string',
            optional: true
        }, email: {
            type: 'email',
            optional: true
        },
        phone: {
            type: 'number',
            pattern: '^(0[6,7])([0-9]{8})$',
            optional: true
        }
    }
);

routes.defaultSelectFields = ['id', 'first_name', 'last_name'];

routes.create = (req, res, next) => {
    const connection = {req, res, next};
    UsersService.register(req.sql, req.body)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));
};

routes.readAll = (req, res, next) => {
    const connection = {req, res, next};
    const limit = Number(req.query.limit);
    const offset = Number(req.query.offset);
    const orderBy = req.query.orderby;
    const asc = BooleanParser(req.query.asc);

    UsersDB.countUsers(req.sql)
        .then(
            (amount) => {
                UsersDB.selectAll(req.sql, ['id', 'first_name', 'last_name', 'email', 'phone'], limit, offset, orderBy, asc)
                    .then((result) => {
                        Responder.sendSuccessWithData(connection, {
                            list: result,
                            totalAmount: amount
                        });
                    })
                    .catch(Responder.sendDefaultError(connection));
            }
        ).catch(Responder.sendDefaultError(connection));


};

routes.read = (req, res, next) => {
    const connection = {req, res, next};
    let targetId = Number(req.params.id);
    const getterId = Number(req.payload.userId);

    if (getterId !== targetId && !Authorization.authorize(req.payload.authorization, [AuthorizationLevel.ADMIN])) {
        Responder.sendError(connection, StandardErrors.FORBIDDEN);
        return;
    }

    const type = req.query.type;
    let fields;
    switch (type) {
        case 'header':
            fields = ['id', 'first_name', 'last_name'];
            break;
        case 'profile':
            fields = ['id', 'first_name', 'last_name', 'email', 'phone'];
            break;
        default:
            fields = ['id'];
            break;
    }

    UsersDB.selectById(req.sql, targetId, fields)
        .then(Responder.sendDefaultData(connection))
        .catch(Responder.sendDefaultError(connection));
};

routes.update = (req, res, next) => {
    const connection = {req, res, next};
    const targetId = Number(req.params.id);
    const getterId = Number(req.payload.userId);

    if (getterId !== targetId && !Authorization.authorize(req.payload.authorization, AuthorizationLevel.ADMIN)) {
        Responder.sendError(connection, StandardErrors.FORBIDDEN);
        return;
    }

    UsersDB.updateById(req.sql, targetId, req.body)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));
};

routes.requestPassword = (req, res, next) => {
    const connection = {req, res, next};
    const email = req.body.email

    UsersService.requestPassword(req.sql, email)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));
};

routes.isResetPasswordTokenValid = (req, res, next) => {
    const connection = {req, res, next};
    const token = req.query.token;
    UsersService.isResetPasswordTokenValid(req.sql, token)
        .then((valid) => Responder.sendSuccessWithData(connection, {valid}))
        .catch(Responder.sendDefaultError(connection));
};

routes.resetPassword = (req, res, next) => {
    const connection = {req, res, next};
    const object = req.body;

    UsersService.resetPassword(req.sql, object.token, object.email, object.password)
        .then(Responder.sendDefaultSuccess(connection))
        .catch(Responder.sendDefaultError(connection));

};

module.exports = routes;
