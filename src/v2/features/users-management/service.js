const config = require('../../../config');
const {UsersDB, ResetPasswordTokensDB} = require('../../database');
const {Password, StandardErrors} = require('../../utils')
const {GoogleService, TokenService} = require('../../services')

const register = (sql, object) => {
    return new Promise(
        (resolve, reject) => {
            Password.encrypt(object.password)
                .then(
                    (hash) => {
                        object.hash = hash;
                        delete object.password;
                        UsersDB.insert(sql, object)
                            .then(() => resolve())
                            .catch((error) => reject(error));
                    }
                )
                .catch((e) => reject(e));
        });
};

const sendResetPasswordMail = (token, user) => {
    const subject = 'Tirajosaure : Récupération de mot de passe';

    const resetLink = 'https://' + ((config.general.prod === true) ? '' : 'dev.') + 'tirajosaure.ars-amiens.fr/#/auth/reset-password/' + token;

    const html = '<html lang="fr">' +
        '<p>Bonjour ' + user.firstName + ', </p>' +
        '<p>Voici le lien de changement de mot de passe <span>Tirajosaure</span> :</p>' +
        '<p><a href=\"' + resetLink + '\">Récupération</a></p>' +
        '<p>Si ce lien ne fonctionne pas, copie-colle ce lien dans ta barre URL : ' + resetLink + '</p>' +
        '<p><span>L\'équipe de Tirajosaure</span></p>' +
        '</html>';
    return GoogleService.sendMail(user.email, subject, html);
};

const requestPassword = (sql, email) => {
    return new Promise(
        (resolve, reject) => {
            UsersDB.selectByEmail(sql, email, ['id', 'first_name', 'last_name', 'email'])
                .then(
                    (user) => {
                        TokenService.getNewResetPasswordToken({userId: user.id})
                            .then(
                                (token) => {
                                    ResetPasswordTokensDB.insert(sql, {
                                        token: token,
                                        userId: user.id
                                    })
                                        .then(() => {
                                                sendResetPasswordMail(token, user)
                                                    .then(() => resolve())
                                                    .catch(() => reject(StandardErrors.MAIL_SENDING_ERROR))
                                            }
                                        )
                                        .catch((error) => reject(error));
                                }
                            )
                            .catch((err) => reject(err))
                    }
                )
                .catch((error) => reject(error));
        }
    )
};

const isResetPasswordTokenValid = (sql, token) => {
    return new Promise(
        (resolve, reject) => {
            TokenService.isTokenLegit(token, TokenService.Types.RESET_PASSWORD)
                .then(() => {
                    ResetPasswordTokensDB.selectByToken(sql, ['id'], token)
                        .then(() => resolve(true))
                        .catch((error) => {
                            if (error === StandardErrors.OBJECT_NOT_FOUND) {
                                resolve(false);
                            } else {
                                reject(error);
                            }
                        });
                })
                .catch(() => {
                    ResetPasswordTokensDB.deleteByToken(sql, token)
                        .then(() => resolve(false))
                        .catch((error) => reject(error));
                });
        }
    )
};

const resetPassword = (sql, token, email, password) => {
    return new Promise(
        (resolve, reject) => {
            TokenService.isTokenLegit(token, TokenService.Types.RESET_PASSWORD)
                .then(
                    () => {
                        ResetPasswordTokensDB.selectByToken(sql, ['id', 'user_id'], token)
                            .then(
                                (object) => {
                                    UsersDB.selectById(sql, object.userId, ['email'])
                                        .then(
                                            (user) => {
                                                if (user.email === email) {
                                                    Password.encrypt(password)
                                                        .then(
                                                            (hash) => {
                                                                UsersDB.updateById(sql, object.userId, {hash})
                                                                    .then(
                                                                        () => {
                                                                            ResetPasswordTokensDB.deleteByToken(sql, token)
                                                                                .then(() => resolve())
                                                                                .catch((err) => reject(err));
                                                                        }
                                                                    )
                                                                    .catch((err) => reject(err));
                                                            }
                                                        )
                                                        .catch((err) => reject(err));
                                                } else {
                                                    reject(StandardErrors.FORBIDDEN);
                                                }
                                            }
                                        )
                                        .catch((err) => reject(err));
                                }
                            )
                            .catch((err) => {
                                if (err === StandardErrors.OBJECT_NOT_FOUND) {
                                    reject(StandardErrors.INVALID_TOKEN);
                                } else {
                                    reject(err);
                                }
                            });
                    }
                )
                .catch(
                    (err) => {
                        ResetPasswordTokensDB.deleteByToken(sql, token)
                            .then(() => reject(StandardErrors.INVALID_TOKEN))
                            .catch((error) => reject(error))
                    });

        }
    );
};

module.exports = {
    register,
    isResetPasswordTokenValid,
    requestPassword,
    resetPassword,
};
