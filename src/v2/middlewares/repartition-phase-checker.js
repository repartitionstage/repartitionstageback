const {PhasesDB, UsersDB} = require('../database');
const {StandardErrors, Responder} = require('../utils');

const groupPhase = (connection) => {
    const phase = connection.req.phase;
    UsersDB.selectById(connection.req.sql, connection.req.payload.userId, ['redoubling', 'groupBlocked'])
        .then(
            (user) => {
                const groupBlocked = Boolean(user.groupBlocked);

                if (groupBlocked) {
                    Responder.sendError(connection, {code: 403, reason: 'group-blocked'});
                } else if (phase.config.onlyRedoubling && phase.config.onlyRedoubling === true) {
                    const redoubling = Boolean(user.redoubling);
                    if (redoubling) {
                        connection.next();
                    } else {
                        Responder.sendError(connection, {code: 403, reason: 'not-redoubling'});
                    }
                } else {
                    connection.next();
                }
            }
        )
        .catch(
            (error) => {
                Responder.sendDefaultError(connection, error);
            });

}


module.exports = (phaseTypes) => (req, res, next) => {
    const connection = {req, res, next};
    const userId = req.payload.userId;

    phaseTypes = Array.isArray(phaseTypes) ? phaseTypes : [phaseTypes];

    if (userId) {
        PhasesDB.selectActivePhaseOfUser(req.sql, userId)
            .then((result) => {
                result.config = JSON.parse(result.config);

                req.phase = result;

                if (result.config.type && phaseTypes.indexOf(result.config.type) !== -1) {

                    switch (result.config.type) {
                        case 'group':

                            break;
                        case 'spreading':
                        case 'terrain':
                            next();
                            break;
                    }

                } else {
                    Responder.sendError(connection, {code: 403, reason: 'wrong-phase'});
                }
            })
            .catch((error) => {
                Responder.sendError(connection, error);
            });

    } else {
        Responder.sendError(connection, StandardErrors.MISSING_TOKEN);
    }
};
