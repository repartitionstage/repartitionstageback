const {StandardErrors, Table} = require('../utils');

const promotions = new Table('promotions',
    {
        'id': 'id',
        'name': 'name',
        'openedRegistration': 'opened_registration'
    },
    {
        'id': 'id',
        'name': 'name',
        'opened_registration': 'openedRegistration'
    });


promotions.selectSelectableOnRegistration = (sql, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM promotions WHERE `selectableOnRegistration` = 1', [fields],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    );
};

module.exports = promotions;
