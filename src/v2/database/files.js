const {StandardErrors, Table} = require('../utils');

const files = new Table('files',
    {
        'id': 'id',
        'uuid': 'uuid',
        'name': 'name'
    },
    {
        'id': 'id',
        'uuid': 'uuid',
        'name': 'name'
    });

files.selectByUUID = (sql, uuid, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM `files` WHERE uuid = ? ',
                [fields, uuid],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                });
        }
    );
};

files.isUUIDTaken = (sql, uuid) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT uuid FROM `files` WHERE uuid = ? ',
                [uuid],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    }
                });
        }
    );
};

module.exports = files;
