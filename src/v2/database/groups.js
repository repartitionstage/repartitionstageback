const {Table} = require('../utils');

const groups = new Table('groups',
    {
        'id': 'id',
        'name': 'name'
    },
    {
        'id': 'id',
        'name': 'name'
    });

groups.GROUP = {
    NONE: 1,
    A: 2,
    B: 3,
    TWICE: 4
};

module.exports = groups;
