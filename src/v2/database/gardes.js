const {StandardErrors, Table, SQL} = require('../utils');

const gardes = new Table('gardes',
    {
        'id': 'id',
        'gardeTerrainId': 'garde_terrain_id',
        'date': 'date',
        'userId': 'user_id',
        'yearId': 'year_id',
        'traded': 'traded'
    },
    {
        'id': 'id',
        'garde_terrain_id': 'gardeTerrainId',
        'date': 'date',
        'user_id': 'userId',
        'year_id': 'yearId',
        'traded': 'traded'
    }
    );

gardes.insert = (sql, values) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('INSERT INTO gardes ' + SQL.getInsertQuery(values), [values],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        });
};

gardes.select = (sql, userId, dates) => {
    return new Promise(
        (resolve, reject) => {
            let query = 'SELECT g.id as id, g.garde_terrain_id as terrainId, g.date as date, g.user_id as userId, u.firstName as userFirstName, u.lastName as userLastName, gt.name as terrainName, gt.night as isNight, y.id as yearId, y.name as yearName FROM gardes g INNER JOIN garde_terrains gt ON g.garde_terrain_id = gt.id INNER JOIN users u on g.user_id = u.id INNER JOIN years y on g.year_id = y.id WHERE ';
            let args = [];
            if (userId) {
                query += 'g.user_id = ?';
                args.push(userId);
            }
            if (userId && dates) {
                query += ' AND ';
            }
            if (dates) {
                query += 'g.date IN (?)';
                args.push(dates);
            }

            sql.query(query, args,
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    )
}

gardes.selectByUserId = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT g.id as id, g.garde_terrain_id as terrainId, g.date as date, g.user_id as userId, gt.name as terrainName, gt.night as isNight, y.id as yearId, y.name as yearName FROM gardes g INNER JOIN garde_terrains gt ON g.garde_terrain_id = gt.id INNER JOIN years y ON y.id = g.year_id WHERE g.user_id = ?',
                [userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};
gardes.selectByDays = (sql, days) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT g.id as id, g.garde_terrain_id as terrainId, g.date as date, g.user_id as userId, u.firstName as userFirstName, u.lastName as userLastName FROM gardes g INNER JOIN users u on g.user_id = u.id WHERE g.date IN (?)',
                [days],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};
gardes.selectFreeGardes = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("SELECT g.id as id, g.garde_terrain_id as terrainId, g.date as `date`, gt.name as terrainName FROM gardes g INNER JOIN garde_terrains gt ON g.garde_terrain_id = gt.id WHERE g.user_id = 0;",
                [],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};
gardes.removeUserFromGarde = (sql, gardeId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query("UPDATE gardes SET user_id = 0 WHERE id = ?",
                [gardeId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    );
};

gardes.selectByYearId = (sql, yearId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT * FROM gardes WHERE year_id = ?',
                [yearId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};

gardes.exchange = (sql, giverId, givenGardeId, receiverId, wantedGardeId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE gardes SET user_id = ?, traded = 1 WHERE id = ?;' +
                'UPDATE gardes SET user_id = ?, traded = 1 WHERE id = ?;',
                [giverId, wantedGardeId, receiverId, givenGardeId],
                (err) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        }
    )
}

module.exports = gardes;
