const {StandardErrors, Table, SQL} = require('../utils');

const terrainAttributions = new Table('terrain_attributions');
terrainAttributions.selectByYearId = (sql, yearId, promotionIds) => {
    return new Promise(
        (resolve, reject) => {
            sql.query(
                'SELECT  ' +
                '    u.promotionId AS promotionId, ' +
                '    prom.name AS promotionName,' +
                '    u.firstName AS firstName,' +
                '    u.lastName AS lastName,' +
                '    u.studentNumber AS studentNumber,' +
                '    t.terrainTypeId AS terrainTypeId,' +
                '    tt.name AS terrainName,' +
                '    p.beginDate AS beginDate,' +
                '    p.endDate AS endDate,' +
                '    tt.respFirstName AS respFirstName,' +
                '    tt.respLastName AS respLastName,' +
                '    tt.respMail AS respMail ' +
                'FROM  terrain_attributions ta  ' +
                'INNER JOIN users u  ON ta.userId = u.id  ' +
                'INNER JOIN promotions prom ON u.promotionId = prom.id ' +
                'INNER JOIN terrains t ON ta.terrainId = t.id  ' +
                'INNER JOIN periods p ON t.periodId = p.id ' +
                'INNER JOIN terrain_types tt ON t.terrainTypeId = tt.id ' +
                'WHERE t.yearId = ' + (yearId === 'active ' ? '(SELECT y.id FROM years y WHERE y.active = 1) ' : (yearId + ' ')) +
                (promotionIds ? ' AND u.promotionId IN (?)' : '') +
                'ORDER BY u.promotionId ASC, lastName ASC, firstName ASC, beginDate ASC',
                (yearId, promotionIds),
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};

terrainAttributions.selectAllByPromotionAndYear = (sql, promotionId, yearId, categoryId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query(
                'SELECT  ' +
                '    ta.id AS id,' +
                '    t.id AS terrainId,' +
                '    t.terrainTypeId AS terrainTypeId,' +
                '    t.periodId AS periodId,' +
                '    u.id AS userId,  ' +
                '    u.firstName AS firstName,  ' +
                '    u.lastName AS lastName  ' +
                'FROM  terrain_attributions ta  ' +
                'INNER JOIN users u  ON  ta.userId = u.id  ' +
                'INNER JOIN  terrains t ON ta.terrainId = t.id  ' +
                (categoryId ?
                        'INNER JOIN category_attributions ca ON ca.terrainTypeId = t.terrainTypeId ' +
                        'WHERE ca.categoryId = ? ' +
                        'AND '
                        :
                        ' WHERE '
                ) +
                ' t.promotionId = ? AND t.yearId = ' + (yearId === 'active' ? '(SELECT y.id FROM years y WHERE y.active = 1)' : yearId),
                (categoryId ? [categoryId, promotionId] : [promotionId]),
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};
terrainAttributions.selectAllByTerrainId = (sql, terrainIds) => {
    return new Promise((resolve, reject) => {
        sql.query('SELECT ta.*, u.firstName, u.lastName FROM terrain_attributions ta INNER JOIN users u ON ta.userId = u.id WHERE ta.terrainId IN (?)',
            [terrainIds],
            (error, result) => {
                if (error) {
                    console.error(error);
                    reject(StandardErrors.SQL_ERROR);
                } else {
                    resolve(result);
                }
            }
        );
    });
};
terrainAttributions.selectByUserId = (sql, userId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query(
                "SELECT ta.id AS attribId, t.terrainTypeId AS terrainTypeId, t.yearId AS yearId, t.periodId AS periodId, ta.state AS state, Y.name AS yearName, prom.id AS promotionId, prom.name AS promotionName, tt.name AS terrainTypeName, p.`order` AS periodOrder, p.beginDate AS beginDate, p.endDate AS endDate FROM terrain_attributions ta INNER JOIN terrains t ON ta.terrainId = t.id INNER JOIN terrain_types tt ON t.terrainTypeId = tt.id INNER JOIN years Y ON t.yearId = Y.id INNER JOIN periods p ON t.periodId = p.id INNER JOIN promotions prom ON t.promotionId = prom.id WHERE userId = ?",
                [userId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                })
        }
    );
};
terrainAttributions.selectAllByTerrainTypesAndPromotionAndGroup = (sql, availableTerrainTypes, promotionId, groupId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ta.*, u.firstName, u.lastName FROM terrain_attributions ta INNER JOIN users u ON ta.userId = u.id WHERE ta.terrainId IN (SELECT t.id FROM terrains t WHERE t.terrainTypeId IN (?) AND t.promotionId = ? AND t.periodId IN (SELECT p.id FROM periods p WHERE p.`group` IN (?, 4) AND p.yearId = (SELECT y.id FROM years y WHERE y.active = 1)))',
                [availableTerrainTypes, promotionId, groupId],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            );
        }
    );
};

terrainAttributions.isAlreadyValidAtThisPeriod = (sql, userId, terrainId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT terrain_attributions.id FROM terrain_attributions ' +
                'WHERE terrain_attributions.userId = ? ' +
                'AND terrain_attributions.state = 2 ' +
                'AND terrain_attributions.terrainId IN ' +
                '(SELECT terrains.id FROM `terrains` WHERE terrains.periodId = ' +
                '(SELECT terrains.periodId FROM terrains WHERE terrains.id = ?));', [userId, terrainId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result.length > 0) {
                            resolve(true);
                        } else {
                            resolve(false);
                        }
                    }
                })
        }
    );
};

terrainAttributions.insert = (sql, values, mustDeleteExistingAttributionsOnSamePeriod) => {
    return new Promise(
        (resolve, reject) => {

            const delQuery = 'DELETE FROM terrain_attributions WHERE terrain_attributions.userId = ? AND terrain_attributions.terrainId IN (SELECT t.id FROM terrains t WHERE t.periodId IN (SELECT t2.periodId FROM terrains t2 WHERE t2.id = ?));';
            const insertQuery = 'INSERT INTO terrain_attributions ' + SQL.getInsertQuery(values);

            sql.query((mustDeleteExistingAttributionsOnSamePeriod ? delQuery : '') + insertQuery, [values.userId, values.terrainId],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        const insertId = (Array.isArray(result) ? result[result.length - 1].insertId : result.insertId);
                        resolve(insertId);
                    }
                });
        }
    );
};
terrainAttributions.updateMultipleStates = (sql, terrainId, state) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE terrain_attributions SET state = ? WHERE terrainId = ?;', [state, terrainId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        }
    );
};
terrainAttributions.deleteMultiplesAttributions = (sql, terrainId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('DELETE FROM terrain_attributions WHERE terrainId = ? AND state != 2;',
                [terrainId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    );
};

terrainAttributions.STATE = {
    WAITING: 0,
    REFUSED: 1,
    ACCEPTED: 2,
    NOT_PRIORITY: 3,
};

module.exports = terrainAttributions;
