const {StandardErrors, Table, SQL} = require('../utils');

const resetPasswordTokens = new Table('reset_password_tokens',
    {
        'id': 'id',
        'token': 'token',
        'userId': 'user_id'
    },
    {
        'id': 'id',
        'token': 'token',
        'user_id': 'userId'
    });

resetPasswordTokens.insert = (sql, values) => {
    return new Promise(
        (resolve, reject) => {
            values = resetPasswordTokens.transformObjectJsToSql(values);
            sql.query('INSERT INTO reset_password_tokens ' + SQL.getInsertQuery(values) + ' ON DUPLICATE KEY UPDATE token = ? ',
                [values.token],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        });
};
resetPasswordTokens.selectByToken = (sql, fields, token) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM reset_password_tokens WHERE `token` = ? LIMIT 1',
                [fields, token],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(resetPasswordTokens.transformObjectSqlToJs(result[0]))
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    );
};

resetPasswordTokens.deleteByToken = (sql, token) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('DELETE FROM reset_password_tokens WHERE `token` = ?',
                [token],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                }
            );
        }
    );
};

module.exports = resetPasswordTokens;
