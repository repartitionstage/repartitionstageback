const {StandardErrors, Table} = require('../utils');

const years = new Table('years',
    {
        'id': 'id',
        'name': 'name',
        'active': 'active'
    },
    {
        'id': 'id',
        'name': 'name',
        'active': 'active'
    }
);

years.selectByActive = (sql, fields) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM `years` WHERE `active` = 1', [fields],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && Array.isArray(result) && result.length > 0) {
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );
        }
    )
};

years.inactiveCurrentYear = (sql) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE `years` SET `active` = 0 WHERE `active` = 1;', [],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                })
        }
    )
}
years.updateActiveYearById = (sql, yearId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('UPDATE `years` SET `active`= 0 WHERE `active` = 1; UPDATE `years` SET `active`= 1 WHERE `id` = ?;', [yearId],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        }
    );
};

module.exports = years;
