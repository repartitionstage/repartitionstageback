const {StandardErrors, Table} = require('../utils');

const categoryAttributions = new Table('category_attributions',
    {
    'id': 'id',
    'categoryId': 'category_id',
    'terrainTypeId': 'terrain_type_id'
    },
    {
        'id': 'id',
        'category_id': 'categoryId',
        'terrain_type_id': 'terrainTypeId'
    },
    );
categoryAttributions.insert = (sql, values) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('INSERT INTO category_attributions (`categoryId`, `terrainTypeId`) VALUES ? ON DUPLICATE KEY UPDATE `id` = id', [values],
                (error) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve();
                    }
                });
        });
};

module.exports = categoryAttributions;
