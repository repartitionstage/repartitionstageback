const {Table} = require('../utils');

const categories = new Table('categories',
    {
        'id': 'id',
        'name': 'name',
        'maxChoices': 'max_choices',
        'maxAttributions': 'max_attributions',
        'maxAttributionsPerYear': 'max_attributions_per_year'
    },
    {
        'id': 'id',
        'name': 'name',
        'max_choices': 'maxChoices',
        'max_attributions': 'maxAttributions',
        'max_attributions_per_year': 'maxAttributionsPerYear'
    });

module.exports = categories;
