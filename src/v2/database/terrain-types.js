const {StandardErrors, Table, SQL} = require('../utils');

const categoryAttributions = require('./category-attributions',);

const terrainTypes = new Table('terrain_types',
    {
        'id': 'id',
        'name': 'name',
        'location': 'location',
        'desc': 'desc',
        'respFirstName': 'resp_first_name',
        'respLastName': 'resp_last_name',
        'respMail': 'resp_mail',
        'maxChoices': 'max_choices',
        'maxAttributions': 'max_attributions',
        'maxAttributionsPerYear': 'max_attributions_per_year'
    },
    {
        'id': 'id',
        'name': 'name',
        'location': 'location',
        'desc': 'desc',
        'resp_first_name': 'respFirstName',
        'resp_last_name': 'respLastName',
        'resp_mail': 'respMail',
        'max_choices': 'maxChoices',
        'max_attributions': 'maxAttributions',
        'max_attributions_per_year': 'maxAttributionsPerYear'
    });

terrainTypes.insert = (sql, values) => {
    return new Promise(
        (resolve, reject) => {
            const categoriesIds = values.categoriesIds;
            delete values.categoriesIds;

            sql.query('INSERT INTO terrain_types ' + SQL.getInsertQuery(values), [],
                (error, results) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        const categoryAttribArray = [];
                        for (let categoryAttrib of categoriesIds) {
                            categoryAttribArray.push([categoryAttrib, results.insertId]);
                        }
                        categoryAttributions.insert(sql, categoryAttribArray)
                            .then(() => resolve())
                            .catch((error) => reject(error));
                    }
                }
            );

        });
};

terrainTypes.selectById = (sql, id, fields) => {
    return new Promise(
        (resolve, reject) => {
            const selectCategoriesIds = fields.includes('categoriesIds');

            const query = selectCategoriesIds ?
                'SELECT ??, (SELECT CONCAT(\'[\', GROUP_CONCAT(ca.categoryId), \']\') FROM category_attributions ca WHERE ca.terrainTypeId = terrain_types.id GROUP BY ca.terrainTypeId) AS categoriesIds FROM `terrain_types` WHERE id = ?' :
                'SELECT ?? FROM terrain_types WHERE `id` = ?';

            sql.query(query, [fields, id],
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (result && result.length > 0) {
                            if (selectCategoriesIds)
                                result[0].categoriesIds = JSON.parse(result[0].categoriesIds);
                            resolve(result[0]);
                        } else {
                            reject(StandardErrors.OBJECT_NOT_FOUND);
                        }
                    }
                }
            );

        }
    );
};
terrainTypes.selectAll = (sql, fields, limit = 0, offset = 0) => {
    return new Promise(
        (resolve, reject) => {
            let limitOffset = '';
            if (limit !== 0) {
                limitOffset = 'LIMIT ' + limit;
                if (offset !== 0) {
                    limitOffset += ' OFFSET ' + offset;
                }
            }

            if (!fields.includes['id'])
                fields.unshift('id');

            const selectCategoriesIds = fields.includes('categoriesIds');
            if (selectCategoriesIds)
                fields.splice(fields.indexOf('categoriesIds'), 1);

            const query = selectCategoriesIds ?
                'SELECT ??, (SELECT CONCAT(\'[\', GROUP_CONCAT(ca.categoryId), \']\') FROM category_attributions ca WHERE ca.terrainTypeId = terrain_types.id GROUP BY ca.terrainTypeId) AS categoriesIds FROM `terrain_types`' :
                'SELECT ?? FROM terrain_types';

            sql.query(query + limitOffset, [fields],
                (error, terrainTypes) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        if (selectCategoriesIds)
                            terrainTypes.forEach(
                                (tt) => {
                                    if (tt.categoriesIds)
                                        tt.categoriesIds = JSON.parse(tt.categoriesIds);
                                }
                            );
                        resolve(terrainTypes);
                    }
                }
            );
        }
    );
};
terrainTypes.updateById = (sql, id, object) => {
    return new Promise(
        (resolve, reject) => {
            delete object.id;
            const categoriesIds = object.categoriesIds;
            delete object.categoriesIds;

            sql.query('UPDATE terrain_types SET ? WHERE `id` = ?', [object, id], (error) => {
                if (error) {
                    console.error(error);
                    reject(StandardErrors.SQL_ERROR);
                } else {
                    if (categoriesIds && Array.isArray(categoriesIds) && categoriesIds.length > 0) {
                        const formatCategoriesIds = [];

                        for (let categoryId of categoriesIds) {
                            formatCategoriesIds.push([categoryId, id]);
                        }

                        sql.query('DELETE FROM `category_attributions` WHERE `terrainTypeId` = ?; INSERT INTO `category_attributions` (`categoryId`, `terrainTypeId`) VALUES ?;', [id, formatCategoriesIds], (error) => {
                            if (error) {
                                console.error(error);
                                reject(StandardErrors.SQL_ERROR);
                            } else {
                                resolve();
                            }
                        });
                    } else {
                        resolve();
                    }
                }
            });

        }
    );
};

//No need to override terrainTypes.deleteById because categories_attributions will be delete on cascade

terrainTypes.selectOpenTerrainTypesByYearAndPromotion = (sql, fields, yearId, promotionId, categoryId) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM terrain_types tt '
                + (categoryId ? ' INNER JOIN category_attributions ca on tt.id = ca.terrainTypeId WHERE ca.categoryId = ? AND ' : ' WHERE ')
                + ' tt.id IN (SELECT t.terrainTypeId FROM terrains t WHERE t.promotionId = ? AND t.yearId = ?)',
                (categoryId ? [fields, categoryId, promotionId, yearId] : [fields, promotionId, yearId]),
                (error, result) => {
                    if (error) {
                        console.error(error);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                });
        }
    )
};

module.exports = terrainTypes;
