const {StandardErrors, Table} = require('../utils');

const locations = new Table('locations',
    {
        'id': 'id',
        'name': 'name',
        'distance': 'distance'
    },
    {
        'id': 'id',
        'name': 'name',
        'distance': 'distance'
    });

locations.selectAllByTerrainType = (sql, fields, availableTerrainTypes) => {
    return new Promise(
        (resolve, reject) => {
            sql.query('SELECT ?? FROM locations l WHERE l.id IN (SELECT tt.locationId FROM terrain_types tt WHERE tt.id IN (?))',
                [fields, availableTerrainTypes],
                (err, result) => {
                    if (err) {
                        console.error(err);
                        reject(StandardErrors.SQL_ERROR);
                    } else {
                        resolve(result);
                    }
                }
            )
        }
    )
};

module.exports = locations;
