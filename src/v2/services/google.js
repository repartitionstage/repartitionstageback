const fs = require('fs');
const readline = require('readline');
const {google} = require('googleapis');

const SCOPES = [
    'https://www.googleapis.com/auth/gmail.send',
    'https://www.googleapis.com/auth/gmail.readonly',
    'https://www.googleapis.com/auth/admin.directory.group',
];
const CREDENTIALS_PATH = '.google_credentials.json';
const TOKEN_PATH = '.google_token.json';

let credentials, tokens, oAuth2Client;

function loadCredentials() {
    return new Promise((resolve, reject) => {
        fs.readFile(CREDENTIALS_PATH, (err, jsonCredentials) => {
            if (err) {
                console.log('Error loading credentials secret file: ' + err + '. Google API will not be enable and will throw error.');
                reject();
            } else {
                credentials = JSON.parse(jsonCredentials);

                const {client_secret, client_id, redirect_uris} = credentials.installed;
                oAuth2Client = new google.auth.OAuth2(client_id, client_secret, redirect_uris[0]);

                fs.readFile(TOKEN_PATH, (err, jsonTokens) => {
                    if (err) {
                        const authUrl = oAuth2Client.generateAuthUrl({
                            access_type: 'offline',
                            scope: SCOPES,
                        });
                        console.log('Authorize this app by visiting this url:', authUrl);
                        const rl = readline.createInterface({
                            input: process.stdin,
                            output: process.stdout,
                        });
                        rl.question('Enter the code from that page here: ', (code) => {
                            rl.close();
                            oAuth2Client.getToken(code, (err, token) => {
                                if (err) {
                                    console.error('Error retrieving access token', err);
                                    reject();
                                } else {
                                    oAuth2Client.setCredentials(token);

                                    fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                                        if (err) {
                                            console.error(err);
                                            reject();
                                        } else {
                                            console.log('Token successfully stored.');
                                            resolve();
                                        }
                                    });
                                }
                            });
                        });
                    } else {
                        tokens = JSON.parse(jsonTokens);
                        oAuth2Client.setCredentials(tokens);
                        resolve();
                    }
                });
            }

        });
    });
}

/**
 * Create a Base64 string representing the mail.
 * @param from{string} email of the sender
 * @param to{string} email of the receiver
 * @param subject{string} subject of the mail
 * @param message{string} content of the mail
 * @returns {string} Base64 string which contains the mail
 */
function createMail(from, to, subject, message) {
    const str = ["Content-Type: text/html; charset=\"UTF-8\"\n",
        "MIME-Version: 1.0\n",
        "Content-Transfer-Encoding: 7bit\n",
        "to: ", to, "\n",
        "from: ", from, "\n",
        "subject: ", "=?utf-8?B?" + Buffer.from(subject).toString("base64") + "?=", "\n\n",
        message
    ].join('');

    return Buffer.from(str).toString("base64").replace(/\+/g, '-').replace(/\//g, '_');
}

/**
 * Send mail
 * @param to{string} email of the receiver
 * @param subject{string} of the mail
 * @param message{string} html mail content
 * @returns {Promise}
 */
function sendMail(to, subject, message) {
    return new Promise(
        (resolve, reject) => {
            const gmail = google.gmail({version: 'v1', oAuth2Client});
            const raw = createMail('ARSA <noreply@ars-amiens.fr>', to, subject, message);
            gmail.users.messages.send({
                auth: oAuth2Client,
                userId: 'me',
                resource: {
                    raw: raw
                }
            }, function (err, response) {
                if (err) {
                    console.error(err);
                    reject(err);
                } else {
                    resolve(response);
                }
            });
        }
    );
}

function createGroup(email, name, description) {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.groups.insert({
                    requestBody: {name, email, description}
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve();
                    }
                });
        }
    );
}

function deleteGroup(name) {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.groups.delete({
                    groupKey: name
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve();
                    }
                });
        }
    );
}

function listGroups() {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.groups.list({
                    customer: 'my_customer'
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve(res.data.groups);
                    }
                });
        }
    );
}

function listMembers(groupEmail) {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.members.list({
                    groupKey: groupEmail
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve(res.members);
                    }
                });
        }
    )
}

function hasMember(groupEmail, memberEmail) {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.members.hasMember({
                    groupKey: groupEmail,
                    memberKey: memberEmail
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve();
                    }
                });
        }
    )
}

function insertMember(groupEmail, memberEmail) {
    return new Promise(
        (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.members.insert({
                    groupKey: groupEmail,
                    requestBody: {email: memberEmail}
                },
                (err) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve();
                    }
                });
        }
    )
}

function removeMember(groupEmail, memberEmail) {
    return new Promise(
        async (resolve, reject) => {
            const service = google.admin({version: 'directory_v1', auth: oAuth2Client});

            service.groups.delete({
                    groupKey: groupEmail,
                    memberKey: memberEmail
                },
                (err, res) => {
                    if (err) {
                        console.error(err)
                        reject();
                    } else {
                        resolve();
                    }
                });
        }
    )
}

module.exports = {
    loadCredentials,
    sendMail,
    listGroups,
    createGroup,
    deleteGroup,
    listMembers,
    hasMember,
    insertMember,
    removeMember
};
