const fs = require('fs');
const jwt = require('jsonwebtoken');
const config = require('../../config.js')
const privateKey = fs.readFileSync(config.token.privatekey, 'utf8');
const publicKey = fs.readFileSync(config.token.publickey, 'utf8');
const algorithm = 'RS256';
const Types = {
    ACCESS: {
        subject: 'access',
        expiresIn: 24 * 60 * 60, // todo 10 * 60,
    },
    REFRESH: {
        subject: 'refresh',
        expiresIn: 7 * 24 * 60 * 60
    },
    RESET_PASSWORD: {
        subject: 'reset_password',
        expiresIn: 2 * 24 * 60 * 60
    }
};

const getNewAccessToken = (payload) => {
    return getNewToken(payload, Types.ACCESS);
};
const getNewRefreshToken = (payload) => {
    return getNewToken(payload, Types.REFRESH);
};
const getNewResetPasswordToken = (payload) => {
    return getNewToken(payload, Types.RESET_PASSWORD)
}
const getNewToken = (payload, type) => {
    return new Promise(
        (resolve, reject) => {
            const signOptions = {
                subject: type.subject,
                expiresIn: type.expiresIn,
                algorithm,
            };

            jwt.sign(payload, privateKey, signOptions, (err, token) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(token);
                }
            });
        }
    )
};

const isTokenLegit = (token, type) => {
    const options = {
        algorithm,
        complete: true
    }

    if (type) {
       options.subject = type.subject;
    }

    return new Promise(
        (resolve, reject) => {
            jwt.verify(token, publicKey, options,
                (err, decoded) => {
                    if (err) {
                        //console.error(err);
                        reject();
                    } else {
                        resolve(decoded);
                    }
                });
        }
    );
};

const decodeToken = (token) => {
    return jwt.decode(token, {complete: true});
};

module.exports = {
    getNewAccessToken,
    getNewRefreshToken,
    getNewResetPasswordToken,
    getNewToken,
    isTokenLegit,
    decodeToken,
    Types
};
