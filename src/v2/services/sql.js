const config = require('../../config');
const mysql = require('mysql');

class Pool {

    pool;

    constructor(props) {
        this.pool = mysql.createPool({
            connectionLimit: props.poolsize,
            host: props.host,
            port: props.port,
            user: props.user,
            password: props.pass,
            database: props.database,
            multipleStatements: true,
            typeCast: true
        });

        this.getConnection = this.getConnection.bind(this);
        this.close = this.close.bind(this);
    }

    getConnection(callback) {
        this.pool.getConnection(callback);
    }

    close() {
        this.pool.end(() => {});
    }
}

const escape = (value) => {
    return mysql.escape(value);
};

module.exports = {
    pool: new Pool(config.sql),
    escape,
};

