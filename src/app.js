require('dotenv').config();
const config = require('./config');

const port = config.general.port || 8001;

const http = require('http'),
    express = require('express'),
    app = express(),
    cors = require('cors'),
    helmet = require('helmet'),
    morgan = require('morgan'),
    bodyParser = require('body-parser');

const {SqlService} = require('./v2/services');

app.enable('trust proxy');
app.use(helmet());
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));

const corsOptions = {
    origin: (
        config.general.prod ?
            ['https://tirajosaure.ars-amiens.fr']
            : ['https://dev.tirajosaure.ars-amiens.fr', 'http://localhost:8080']
    ),
    methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    exposedHeaders: ['Content-Disposition'],
    credentials: true,
    preflightContinue: false,
};

app.use(cors(corsOptions));

const v2 = require('./v2')(express.Router());
app.use('/v2', v2);

http.createServer(app).listen(port, () => console.log("Express server listening on port " + port + ". " + (config.general.prod === true ? 'Production' : 'Developpement') + " version"));


const {GoogleService} = require('./v2/services');
GoogleService.loadCredentials()
    .then(
        () => {
            console.log("GoogleAPI connection successfully established.");
        }
    )
    .catch(
        (error) => console.error(error)
    );

process.on('exit', function () {
    if (SqlService) {
        SqlService.pool.close();
    }
});
