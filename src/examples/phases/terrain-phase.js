const config = {
    type: 'terrain',
    availableTerrainTypes: [0, 1, 2, 3]
};

export {config as default};
