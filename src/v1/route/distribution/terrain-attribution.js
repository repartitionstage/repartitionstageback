const db = require('../../../v2/database');
const RESTRoute = require('../restroute');
const Utils = require('../utils');
const Log = require('../../service/log-service');

const routes = new RESTRoute(db.terrainAttributions);

routes.createFormat = {
    userId: {
        type: 'number'
    },
    terrainId: {
        type: 'number'
    }
};

routes.updateFormat = {
    userId: {
        type: 'number',
        optional: true
    },
    terrainId: {
        type: 'number',
        optional: true
    },
    state: {
        type: 'number',
        optional: true
    }
};

routes.defaultSelectFields = ['id', 'userId', 'terrainId', 'state', 'userInfo'];

routes.create = async (req, res, next) => {
    const connection = {req, res, next};
    const object = JSON.parse(req.body.object);

    if (Utils.validateData(object, routes.createFormat)) {

        let constraints = {
            terrainConstraints: {
                terrainId: 0,
                periodId: 0, //MAX CHOICES PER PERIOD CHECKED
                terrainTypeId: 0,
                maxAmountPlaces: -1, //CHECKED
                maxAttributionPerTerrainType: -1, //CHECKED
                maxAttributionsPerYearPerTerrainType: -1 // CHECKED
            },
            categoriesConstraints: [
                {
                    categoryId: 0,
                    maxAttributionsPerCategory: -1, //CHECKED
                    maxAttributionsPerYearPerCategory: -1, //CHECKED
                    maxChoicesPerCategory: -1, //CHECKED
                    terrainTypeIds: [0, 1, 2]
                }
            ]
        };
        constraints = await db.terrains.selectConstraintsById(req.sql.connection, object.terrainId);

        // I - CHECK IF USER IS AUTHORIZED TO ASK FOR AN ATTRIBUTION ON THIS TERRAIN
        //  1 - CHECK IF USER ALREADY HAS A VALID ATTRIBUTION ON THIS PERIOD
        const isAlreadyValidAtThiPeriod = await db.terrainAttributions.isAlreadyValidAtThisPeriod(req.sql.connection, object.userId, object.terrainId);
        if (isAlreadyValidAtThiPeriod) {
            Utils.sendError(connection, Utils.ALREADY_ATTRIB);
            return;
        }

        // II - CALCULATE IF USER IS NOT PRIORITY
        const currentYearId = Number((await db.years.selectByActive(req.sql.connection, ['id'])).id);
        const userAttributions = await db.terrainAttributions.selectByUserId(req.sql.connection, object.userId);
        const countAttribs = (terrainTypeIds, states, yearIds, periodIds) => {
            let i = 0;

            terrainTypeIds = terrainTypeIds ? (Array.isArray(terrainTypeIds) ? terrainTypeIds : [terrainTypeIds]) : [];
            states = states ? (Array.isArray(states) ? states : [states]) : [];
            yearIds = yearIds ? (Array.isArray(yearIds) ? yearIds : [yearIds]) : [];
            periodIds = periodIds ? (Array.isArray(periodIds) ? periodIds : [periodIds]) : [];

            for (const attrib of userAttributions) { // {terrainTypeId, yearId, state, periodId}
                let incr = true;
                if (terrainTypeIds && terrainTypeIds.length > 0 && incr === true)
                    incr = (terrainTypeIds.indexOf(attrib.terrainTypeId) !== -1);

                if (states && states.length > 0 && incr === true)
                    incr = (states.indexOf(attrib.state) !== -1);

                if (yearIds && yearIds.length > 0 && incr === true)
                    incr = (yearIds.indexOf(attrib.yearId) !== -1);

                if (periodIds && periodIds.length > 0 && incr === true)
                    incr = (periodIds.indexOf(attrib.periodId) !== -1);

                if (incr)
                    i++;

            }
            return i;
        };
        object.state = db.terrainAttributions.STATE.WAITING;

        //  1 - CHECK ON CURRENT YEAR AND PREVIOUS YEAR IF USER ALREADY HAS MADE THIS STAGE
        if (constraints.terrainConstraints.maxAttributionPerTerrainType !== -1
            && countAttribs(constraints.terrainConstraints.terrainTypeId, null, [currentYearId, currentYearId - 1]) >= constraints.terrainConstraints.maxAttributionPerTerrainType) {
            object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
        }

        //  2 - CHECK ON CURRENT YEAR IF USER ALREADY HAS MADE THIS STAGE
        if (object.state !== db.terrainAttributions.STATE.NOT_PRIORITY
            && constraints.terrainConstraints.maxAttributionsPerYearPerTerrainType !== -1
            && countAttribs(constraints.terrainConstraints.terrainTypeId, null, currentYearId) >= constraints.terrainConstraints.maxAttributionsPerYearPerTerrainType) {
            object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
        }

        //  3 - CHECK CATEGORIES
        if (object.state !== db.terrainAttributions.STATE.NOT_PRIORITY) {
            for (const category of constraints.categoriesConstraints) {
                if (category.maxAttributionsPerCategory !== -1
                    && countAttribs(category.terrainTypeIds, null, [currentYearId, currentYearId - 1]) >= category.maxAttributionsPerCategory) {
                    object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
                    break;
                }

                if (category.maxAttributionsPerYearPerCategory !== -1
                    && countAttribs(category.terrainTypeIds, null, currentYearId) >= category.maxAttributionsPerYearPerCategory) {
                    object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
                    break;
                }
            }
        }

        db.terrainAttributions.insert(req.sql.connection, object)
            .then(
                async (insertId) => {
                    object.id = insertId;
                    await Log.append(req.sql.connection, req.payload.userId, object.userId, req.ip, Log.TYPES.ADD_ADMIN_ATTRIBUTION, object);
                    Utils.sendSuccessWithNoResponse(connection);
                }
            )
            .catch(Utils.sendDefaultError(connection));

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
}; // LOGGED

routes.update = async (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;
    const phaseId = req.params.phaseId;
    const object = JSON.parse(req.body.object);

    if (Utils.validateData(object, routes.updateFormat)
        && Utils.isPositiveInt(id)) {
        delete object.id;

        db.terrainAttributions.updateById(connection.req.sql.connection, id, object)
            .then(
                async () => {
                    const config = JSON.parse((await db.phases.selectById(req.sql.connection, phaseId, ['config'])).config);
                    const attrib = (await db.terrainAttributions.selectById(req.sql.connection, id, ['id', 'terrainId', 'userId', 'state']));

                    await Log.append(req.sql.connection, req.payload.userId, attrib.userId, req.ip, Log.TYPES.UPDATE_ATTRIBUTION_STATE, attrib);
                    if (config.type === 'spreading') {
                        const group = (object.state === db.terrainAttributions.STATE.ACCEPTED) ? (await db.periods.selectGroupByAttribution(req.sql.connection, id)) : db.groups.GROUP.NO_ONE;

                        db.users.updateById(req.sql.connection, attrib.userId,
                            {
                                group: group,
                                groupBlocked: (object.state === db.terrainAttributions.STATE.ACCEPTED)
                            }
                        )
                            .then(
                                async () => {
                                    await Log.append(req.sql.connection, req.payload.userId, attrib.userId, req.ip, Log.TYPES.CHANGE_ADMIN_GROUP, {toGroup: group});
                                    Utils.sendSuccessWithNoResponse(connection);
                                })
                            .catch(Utils.sendDefaultError(connection));
                    } else {
                        Utils.sendSuccessWithNoResponse(connection);
                    }
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
}; //LOGGED

routes.delete = async (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    if (Utils.isPositiveInt(id)) {
        const attrib = (await db.terrainAttributions.selectById(req.sql.connection, id, ['id', 'terrainId', 'userId', 'state']));
        const userId = attrib.userId;

        const phaseId = req.params['phaseId'];
        if (Utils.isPositiveInt(phaseId)) {
            const config = JSON.parse((await db.phases.selectById(req.sql.connection, phaseId, ['config'])).config);
            if (config.type === 'spreading') {
                db.terrainAttributions.deleteById(connection.req.sql.connection, id)
                    .then(
                        async () => {
                            await Log.append(req.sql.connection, req.payload.userId, userId, req.ip, Log.TYPES.REMOVE_ADMIN_ATTRIBUTION, attrib);

                            db.users.updateById(req.sql.connection, userId, {
                                group: db.groups.GROUP.NO_ONE,
                                groupBlocked: 0
                            })
                                .then(
                                    async () => {
                                        await Log.append(req.sql.connection, req.payload.userId, userId, req.ip, Log.TYPES.CHANGE_ADMIN_GROUP, {toGroup: db.groups.GROUP.NO_ONE});
                                        Utils.sendSuccessWithNoResponse(connection);
                                    }
                                )
                                .catch(Utils.sendDefaultError(connection));
                        }
                    )
                    .catch(Utils.sendDefaultError(connection));
                return;
            }
        }

        db.terrainAttributions.deleteById(connection.req.sql.connection, id)
            .then(
                async () => {
                    await Log.append(req.sql.connection, req.payload.userId, userId, req.ip, Log.TYPES.REMOVE_ADMIN_ATTRIBUTION, attrib);
                    Utils.sendSuccessWithNoResponse(connection)
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};// LOGGED

routes.updateMultiplesAttributionsState = async (req, res, next) => {
    const connection = {req, res, next};
    const terrainId = req.params.terrainId;
    const object = JSON.parse(req.body.object);
    const state = object.state;
    const phaseId = object.phaseId;

    if (Utils.isPositiveInt(terrainId)
        && Utils.isPositiveInt(state)
        && Utils.isPositiveInt(phaseId)) {

        const config = JSON.parse((await db.phases.selectById(req.sql.connection, phaseId, ['config'])).config);
        db.terrainAttributions.updateMultipleStates(req.sql.connection, terrainId, state)
            .then(
                async () => {
                    const attribs = (await db.terrainAttributions.selectAllByTerrainId(req.sql.connection, terrainId));
                    let logs = [];

                    for (const attrib of attribs) {
                        logs.push(Log.createLogObject(req.payload.userId, attrib.userId, req.ip, Log.TYPES.UPDATE_ATTRIBUTION_STATE, attrib))
                    }
                    await Log.appendMultipleLogs(req.sql.connection, logs);

                    if (config.type === 'spreading') {
                        const group = (state === db.terrainAttributions.STATE.ACCEPTED ? (await db.periods.selectGroupOfTerrain(req.sql.connection, terrainId)) : db.groups.GROUP.NO_ONE);

                        db.users.updateGroupByTerrainId(req.sql.connection, group, (state === db.terrainAttributions.STATE.ACCEPTED), terrainId)
                            .then(
                                async () => {
                                    logs = [];
                                    for (const attrib of attribs) {
                                        logs.push(Log.createLogObject(req.payload.userId, attrib.userId, req.ip, Log.TYPES.CHANGE_ADMIN_GROUP, {toGroup: group}))
                                    }
                                    await Log.appendMultipleLogs(req.sql.connection, logs);
                                    Utils.sendSuccessWithNoResponse(connection);
                                }
                            )
                            .catch(Utils.sendDefaultError(connection));

                    } else {
                        Utils.sendSuccessWithNoResponse(connection);
                    }
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
}; //LOGGED

routes.deleteMultiplesAttributions = async (req, res, next) => {
    const connection = {req, res, next};
    const terrainId = req.params.terrainId;

    if (Utils.isPositiveInt(terrainId)) {
        const attribs = (await db.terrainAttributions.selectAllByTerrainId(req.sql.connection, terrainId));

        db.terrainAttributions.deleteMultiplesAttributions(req.sql.connection, terrainId)
            .then(
                async () => {
                    let logs = [];
                    for (const attrib of attribs) {
                        logs.push(Log.createLogObject(req.payload.userId, attrib.userId, req.ip, Log.TYPES.REMOVE_ADMIN_ATTRIBUTION, attrib))
                    }
                    Log.appendMultipleLogs(req.sql.connection, logs)
                        .then(Utils.sendDefaultSuccess(connection))
                        .catch(Utils.sendDefaultError(connection));
                }
            )
            .catch(Utils.sendDefaultError(connection));

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.readAllByPhase = async (req, res, next) => {
    const connection = {req, res, next};
    const phaseId = req.params.phaseId;
    const readByGroup = req.query.group;

    const result = {
        periods: [],
        terrainTypes: [],
        terrains: [],
        attributions: [],
        locations: []
    };

    if (Utils.isPositiveInt(phaseId)) {
        try {

            const phase = await db.phases.selectById(req.sql.connection, phaseId, ['promotionId', 'config']);
            const config = JSON.parse(phase.config);

            if (config.availableTerrainTypes && Array.isArray(config.availableTerrainTypes) && config.availableTerrainTypes.length > 0) {

                let group;
                if (readByGroup)
                    group = (await db.users.selectById(req.sql.connection, req.payload.userId, ['group'])).group;

                result.locations = await db.locations.selectAll(req.sql.connection, ['id', 'name', 'distance']);
                result.periods = await db.periods.selectAllByPromotionAndYear(req.sql.connection, ['id', 'beginDate', 'endDate', 'order', 'group'], phase.promotionId, 'active', group);
                result.terrainTypes = await db.terrainTypes.selectAllByIds(req.sql.connection, config.availableTerrainTypes, ['id', 'name', 'desc', 'locationId']);
                result.terrains = await db.terrains.selectByTerrainTypeAndPeriod(req.sql.connection, ['id', 'maxAmountPlaces', 'periodId', 'terrainTypeId'], config.availableTerrainTypes, result.periods.map(period => period.id));
                result.attributions = await db.terrainAttributions.selectAllByTerrainId(req.sql.connection, result.terrains.map(terrain => terrain.id));

                Utils.sendSuccessWithData(connection, result);

            } else {
                Utils.sendError(connection, Utils.ERROR_OCCURS);
            }

        } catch (err) {
            Utils.sendError(connection, Utils.ERROR_OCCURS);
        }

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.createOnDistribution = async (req, res, next) => {
    const connection = {req, res, next};
    const object = JSON.parse(req.body.object);

    if (Utils.validateData(object, routes.createFormat)) {
        const userId = req.payload.userId;
        if (object.userId === userId) {

            let constraints = {
                terrainConstraints: {
                    terrainId: 0,
                    periodId: 0, //MAX CHOICES PER PERIOD CHECKED
                    terrainTypeId: 0,
                    maxAmountPlaces: -1, //CHECKED
                    maxAttributionPerTerrainType: -1, //CHECKED
                    maxAttributionsPerYearPerTerrainType: -1 // CHECKED
                },
                categoriesConstraints: [
                    {
                        categoryId: 0,
                        maxAttributionsPerCategory: -1, //CHECKED
                        maxAttributionsPerYearPerCategory: -1, //CHECKED
                        maxChoicesPerCategory: -1, //CHECKED
                        terrainTypeIds: [0, 1, 2]
                    }
                ]
            };
            constraints = await db.terrains.selectConstraintsById(req.sql.connection, object.terrainId);

            // I - CHECK IF USER IS AUTHORIZED TO ASK FOR AN ATTRIBUTION ON THIS TERRAIN
            //  1 - CHECK IF ENOUGH PLACES ARE STILL OPENED
            const terrainAttributions = await db.terrainAttributions.selectAllByTerrainId(req.sql.connection, object.terrainId);
            const countStated = (array, state) => {
                let i = 0;
                for (let line of array) {
                    if (line.state === state) {
                        i++;
                    }
                }
                return i;
            };
            if (constraints.terrainConstraints.maxAmountPlaces !== -1
                && countStated(terrainAttributions, db.terrainAttributions.STATE.ACCEPTED) >= constraints.terrainConstraints.maxAmountPlaces) {
                Utils.sendError(connection, Utils.ADMIN_PRIV_REQUIRED);
                return;
            }

            //  2 - CHECK IF USER ALREADY HAS A VALID ATTRIBUTION ON THIS PERIOD
            const isAlreadyValidAtThiPeriod = await db.terrainAttributions.isAlreadyValidAtThisPeriod(req.sql.connection, object.userId, object.terrainId);
            if (isAlreadyValidAtThiPeriod) {
                Utils.sendError(connection, Utils.ALREADY_ATTRIB);
                return;
            }

            // 3 - CHECK FOR MAX CHOICES PER PHASE
            const currentYearId = Number((await db.years.selectByActive(req.sql.connection, ['id'])).id);
            const userAttributions = await db.terrainAttributions.selectByUserId(req.sql.connection, object.userId);
            const countAttribs = (terrainTypeIds, states, yearIds, periodIds) => {
                let i = 0;

                terrainTypeIds = terrainTypeIds ? (Array.isArray(terrainTypeIds) ? terrainTypeIds : [terrainTypeIds]) : [];
                states = states ? (Array.isArray(states) ? states : [states]) : [];
                yearIds = yearIds ? (Array.isArray(yearIds) ? yearIds : [yearIds]) : [];
                periodIds = periodIds ? (Array.isArray(periodIds) ? periodIds : [periodIds]) : [];

                for (const attrib of userAttributions) { // {terrainTypeId, yearId, state, periodId}
                    let incr = true;
                    if (terrainTypeIds && terrainTypeIds.length > 0 && incr === true)
                        incr = (terrainTypeIds.indexOf(attrib.terrainTypeId) !== -1);

                    if (states && states.length > 0 && incr === true)
                        incr = (states.indexOf(attrib.state) !== -1);

                    if (yearIds && yearIds.length > 0 && incr === true)
                        incr = (yearIds.indexOf(attrib.yearId) !== -1);

                    if (periodIds && periodIds.length > 0 && incr === true)
                        incr = (periodIds.indexOf(attrib.periodId) !== -1);

                    if (incr)
                        i++;

                }
                return i;
            };


            const maxChoicesPerPhase = req.phase.config['maxChoices'] ? req.phase.config['maxChoices'] : -1;
            if (maxChoicesPerPhase !== -1
                && countAttribs(req.phase.config['availableTerrainTypes'], [db.terrainAttributions.STATE.WAITING, db.terrainAttributions.STATE.NOT_PRIORITY], currentYearId) >= maxChoicesPerPhase) {
                Utils.sendError(connection, Utils.MAX_CHOICES);
                return;
            }

            // 4 - CHECK FOR MAX CHOICES PER PERIOD
            const maxChoicesPerPeriod = req.phase.config['maxChoicesPerPeriod'] ? req.phase.config['maxChoicesPerPeriod'] : -1;
            if (maxChoicesPerPeriod !== -1
                && countAttribs(null, null, null, constraints.terrainConstraints.periodId) >= maxChoicesPerPeriod) {
                Utils.sendError(connection, Utils.MAX_CHOICES_PERIOD);
                return;
            }
            const mustDeleteExistingAttributionsOnSamePeriod = (maxChoicesPerPeriod === 1);


            // 5 - CHECK FOR MAX CHOICES PER CATEGORY
            for (const category of constraints.categoriesConstraints) {
                if (category.maxChoicesPerCategory !== -1
                    && countAttribs(category.terrainTypeIds, null, currentYearId) >= category.maxChoicesPerCategory) {
                    Utils.sendError(connection, Utils.MAX_CHOICES_CATEGORY);
                    return;
                }
            }

            // II - CALCULATE IF USER IS NOT PRIORITY
            object.state = db.terrainAttributions.STATE.WAITING;

            //  1 - CHECK ON CURRENT YEAR AND PREVIOUS YEAR IF USER ALREADY HAS MADE THIS STAGE
            if (constraints.terrainConstraints.maxAttributionPerTerrainType !== -1
                && countAttribs(constraints.terrainConstraints.terrainTypeId, null, [currentYearId, currentYearId - 1]) >= constraints.terrainConstraints.maxAttributionPerTerrainType) {
                object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
            }

            //  2 - CHECK ON CURRENT YEAR IF USER ALREADY HAS MADE THIS STAGE
            if (object.state !== db.terrainAttributions.STATE.NOT_PRIORITY
                && constraints.terrainConstraints.maxAttributionsPerYearPerTerrainType !== -1
                && countAttribs(constraints.terrainConstraints.terrainTypeId, null, currentYearId) >= constraints.terrainConstraints.maxAttributionsPerYearPerTerrainType) {
                object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
            }

            //  3 - CHECK CATEGORIES
            if (object.state !== db.terrainAttributions.STATE.NOT_PRIORITY) {
                for (const category of constraints.categoriesConstraints) {
                    if (category.maxAttributionsPerCategory !== -1
                        && countAttribs(category.terrainTypeIds, null, [currentYearId, currentYearId - 1]) >= category.maxAttributionsPerCategory) {
                        object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
                        break;
                    }

                    if (category.maxAttributionsPerYearPerCategory !== -1
                        && countAttribs(category.terrainTypeIds, null, currentYearId) >= category.maxAttributionsPerYearPerCategory) {
                        object.state = db.terrainAttributions.STATE.NOT_PRIORITY;
                        break;
                    }
                }
            }

            db.terrainAttributions.insert(req.sql.connection, object, mustDeleteExistingAttributionsOnSamePeriod)
                .then(
                    async (insertId) => {
                        object.id = insertId;
                        await Log.append(req.sql.connection, userId, userId, req.ip, Log.TYPES.ADD_PERSONAL_ATTRIBUTION, object);
                        Utils.sendSuccessWithNoResponse(connection);
                    }
                )
                .catch(Utils.sendDefaultError(connection));
        } else {
            Utils.sendError(connection, Utils.ADMIN_PRIV_REQUIRED);
        }
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
}; // LOGGED

routes.deleteOnDistribution = async (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    if (Utils.isPositiveInt(id)) {
        const ta = await db.terrainAttributions.selectById(req.sql.connection, id, ['id', 'terrainId', 'userId', 'state']);
        if (ta.userId === req.payload.userId
            && (ta.state === 0
                || ta.state === 3)) {
            db.terrainAttributions.deleteById(req.sql.connection, id)
                .then(
                    async () => {
                        await Log.append(req.sql.connection, req.payload.userId, ta.userId, req.ip, Log.TYPES.REMOVE_PERSONAL_ATTRIBUTION, ta);
                        Utils.sendSuccessWithNoResponse(connection);
                    }
                )
                .catch(Utils.sendDefaultError(connection));
        } else {
            Utils.sendError(connection, Utils.ADMIN_PRIV_REQUIRED);
        }
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
}; // LOGGED

routes.selectByUser = async (req, res, next) => {
    const connection = {req, res, next};

    const userId = ((req.params['userId'] === 'self') ? req.payload.userId : req.params['userId']);

    if (Utils.isPositiveInt(userId)) {
        db.terrainAttributions.selectByUserId(req.sql.connection, userId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }


};

routes.readAdminAttribution = async (req, res, next) => {
    const connection = {req, res, next};
    const phaseId = req.query['phaseId'];
    const groupId = req.query['groupId'];
    let withAssets = req.query['withAssets'];

    if (Utils.isPositiveInt(phaseId)
        && Utils.isPositiveInt(groupId)
        && Utils.isBoolean(withAssets)) {
        withAssets = (withAssets === 'true');

        const result = {
            attributions: []
        };

        try {
            const phase = await db.phases.selectById(req.sql.connection, phaseId, ['promotionId', 'config']);
            const config = JSON.parse(phase.config);
            if (config.availableTerrainTypes && Array.isArray(config.availableTerrainTypes) && config.availableTerrainTypes.length > 0) {

                if (withAssets) {
                    result.locations = await db.locations.selectAllByTerrainType(req.sql.connection, ['id', 'name', 'distance'], config.availableTerrainTypes);
                    result.periods = await db.periods.selectAllByPromotionAndYear(req.sql.connection, ['id', 'beginDate', 'endDate', 'order', 'group'], phase.promotionId, 'active', groupId);
                    result.terrainTypes = await db.terrainTypes.selectAllByIds(req.sql.connection, config.availableTerrainTypes, ['id', 'name', 'desc', 'locationId']);
                    result.terrains = await db.terrains.selectByTerrainTypeAndPeriod(req.sql.connection, ['id', 'maxAmountPlaces', 'periodId', 'terrainTypeId'], config.availableTerrainTypes, result.periods.map(period => period.id));
                }
                result.attributions = await db.terrainAttributions.selectAllByTerrainTypesAndPromotionAndGroup(req.sql.connection, config.availableTerrainTypes, phase.promotionId, groupId);

                Utils.sendSuccessWithData(connection, result);
            } else {
                Utils.sendError(connection, Utils.ERROR_OCCURS);
            }
        } catch (err) {
            Utils.sendError(connection, Utils.ERROR_OCCURS);
        }
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};


module.exports = routes;
