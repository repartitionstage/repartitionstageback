const db = require('../../../v2/database');
const RESTRoute = require('../restroute');
const Utils = require('../utils');

const routes = new RESTRoute(db.terrainTypes);

routes.defaultSelectFields = ['id', 'name', 'locationId', 'desc', 'respFirstName', 'respLastName', 'respMail', 'maxChoices', 'maxAttributions', 'maxAttributionsPerYear', 'categoriesIds'];

routes.createFormat = {
    name: {
        type: 'string',
        minLength: 0
    },
    locationId: {
        type: 'number',
        min: 0
    },
    desc: {
        type: 'string'
    },
    respFirstName: {
        type: 'string',
        minLength: 0
    },
    respLastName: {
        type: 'string',
        minLength: 0
    },
    respMail: {
        type: 'string',
        minLength: 0
    }
};

routes.updateFormat = {
    id: {
        type: 'number'
    },
    name: {
        type: 'string',
        minLength: 0
    },
    locationId: {
        type: 'number',
        min: 0
    },
    desc: {
        type: 'string'
    },
    respFirstName: {
        type: 'string',
        minLength: 0
    },
    respLastName: {
        type: 'string',
        minLength: 0
    },
    respMail: {
        type: 'string',
        minLength: 0
    },
    maxChoices: {
        type: 'number',
        min: -1
    },
    maxAttributions: {
        type: 'number',
        min: -1
    },
    maxAttributionsPerYear: {
        type: 'number',
        min: -1
    }
};


routes.readOpenTerrainTypesByYearAndPromotion = (req, res, next) => {
    const connection = {req, res, next};

    const promotionId = req.params.promotionId;
    const yearId = req.params.yearId;

    if (Utils.isPositiveInt(promotionId) && Utils.isPositiveInt(yearId)) {
        db.terrainTypes.selectOpenTerrainTypesByYearAndPromotion(req.sql.connection, ['tt.id', 'tt.name'], yearId, promotionId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(res, Utils.INVALID_FORMAT)
    }
};

module.exports = routes;
