const RESTRoute = require('../restroute');
const Utils = require('../utils');
const db = require('../../../v2/database');

const routes = new RESTRoute(db.terrains);

routes.createFormat = {
    maxAmountPlaces: {
        type: 'number'
    },
    periodId: {
        type: 'number'
    },
    promotionId: {
        type: 'number'
    },
    terrainTypeId: {
        type: 'number'
    },
    yearId: {
        type: 'number'
    }
};

routes.updateFormat = {
    id: {
        type: 'number'
    },
    maxAmountPlaces: {
        type: 'number'
    },
    periodId: {
        type: 'number'
    },
    promotionId: {
        type: 'number'
    },
    terrainTypeId: {
        type: 'number'
    },
    yearId: {
        type: 'number'
    }
};

routes.defaultSelectFields = ['id', 'maxAmountPlaces', 'periodId', 'promotionId', 'terrainTypeId', 'yearId'];

routes.updateByYearAndPromotion = (req, res, next) => {
    const connection = {req, res, next};

    const promotionId = req.params.promotionId;
    const yearId = req.params.yearId;

    if (Utils.isPositiveInt(promotionId) && Utils.isPositiveInt(yearId)) {
        let data = req.body;

        if (Utils.isJsonString(data)) {
            data = JSON.parse(data);

            db.terrains.updateByYearAndPromotion(req.sql.connection, yearId, promotionId, data)
                .then(Utils.sendDefaultSuccess(connection))
                .catch(Utils.sendDefaultError(connection));

        } else {
            Utils.sendError(connection, Utils.INVALID_FORMAT)
        }
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.readByYearAndPromotion = (req, res, next) => {
    const connection = {req, res, next};

    const promotionId = req.params.promotionId;
    const yearId = req.params.yearId;

    if (Utils.isPositiveInt(promotionId) && Utils.isPositiveInt(yearId)) {
        db.terrains.selectByYearAndPromotion(req.sql.connection, ['id', 'maxAmountPlaces', 'periodId', 'promotionId', 'terrainTypeId', 'yearId'], yearId, promotionId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};


module.exports = routes;
