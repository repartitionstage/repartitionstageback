const db = require('../../../v2/database');
const Utils = require('../utils');
const xl = require('excel4node');
const sqlReleaser = require('../../middleware/sql-releaser');
const fs = require('fs');

class Cell {

    constructor() {
        this.referenceId = -1; //userId or terrainTypeId for leftCell
        this.content = '';
        this.data = {};
    }

}

class Table {
    constructor(title) {
        this.topRow = {
            cells: []
        };
        const firstCell = new Cell();
        firstCell.content = title;
        this.topRow.cells.push(firstCell); // add first blank cell;

        /**
         *   {
         *      referenceId: number, //terrainTypeId
         *      leftCell: new Cell(),
         *      contentCells: {
         *          referenceId: number, //terrainId
         *          entries: { //attrib
         *              referenceId: number, //attribId
         *              content: string, //userName
         *              data: string,
         *          }[]
         *      }[]
         *   }[]
         * @type {Array}
         */
        this.contentRows = [];

        this.getTopCellIndexByReferenceId = (referenceId) => {
            for (let i = 0; i < this.topRow.cells.length; i++) {
                if (this.topRow.cells[i].referenceId === referenceId) {
                    return i;
                }
            }
            return null;
        };
        this.getRowIndexByReferenceId = (referenceId) => {
            for (let i = 0; i < this.contentRows.length; i++) {
                if (this.contentRows[i].referenceId === referenceId) {
                    return i;
                }
            }
            return null;
        };
        this.getContentCellByReferenceId = (rowReferenceId, topReferenceId) => {
            const row = this.contentRows[this.getRowIndexByReferenceId(rowReferenceId)];
            return row.contentCells[this.getTopCellIndexByReferenceId(topReferenceId) - 1];
        }
    }

}

const fromTableToXL = (table) => {
    const wb = new xl.Workbook();

    const ws = wb.addWorksheet('Sheet 1', {});

    const headerStyle = wb.createStyle({
        alignment: {
            horizontal: 'center',
            vertical: 'center',
        },
        font: {
            color: '#000000',
            size: 12,
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#92cddc'
        },
        border: {
            left: {
                style: 'thin', //BorderStyle ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#000000'
            },
            right: {
                style: 'thin',
                color: '#000000'
            },
            top: {
                style: 'thin',
                color: '#000000'
            },
            bottom: {
                style: 'thin',
                color: '#000000'
            },
            diagonal: {
                style: 'none',
            },
            diagonalDown: false,
            diagonalUp: false,
            outline: false
        },
    });
    const firstColStyle = wb.createStyle({
        alignment: { // §18.8.1
            horizontal: 'center',
            vertical: 'center',
        },
        font: {
            color: '#000000',
            size: 12,
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#daeef3'
        },
        border: { // §18.8.4 border (Border)
            left: {
                style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#000000'
            },
            right: {
                style: 'thin',
                color: '#000000'
            },
            top: {
                style: 'thin',
                color: '#000000'
            },
            bottom: {
                style: 'thin',
                color: '#000000'
            },
            diagonal: {
                style: 'none',
            },
            diagonalDown: false,
            diagonalUp: false,
            outline: false
        },
    });
    const entryStyle = wb.createStyle({
        alignment: { // §18.8.1
            horizontal: 'center',
            vertical: 'center',
        },
        font: {
            color: '#000000',
            size: 12,
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#ffffff'
        },
        border: { // §18.8.4 border (Border)
            left: {
                style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#000000'
            },
            right: {
                style: 'thin',
                color: '#000000'
            },
            top: {
                style: 'hair',
                color: '#000000'
            },
            bottom: {
                style: 'none',
            },
            diagonal: {
                style: 'none',
            },
            diagonalDown: false,
            diagonalUp: false,
            outline: false
        },
    });
    const lastEntryStyle = wb.createStyle({
        alignment: { // §18.8.1
            horizontal: 'center',
            vertical: 'center',
        },
        font: {
            color: '#000000',
            size: 12,
        },
        fill: {
            type: 'pattern',
            patternType: 'solid',
            fgColor: '#ffffff'
        },
        border: { // §18.8.4 border (Border)
            left: {
                style: 'thin', //§18.18.3 ST_BorderStyle (Border Line Styles) ['none', 'thin', 'medium', 'dashed', 'dotted', 'thick', 'double', 'hair', 'mediumDashed', 'dashDot', 'mediumDashDot', 'dashDotDot', 'mediumDashDotDot', 'slantDashDot']
                color: '#000000'
            },
            right: {
                style: 'thin',
                color: '#000000'
            },
            top: {
                style: 'hair',
                color: '#000000'
            },
            bottom: {
                style: 'thin',
                color: '#000000'
            },
            diagonal: {
                style: 'none',
            },
            diagonalDown: false,
            diagonalUp: false,
            outline: false
        },
    });

    // ws.cell(startRow, startColumn, [[endRow, endColumn], isMerged]);

    for (let i = 0; i < table.topRow.cells.length; i++) {
        ws.column(i + 1).setWidth(30);
        ws.cell(1, (i + 1))
            .string(table.topRow.cells[i].content)
            .style(headerStyle);
    }

    const getMaxEntries =
        (row) => {
            let maxEntries = 1;
            row.contentCells.forEach(
                (cell) => {
                    if (cell.entries.length > maxEntries)
                        maxEntries = cell.entries.length;
                }
            );
            return maxEntries;
        };

    ws.column(1).setWidth(30);

    let currentRowId = 2;
    for (let i = 0; i < table.contentRows.length; i++) {
        const maxEntries = getMaxEntries(table.contentRows[i]);
        const row = table.contentRows[i];

        ws.cell(currentRowId, 1, currentRowId + (maxEntries - 1), 1, true)
            .string(row.leftCell.content)
            .style(firstColStyle);


        for (let j = 0; j < (table.topRow.cells.length - 1); j++) {
            const contentCell = row.contentCells[j];
            if (contentCell) {
                for (let k = 0; k < maxEntries; k++) {
                    const entry = contentCell.entries[k];

                    ws.cell(currentRowId + k, 2 + j)
                        .string(entry ? entry.content : '')
                        .style((k === (maxEntries - 1)) ? lastEntryStyle : entryStyle);
                }
            } else {
                ws.cell(currentRowId, 2 + j, currentRowId + (maxEntries - 1), 2 + j, true)
                    .string('')
                    .style(lastEntryStyle);
            }
        }
        currentRowId += maxEntries;
    }
    return wb;
};

const exportDistribution = async (req, res, next) => {
    const connection = {req, res, next};

    const yearId = req.query.yearId;
    const promotionId = req.query.promotionId;
    const sortType = req.query.sortType;

    if ((Utils.isPositiveInt(yearId) || yearId === 'active')
        && Utils.isPositiveInt(promotionId)
        && (sortType === 'user' || sortType === 'terrain')) {

        const categoryId = Utils.isPositiveInt(req.query.categoryId) ? Number(req.query.categoryId) : null;

        const periods = await db.periods.selectAllByPromotionAndYear(req.sql.connection, ['id', 'beginDate', 'endDate', 'order'], promotionId, yearId);
        const terrainsTypes = await db.terrainTypes.selectOpenTerrainTypesByYearAndPromotion(req.sql.connection, ['tt.id', 'tt.name'], yearId, promotionId, categoryId);

        db.terrainAttributions.selectAllByPromotionAndYear(req.sql.connection, promotionId, yearId, categoryId)
            .then(
                (attribs) => {
                    const title = 'DFASM' + (promotionId - 1) + (sortType === 'user' ? ' par externe' : sortType === 'terrain' ? ' par stages' : ''); //TODO change this ugly thing
                    const table = new Table(title);

                    // STEP 1 : CONVERT DB DATA TO TABLE OBJECT
                    periods.sort((a, b) => a.order - b.order);

                    const formatTo2Digits = (number) => {
                        return (number < 10 && number > 0) ? '0' + number : number;
                    };
                    const formatDate = (date) => {
                        return formatTo2Digits(date.getDate()) + '/' + formatTo2Digits(date.getMonth() + 1) + '/' + date.getFullYear().toString().slice(-2);
                    };

                    periods.forEach(
                        (period) => {
                            const cell = new Cell();
                            cell.referenceId = period.id;
                            cell.content = formatDate(new Date(period.beginDate)) + ' au ' + formatDate(new Date(period.endDate));
                            table.topRow.cells.push(cell);
                        }
                    );

                    if (sortType === 'terrain') {
                        terrainsTypes.sort((a, b) => a.name.localeCompare(b.name));
                        terrainsTypes.forEach(
                            (terrainsType) => {
                                const row = {
                                    referenceId: terrainsType.id,
                                    leftCell: new Cell(),
                                    contentCells: [],
                                };

                                row.leftCell.referenceId = terrainsType.id;
                                row.leftCell.content = terrainsType.name;
                                //row.leftCell.data = terrainsType;

                                table.contentRows.push(row);
                            }
                        );

                        /**
                         * Attribution members:
                         *
                         * id
                         * terrainId
                         * terrainTypeId
                         * periodId
                         * userId
                         * firstName
                         * lastName
                         *
                         *
                         *      referenceId: number, //terrainTypeId
                         *      leftCell: new Cell(),
                         *      contentCells: {
                         *          referenceId: number, //terrainId
                         *          entries: { //attrib
                         *              referenceId: number, //attribId
                         *              content: string, //userName
                         *              data: string,
                         *          }[]
                         *      }[]
                         */
                        attribs.forEach(
                            (attrib) => {
                                let contentCell = table.getContentCellByReferenceId(attrib.terrainTypeId, attrib.periodId);

                                if (!contentCell)
                                    contentCell = {
                                        referenceId: attrib.terrainId,
                                        entries: []
                                    };

                                const entry = {
                                    referenceId: attrib.id,
                                    content: (attrib.firstName + ' ' + attrib.lastName),
                                    data: attrib
                                };

                                contentCell.entries
                                    .push(entry);

                                table.contentRows[table.getRowIndexByReferenceId(attrib.terrainTypeId)].contentCells[table.getTopCellIndexByReferenceId(attrib.periodId) - 1] = contentCell;
                            }
                        );

                        table.contentRows.forEach(
                            (row) => {
                                row.contentCells.forEach(
                                    (cell) => {
                                        cell.entries.sort((a, b) => {
                                            if (a.data.lastName === b.data.lastName) {
                                                return a.data.firstName.localeCompare(b.data.firstName);
                                            }
                                            return a.data.lastName.localeCompare(b.data.lastName);
                                        })
                                    }
                                )
                            }
                        );
                    } else if (sortType === 'user') {
                        /**
                         * Attribution members:
                         *
                         * id
                         * terrainId
                         * terrainTypeId
                         * periodId
                         *
                         * userId
                         * firstName
                         * lastName
                         *
                         *
                         *      referenceId: number, //terrainTypeId
                         *      leftCell: new Cell(),
                         *      contentCells: {
                         *          referenceId: number, //terrainId
                         *          entries: { //attrib
                         *              referenceId: number, //attribId
                         *              content: string, //userName
                         *              data: string,
                         *          }[]
                         *      }[]
                         */

                        const getTerrainTypeById = (terrainTypeId) => {
                            for (const terrainType of terrainsTypes) {
                                if (terrainType.id === terrainTypeId) {
                                    return terrainType;
                                }
                            }
                        };

                        attribs.forEach(
                            (attrib) => {
                                const rowIndex = table.getRowIndexByReferenceId(attrib.userId);
                                let row;
                                if (rowIndex === null) {
                                    const leftCell = new Cell();
                                    leftCell.referenceId = attrib.userId;
                                    leftCell.content = attrib.firstName + ' ' + attrib.lastName;
                                    leftCell.data = {
                                        userId: attrib.userId,
                                        firstName: attrib.firstName,
                                        lastName: attrib.lastName
                                    };

                                    row = {
                                        referenceId: attrib.userId,
                                        leftCell: leftCell,
                                        contentCells: []
                                    };

                                    table.contentRows.push(row);
                                } else {
                                    row = table.contentRows[rowIndex];
                                }

                                const topCellIndex = table.getTopCellIndexByReferenceId(attrib.periodId);
                                let contentCell = row.contentCells[topCellIndex - 1];

                                const tt = getTerrainTypeById(attrib.terrainTypeId);
                                if (!contentCell) {
                                    contentCell = {
                                        referenceId: attrib.id,
                                        entries: [{
                                            referenceId: tt.id,
                                            content: tt.name,
                                        }]
                                    }
                                } else {
                                    contentCell.entries.push({
                                        referenceId: attrib.id,
                                        entries: [{
                                            referenceId: tt.id,
                                            content: tt.name,
                                        }]
                                    })
                                }

                                row.contentCells[topCellIndex - 1] = contentCell
                            }
                        );

                        table.contentRows.sort(
                            (a, b) => {
                                if (a.leftCell.data.lastName === b.leftCell.data.lastName) {
                                    return a.leftCell.data.firstName.localeCompare(b.leftCell.data.firstName);
                                }
                                return a.leftCell.data.lastName.localeCompare(b.leftCell.data.lastName);
                            }
                        )

                    }

                    // STEP 2 : CONVERT TABLE OBJECT TO EXCEL FILE

                    const wb = fromTableToXL(table);

                    wb.write(title + '.xlsx', res);
                    sqlReleaser(req);
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

const formatDateForEpione = (dateAsString) => {
    const date = new Date(dateAsString);
    return date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear();
};

const exportToCSV = async (req, res, next) => {
    const connection = {req, res, next};
    const yearId = req.query.yearId;

    if (Utils.isPositiveInt(yearId)) {
        const attribs = await db.terrainAttributions.selectByYearId(req.sql.connection, yearId, [2, 3, 4]);
        let text = 'scholaryear;' +
            'about;' +
            'teacherlastname;' +
            'teacherfirstname;' +
            'teacheremail;' +
            'studentlastname;' +
            'studentfirstname;' +
            'studentemail;' +
            'department;' +
            'stagenumber;' +
            'startdate;' +
            'enddate;' +
            'headofservice;' +
            'headofunit' +
            '\n';

        let lastStudentNb = undefined;
        let i = 1;
        for (let line of attribs) {
            if (lastStudentNb !== line.studentNumber) {
                i = 1;
                lastStudentNb = line.studentNumber;
            }

            text += "2019" + ";";
            text += (line.promotionName + ";");
            text += (line.respFirstName + ';');
            text += (line.respLastName + ';');
            text += (line.respMail + ';');
            text += (line.lastName + ';');
            text += (line.firstName + ';');
            text += (line.studentNumber + ';');
            text += (line.terrainName + ';');
            text += (i++ + ';');
            text += (formatDateForEpione(line.beginDate) + ';');
            text += (formatDateForEpione(line.endDate) + ';');
            text += (line.respLastName + ';');
            text += ''; //headofunit line must be left blank
            text += '\n';
        }

        const fileName = 'repart-epione.csv';
        fs.writeFileSync(fileName, text);

        res.download(fileName, fileName);

        sqlReleaser(req);
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

module.exports = {
    exportDistribution,
    exportToCSV
};

