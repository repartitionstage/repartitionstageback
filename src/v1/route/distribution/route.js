module.exports = (router) => {
    const tokenChecker = require('../../middleware/tokenChecker');
    const adminChecker = require('../../middleware/adminChecker');
    const repartitionChecker = require('../../../v2/middlewares/repartChecker');

    const phase = require('./phase');
    router.route('/phases/model')
        .get(tokenChecker, phase.readAll)
        .post(tokenChecker, adminChecker, phase.create);
    router.route('/phases/model/:id')
        .get(tokenChecker, phase.read)
        .put(tokenChecker, adminChecker, phase.update)
        .delete(tokenChecker, adminChecker, phase.delete);
    router.route('/phases/getByUser')
        .get(tokenChecker, phase.readAllByUser);
    router.route('/phases/year/:yearId/promotion/:promotionId')
        .get(tokenChecker, phase.readAllByPromotionAndYear);
    router.route('/phases/active/:id')
        .put(tokenChecker, adminChecker, phase.activePhase);

    const terrain = require('./terrain');
    router.route('/terrains/model')
        .get(tokenChecker, terrain.readAll)
        .post(tokenChecker, adminChecker, terrain.create);
    router.route('/terrains/model/:id')
        .get(tokenChecker, terrain.read)
        .put(tokenChecker, adminChecker, terrain.update)
        .delete(tokenChecker, adminChecker, terrain.delete);
    router.route('/terrains/year/:yearId/promotion/:promotionId')
        .post(tokenChecker, adminChecker, terrain.updateByYearAndPromotion)
        .get(tokenChecker, terrain.readByYearAndPromotion);

    const terraintype = require('./terraintype');
    router.route('/terraintypes/model')
        .get(tokenChecker, terraintype.readAll)
        .post(tokenChecker, adminChecker, terraintype.create);
    router.route('/terraintypes/model/:id')
        .get(tokenChecker, terraintype.read)
        .put(tokenChecker, adminChecker, terraintype.update)
        .delete(tokenChecker, adminChecker, terraintype.delete);
    router.route('/terraintypes/open/year/:yearId/promotion/:promotionId')
        .get(tokenChecker, adminChecker, terraintype.readOpenTerrainTypesByYearAndPromotion);

    const terrainAttributions = require('./terrain-attribution');
    router.route('/terrain-attributions/model')
        .get(tokenChecker, terrainAttributions.readAll)
        .post(tokenChecker, adminChecker, terrainAttributions.create);
    router.route('/terrain-attributions/model/:id/phase/:phaseId')
        .get(tokenChecker, terrainAttributions.read)
        .put(tokenChecker, adminChecker, terrainAttributions.update)
        .delete(tokenChecker, adminChecker, terrainAttributions.delete);

    router.route('/terrain-attributions/user/:userId')
        .get(tokenChecker, terrainAttributions.selectByUser);

    router.route('/terrain-attributions/admin/terrain/:terrainId')
        .put(tokenChecker, adminChecker, terrainAttributions.updateMultiplesAttributionsState)
        .delete(tokenChecker, adminChecker, terrainAttributions.deleteMultiplesAttributions);
    router.route('/terrain-attributions/admin/model')
        .get(tokenChecker, adminChecker, terrainAttributions.readAdminAttribution);

    router.route('/terrain-attributions/private/model')
        .post(tokenChecker, repartitionChecker(['terrain', 'spreading']), terrainAttributions.createOnDistribution);
    router.route('/terrain-attributions/private/model/:id')
        .delete(tokenChecker, repartitionChecker(['terrain', 'spreading']), terrainAttributions.deleteOnDistribution);
    router.route('/terrain-attributions/private/by-phase/:phaseId')
        .get(tokenChecker, terrainAttributions.readAllByPhase);

    const exportation = require('./exportation');
    router.route('/exportation/pdf')
        .get(tokenChecker, adminChecker, exportation.exportDistribution);
    router.route('/exportation/csv')
        .get(tokenChecker, adminChecker, exportation.exportToCSV);

    return router;
};
