const db = require('../../../v2/database');
const RESTRoute = require('../restroute');
const Utils = require('../utils');

const routes = new RESTRoute(db.phases);

routes.createFormat = {
    name: {
        type: 'string'
    },
    order: {
        type: 'number',
        min: 0
    },
    promotionId: {
        type: 'number',
        min: 0
    },
    beginDate: {
        type: 'string'
    },
    endDate: {
        type: 'string'
    },
    config: {},
    yearId: {
        type: 'number',
    }
};

routes.updateFormat = {
    name: {
        type: 'string',
        optional: true
    },
    order: {
        type: 'number',
        min: 0,
        optional: true
    },

    beginDate: {
        type: 'string',
        optional: true
    },

    endDate: {
        type: 'string',
        optional: true
    },

    active: {
        type: 'boolean',
        optional: true
    },

    finish: {
        type: 'boolean',
        optional: true
    },

    config: {
        type: 'string',
        optional: true
    },

    promotionId: {
        type: 'number',
        min: 0,
        optional: true
    },

    yearId: {
        type: 'number',
        min: 0,
        optional: true
    }
};

routes.defaultSelectFields = ['id', 'order', 'name', 'promotionId', 'beginDate', 'endDate', 'active', 'finish', 'config', 'yearId'];

routes.readAllByUser = (req, res, next) => {
    const connection = {req, res, next};
    const userId = Number(req.payload.userId);
    if (!isNaN(userId)) {
        db.phases.selectAllByUser(req.sql.connection, userId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.readAllByPromotionAndYear = (req, res, next) => {
    const connection = {req, res, next};
    const promotionId = req.params.promotionId;
    const yearId = req.params.yearId;

    if ((yearId === 'active' || Utils.isPositiveInt(yearId)) && Utils.isPositiveInt(promotionId)) {
        if (yearId === 'active') {
            db.phases.selectByActiveYear(req.sql.connection, ['id', 'name', 'order', 'beginDate', 'endDate', 'active', 'finish', 'config', 'yearId'], promotionId)
                .then(Utils.sendDefaultData(connection))
                .catch(Utils.sendDefaultError(connection));
        } else {
            db.phases.selectAllByPromotionAndYear(req.sql.connection, ['id', 'name', 'order', 'beginDate', 'endDate', 'active', 'finish', 'config', 'yearId'], promotionId, yearId)
                .then(Utils.sendDefaultData(connection))
                .catch(Utils.sendDefaultError(connection));

        }
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.activePhase = (req, res, next) => {
    const connection = {req, res, next};
    const phaseId = req.params.id;
    if (Utils.isPositiveInt(phaseId)) {
        db.phases.activePhase(req.sql.connection, phaseId)
            .then(Utils.sendDefaultSuccess(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

module.exports = routes;
