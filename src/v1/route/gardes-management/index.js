module.exports = (router) => {
    const tokenChecker = require('../../middleware/tokenChecker');
    const adminChecker = require('../../middleware/adminChecker');

    const garde = require('./gardes');
    router.route('/gardes')
        .get(tokenChecker, garde.getGardes);
    router.route('/gardes/:id')
        .put(tokenChecker, adminChecker, garde.update);

    router.route('/free')
        .get(tokenChecker, adminChecker, garde.getFreeGardes);
    router.route('/free/:id')
        .put(tokenChecker, adminChecker, garde.removeUserFromGarde);

    const terrains = require('./terrains');
    router.route('/terrains')
        .get(tokenChecker, terrains.readAll)
        .post(tokenChecker, adminChecker, terrains.create);
    router.route('/terrains/:id')
        .get(tokenChecker, terrains.read)
        .put(tokenChecker, adminChecker, terrains.update)
        .delete(tokenChecker, adminChecker, terrains.delete);

    const yearConfig = require('./year-config');
    router.route('/config/:yearId')
        .get(tokenChecker, yearConfig.readByYearId)
        .put(tokenChecker, adminChecker, yearConfig.updateByYearId);
    router.route('/holydays')
        .get(tokenChecker, yearConfig.getWeekHolydaysByDate);

    const planning = require('./planning');
    router.route('/planning/:yearId')
        .get(tokenChecker, planning.downloadPlanning)
        .put(tokenChecker, adminChecker, planning.genPlanning);

    return router;
};
