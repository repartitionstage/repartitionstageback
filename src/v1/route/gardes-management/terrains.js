const db = require('../../../v2/database');
const RESTRoute = require('../restroute');

const routes = new RESTRoute(db.gardeTerrains);
routes.createFormat = {
    name: {
        type: 'string'
    },
    description: {
        type: 'string'
    },
    night: {
        type: 'boolean'
    }
};
routes.updateFormat = {
    id: {
        type: 'number'
    },
    name: {
        type: 'string'
    },
    description: {
        type: 'string'
    },
    night: {
        type: 'boolean'
    }
};
routes.defaultSelectFields = ['id', 'name', 'description', 'night'];

module.exports = routes;
