const db = require('../../../v2/database');
const RESTRoute = require('../restroute');
const Utils = require('../utils');

const routes = new RESTRoute(db.gardeYearConfigs);
routes.createFormat = {
    yearId: {
        type: 'number'
    },
    config: {
        type: 'object'
    }
};
routes.updateFormat = {
    config: {
        type: 'object'
    }
};
routes.defaultSelectFields = ['yearId', 'config'];

routes.readByYearId = (req, res, next) => {
    const connection = {req, res, next};
    const yearId = req.params['yearId'];

    if (Utils.isPositiveInt(yearId)) {

        db.gardeYearConfigs.selectByYearId(req.sql.connection, yearId)
            .then(Utils.sendDefaultData(connection))
            .catch(
                (error) => {
                    if (error === Utils.OBJECT_NOT_FOUND) {
                        const defaultVal = {
                            beginDate: '',
                            endDate: '',
                            holydays: [],
                            exceptions: [
                                {
                                    promotion: 2,
                                    dates: []
                                },
                                {
                                    promotion: 3,
                                    dates: []
                                },
                                {
                                    promotion: 4,
                                    dates: []
                                }],
                        };
                        Utils.sendSuccessWithData(connection, {config: defaultVal});
                    } else {
                        Utils.sendError(connection, error);
                    }
                }
            );

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.updateByYearId = (req, res, next) => {
    const connection = {req, res, next};
    const yearId = req.params['yearId'];
    const object = JSON.parse(req.body.object);

    if (Utils.isPositiveInt(yearId)
        && Utils.validateData(object, routes.updateFormat)) {

        db.gardeYearConfigs.updateByYearId(req.sql.connection, yearId, object.config)
            .then(Utils.sendDefaultSuccess(connection))
            .catch(Utils.sendDefaultError(connection));

    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.getWeekHolydaysByDate = (req, res, next) => {
    const connection = {req, res, next};
    const dateInput = req.query.date;

    if (dateInput && !isNaN(Date.parse(dateInput))) {
        const date = new Date(dateInput);

        while (date.getDay() !== 1) { // Back to Monday
            date.setDate(date.getDate() - 1);
        }

        const dates = [];

        while (dates.length === 0 || date.getDay() !== 1) {
            dates.push(new Date(date.getTime()));
            date.setDate(date.getDate() + 1);
        }


        const dateStrArray = dates.map((item) => Utils.formatDate(item));
        db.gardeYearConfigs.selectAll(req.sql.connection, ['config'])
            .then(
                (result) => {
                    const holydays = [];

                    result.forEach(
                        (year) => {
                            year.config = JSON.parse(year.config);
                            year.config.holydays.forEach(
                                (date) => {
                                    if (dateStrArray.indexOf(date) !== -1)
                                        holydays.push(date);
                                }
                            )
                        }
                    );
                    Utils.sendSuccessWithData(connection, holydays);
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

module.exports = routes;
