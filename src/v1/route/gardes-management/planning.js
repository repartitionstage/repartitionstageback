const Utils = require('../utils');
const gardeService = require('../../../v2/features/gardes-management/garde-service');
const sqlReleaser = require('../../middleware/sql-releaser');

const downloadPlanning = (req, res, next) => {
    const connection = {req, res, next};
    const yearId = req.params['yearId'];

    if (Utils.isPositiveInt(yearId)) {

        gardeService.genXL(req.sql.connection, yearId)
            .then(
                (wb) => {
                    wb.write('Planning de gardes.xlsx', res);
                    sqlReleaser(req)
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

const genPlanning = (req, res, next) => {
    const connection = {req, res, next};
    const yeardId = req.params['yearId'];

    if (Utils.isPositiveInt(yeardId)) {
        gardeService.genPlanning(req.sql.connection, yeardId)
            .then(Utils.sendDefaultSuccess(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }

};

module.exports = {
    genPlanning,
    downloadPlanning
};
