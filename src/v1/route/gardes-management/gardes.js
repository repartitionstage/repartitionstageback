const db = require('../../../v2/database');
const RESTRoute = require('../restroute');
const Utils = require('../utils');

const routes = new RESTRoute(db.gardes);

routes.getGardes = (req, res, next) => {
    const connection = {req, res, next};

    let userId = req.query.userId;
    if (userId) {
        if (userId === 'self') {
            userId = req.payload.userId;
        } else if (isNaN(userId) || !Utils.isPositiveInt(userId, 1)) {
            Utils.sendError(connection, Utils.INVALID_FORMAT);
            return;
        }
    }

    let date = req.query.date;
    if (date) {
        if (isNaN(Date.parse(date))) {
            Utils.sendError(connection, Utils.INVALID_FORMAT);
            return;
        }

        date = new Date(date);
        while (date.getDay() !== 1) { // Back to Monday
            date.setDate(date.getDate() - 1);
        }

        const dates = [];
        while (dates.length === 0 || date.getDay() !== 1) { // get all of the week date
            dates.push(new Date(date.getTime()));
            date.setDate(date.getDate() + 1);
        }

        date = dates.map((item) => Utils.formatDate(item));
    }

    if (!date && !userId) {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
        return;
    }


    db.gardes.select(req.sql.connection, userId, date)
        .then(Utils.sendDefaultData(connection))
        .catch(Utils.sendDefaultError(connection));
};

routes.getGardesByUserId = (req, res, next) => {
    const connection = {req, res, next};
    let userId = req.params.userId;

    if (userId === 'self')
        userId = req.payload.userId;

    if (Utils.isPositiveInt(userId)) {
        db.gardes.selectByUserId(req.sql.connection, userId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.getWeekGardesByDate = (req, res, next) => {
    const connection = {req, res, next};
    const dateInput = req.query.date;

    if (dateInput && !isNaN(Date.parse(dateInput))) {
        const date = new Date(dateInput);

        while (date.getDay() !== 1) { // Back to Monday
            date.setDate(date.getDate() - 1);
        }

        const dates = [];

        while (dates.length === 0 || date.getDay() !== 1) {
            dates.push(new Date(date.getTime()));
            date.setDate(date.getDate() + 1);
        }

        const dateStrArray = dates.map((item) => Utils.formatDate(item));
        db.gardes.selectByDays(req.sql.connection, dateStrArray)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

routes.getFreeGardes = (req, res, next) => {
    const connection = {req, res, next};

    db.gardes.selectFreeGardes(req.sql.connection)
        .then(Utils.sendDefaultData(connection))
        .catch(Utils.sendDefaultError(connection));
};

routes.removeUserFromGarde = (req, res, next) => {
    const connection = {req, res, next};
    let gardeId = req.params.id;

    if (Utils.isPositiveInt(gardeId)) {
        db.gardes.removeUserFromGarde(req.sql.connection, gardeId)
            .then(Utils.sendDefaultData(connection))
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }
};

module.exports = routes;
