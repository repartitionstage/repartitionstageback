const db = require('../../../v2/database');
const Utils = require('../utils');
const Logs = require('../../service').Logs;

const create = (req, res, next) => {
    const connection = {req, res, next};

    const object = JSON.parse(req.body.object);

    if (object && Utils.validateData(object, {givenGardeId: {type: 'number'}, wantedGardeId: {type: 'number'}})) {
        db.exchanges.insert(req.sql.connection, {
            given_garde_id: object.givenGardeId,
            wanted_garde_id: object.wantedGardeId,
            state: db.exchanges.STATE.STANDBY
        })
            .then(
                () => {
                    Logs.append(req.sql.connection, req.payload.userId, req.payload.userId, req.ip, Logs.TYPES.CREATE_EXCHANGE, object)
                        .then(Utils.sendDefaultSuccess(connection))
                        .catch(Utils.sendDefaultError(connection));
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils.INVALID_FORMAT);
    }

}

const revoke = (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    if (Utils.isPositiveInt(id)) {
        db.exchanges.selectExchangers(req.sql.connection, id)
            .then(
                (result) => {
                    if (req.payload.userId === result.giverId || req.payload.userId === result.receiverId) {

                        db.exchanges.updateById(req.sql.connection, id, {
                            state: db.exchanges.STATE.REFUSED
                        })
                            .then(
                                () => {
                                    Logs.append(req.sql.connection, req.payload.userId, req.payload.userId, req.ip, Logs.TYPES.CREATE_EXCHANGE, object)
                                        .then(Utils.sendDefaultSuccess(connection))
                                        .catch(Utils.sendDefaultError(connection));
                                }
                            )
                            .catch(Utils.sendDefaultError(connection));

                    } else {
                        Utils.sendError(connection, Utils.ERROR_OCCURS);
                    }
                }
            )
            .catch(Utils.sendDefaultError(connection));
    }
}

const exchange = (sql, exchangeId) => {
    return new Promise(
        (resolve, reject) => {
            db.exchanges.selectGardes(sql, exchangeId)
                .then(
                    data => {
                        const givenGardeId = data.givenGardeId;
                        const giverId = data.giverId;
                        const wantedGardeId = data.wantedGardeId;
                        const receiverId = data.receiverId;

                        db.gardes.updateById(sql, givenGardeId, {user_id: receiverId, trade: 1})
                            .then(
                                () => {
                                    db.gardes.updateById(sql, wantedGardeId, {user_id: giverId, trade: 1})
                                        .then(
                                            () => {
                                                resolve();
                                            }
                                        ).catch(() => reject(Utils.SQL_ERROR));
                                }
                            ).catch(() => reject(Utils.SQL_ERROR));

                    }
                )
        }
    );
}

const accept = (req, res, next) => {
    const connection = {req, res, next};
    const id = req.params.id;

    if (Utils.isPositiveInt(id)) {
        db.exchanges.selectExchangers(req.sql.connection, id)
            .then(
                (result) => {
                    if (req.payload.userId === result.receiverId) {

                        exchange(req.sql.connection, id)
                            .then(
                                () => {
                                    db.exchanges.updateById(req.sql.connection, id, {
                                        state: db.exchanges.STATE.ACCEPTED
                                    })
                                        .then(
                                            () => {
                                                Logs.append(req.sql.connection, req.payload.userId, req.payload.userId, req.ip, Logs.TYPES.ACCEPT_EXCHANGE, {id})
                                                    .then(Utils.sendDefaultSuccess(connection))
                                                    .catch(Utils.sendDefaultError(connection));
                                            }
                                        )
                                        .catch(Utils.sendDefaultError(connection));
                                }
                            )
                            .catch(Utils.sendDefaultError(connection));
                    } else {
                        Utils.sendError(connection, Utils.ERROR_OCCURS);
                    }
                }
            )
            .catch(Utils.sendDefaultError(connection));
    } else {
        Utils.sendError(connection, Utils)
    }

}


module.exports = {
    create,
    answer,
    revoke
}
