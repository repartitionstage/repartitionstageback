const Validator = require('../v2/utils/validator')

test('Validate boolean', () => {
    expect(Validator.validateBoolean(true)).toBe(true);
    expect(Validator.validateBoolean(false)).toBe(true);
    expect(Validator.validateBoolean('true')).toBe(true);
    expect(Validator.validateBoolean('false')).toBe(true);
    expect(Validator.validateBoolean(0)).toBe(true);
    expect(Validator.validateBoolean(1)).toBe(true);
    expect(Validator.validateBoolean('0')).toBe(true);
    expect(Validator.validateBoolean('1')).toBe(true);

    expect(Validator.validateBoolean(12)).toBe(false);
    expect(Validator.validateBoolean('coucou')).toBe(false);
});

test('Validate number', () => {
    expect(Validator.validateNumber('coucou')).toBe(false);
    expect(Validator.validateNumber('56')).toBe(true);
    expect(Validator.validateNumber(56)).toBe(true);
    expect(Validator.validateNumber(5, {min: 4, max: 6})).toBe(true);
    expect(Validator.validateNumber(3, {min: 4, max: 6})).toBe(false);
    expect(Validator.validateNumber(7, {min: 4, max: 6})).toBe(false);
    expect(Validator.validateNumber(5, {min: 'coucou', max: 6})).toBe(false);
    expect(Validator.validateNumber(5, {min: 5, max: "roubroub"})).toBe(false);
});
