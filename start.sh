echo "Lancement de l'api de Tirajosaure"
echo "------------------------------------"
echo "1. Installation des dépendances"
npm install
echo "------------------------------------"
echo "2. Lancement via pm2"
#pm2 start npm --name dev.api.tirajosaure.ars-amiens.fr -i 2 -- start
pm2 start src/app.js --name dev.api.tirajosaure.ars-amiens.fr -i 2
echo "------------------------------------"
